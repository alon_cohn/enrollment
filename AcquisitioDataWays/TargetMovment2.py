import pygame
import pygame.gfxdraw
import time
import toGroup.WindowPropertys
from colorama import *
import matplotlib.pyplot as plt
import datetime
from threading import Thread
from Utils import Log
import  pandas as pd
import numpy as np
from AcquisitioDataWays.TargetPin import targetPin


class movment2T(targetPin):
    def __init__(self,groupMembersInnerGroup,Velocity,GroupDistance,FramRate,GroupLength,TargetMovement1DF):
        super().__init__(groupMembersInnerGroup,Velocity,GroupDistance,FramRate,GroupLength)
        self.TargetMovement1DF=TargetMovement1DF
        self.CreatTargetCoorDFMovement2()

    def Movement2TargetData(self):
        Log.AddToLog('BuildingTargetValues started')
        try:
            a=1
        except Exception as e:
            Log.AddToLog("exception in BuildingTargetValues :  {}".format(e), 2)

    def CreatTargetCoorDFMovement2(self):
        # making a multi index matrix
        Log.AddToLog('CreatTargetCoorDF started')
        try:
            TargetMovement1DF = self.TargetMovement1DF
            index_tuples = []
            FirstRaw = []
            start_angle = 180
            self.outerGroupAngle = range(start_angle, start_angle + 360, int(360 / self.MembersInGroup))
            for firstMovementAngle in TargetMovement1DF.Angle:
                for secondMovementAngle in self.outerGroupAngle:
                    for coor in ["X", "Y"]:
                        index_tuples.append([(firstMovementAngle), secondMovementAngle, coor])
            index = pd.MultiIndex.from_tuples(index_tuples, names=["A1", "A2", "Coordinate"])
            self.CooridnateDF = pd.DataFrame(columns=index)

        except Exception as e:
            Log.AddToLog("exception in CreatTargetCoorDF :  {}".format(e), 2)

    def EnterValues(self,TargetMovement1, Movement1DF,sampls):
        # making a multi index matrix
        Log.AddToLog('EntreValues started')
        try:
            TotalTime = self.GroupDistance / self.Velocity
            TimeIntervals = np.arange(0, TotalTime, TotalTime / sampls)[0:sampls]
            if len(TimeIntervals) != sampls:
                print('Error in Enter Values,sample = {}, ToatalTime = {}'.format(sampls,TotalTime))
            for firstMovementAngle in self.CooridnateDF._info_axis._levels[0]:
                for secondMovementAngle in self.CooridnateDF._info_axis._levels[1]:
                    XO,YO = self.StartPoint(TargetMovement1[firstMovementAngle] , secondMovementAngle)
                    angleRAD = secondMovementAngle * 2 * np.pi / 360
                    self.CooridnateDF.loc[:, (firstMovementAngle, secondMovementAngle, 'X')] = float(XO) + (self.Velocity * TimeIntervals * np.cos(angleRAD))
                    self.CooridnateDF.loc[:, (firstMovementAngle, secondMovementAngle, 'Y')] = float(YO) + (self.Velocity * TimeIntervals * np.sin(angleRAD))
        except Exception as e:
            Log.AddToLog("exception in CreatTargetCoorDF :  {}".format(e), 2)

    def StartPoint(self, DF,secondMovementAngle):
        OX = DF.X[-1:]
        OY = DF.Y[-1:]
        return(self.RvresePolarToCartzien(secondMovementAngle, self.GroupLength, self.GroupLength,OX,OY))

