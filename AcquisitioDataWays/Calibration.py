# -*- coding: utf-8 -*-
__author__ = "Klaas Filler"

import pygame
import pygame.gfxdraw
from Utils import Log
from threading import Thread
import time
from colorama import *

from toGroup import WindowPropertys

class cali():

    def __init__(self, newlogfile="None", logStatus="a"):
        pygame.init()
        self.GUIframeRate = 60
        self.window = WindowPropertys.windowPropertys()
        self.window.setScreenSize()
        self.clock = pygame.time.Clock()
        self.bg_RGB = 255
        self.initialRGB = self.bg_RGB
        self.bg_color = (self.bg_RGB, self.bg_RGB, self.bg_RGB)
        self.object_color = (0, 0, 0)
        self.circle_size = self.window.valueResizer(416)
        self.initilCircleSize = self.circle_size
        self.ScreenName=''
        self.SampleTimeDict = {'White Screen': 2,'Pre Baseline': 2, 'Brightness Change': 2.5, 'Post Baseline': 0.5}

    def backgroundDrawer(self):
        self.window.background.fill(self.bg_color)
        self.window.screen.blit(self.window.background, (0, 0))

    def circleDrawer(self):
        Log.AddToLog("circleDrawer started ")
        try:
            if self.circle_size - 1 > 0:
                self.circlesurface = pygame.Surface((self.circle_size, self.circle_size))
                self.circlesurface.fill(self.bg_color)
                pygame.draw.circle(self.circlesurface, self.object_color,
                                   (int(self.circle_size / 2), int(self.circle_size / 2)), int(self.circle_size / 2))
                pygame.draw.circle(self.circlesurface, self.bg_color,
                                   (int(self.circle_size / 2), int(self.circle_size / 2)), int(self.circle_size / 2 - 1))
                pygame.draw.circle(self.circlesurface, self.object_color,
                                   (int(self.circle_size / 2), int(self.circle_size / 2)), int(2))
                self.circlesurface.convert()
                self.window.screen.blit(self.circlesurface, (
                self.window.width / 2 - self.circle_size / 2, self.window.height / 2 - self.circle_size / 2))
                pygame.display.update()
        except Exception as e:
            Log.AddToLog("exception in circleDrawer :  {}".format(e), 2)
            return False
    # calibration run GUI
    '''
    update the note columns.
    for each note transfer to a function call ChangeScreen- a parser to the correct function
    get back from Change screen true or false. if get false, stop the program and and return false to runner
    Activate the thread whcih collect the data from the SMI
    after collectedf all stages ask if the data is valid ( in pin it for each note here it is for each run)
    '''
    def RunGUI(self,Tracker, Screen,AmountofFeatureVectors, pin=None):
        Log.AddToLog("RunGUI started ")
        try:
            Screen.ExplainCalibration()
            pygame.mouse.set_visible(False)
            self.window.getScreenWithBlackBackground()
            StartCollectingData = Thread(target=Tracker.StartTracking)
            self.backgroundDrawer()         #Drawing a white screen
            pygame.display.update()
            start = time.perf_counter()
            deltaTime = 0
            while deltaTime < self.SampleTimeDict['White Screen']:    #Waiting for 4 seconds
                deltaTime = time.perf_counter() - start
            print('Start to collect Data in calibration ')
            '''------------------prebase line-------------------------------------------------------------'''
            # Drawing a circle
            self.circleDrawer()
            Tracker.Note = "Pre Baseline"
            Tracker.trackerRunning = True
            StartCollectingData.start()
            if not self.ChangeScreen(Tracker):
                return False
            '''------------------Brightness Change line---------------------------------------------------'''
            Tracker.Note = "Brightness Change"
            if not self.ChangeScreen(Tracker):
                return False
            '''------------------Post Baseline Change line-------------------------------------------------'''
            Tracker.Note = "Post Baseline"
            if not self.ChangeScreen(Tracker):
                return False
            '''------------------Post Baseline Change line-------------------------------------------------'''
            # while the thread have not ended!
            while Tracker.trackerRunning:
                continue
            if not Tracker.IsDataValid(self.window.width,self.window.height):
                print(Fore.RED + 'Failed to collect ValidData at Clibration return False ' + Fore.RESET)
                return False
            return True
        except Exception as e:
            Log.AddToLog("exception in RunGUI :  {}".format(e), 2)
            return False

    #TIME and not sample IS CONSTANCT.
    # a while loop on time ( change from betweens the note)
    #Gui tool
    def ChangeScreen(self,Tracker):
        Log.AddToLog("ChangeScreen started for {}".format(Tracker.Note))
        try:
            #Tracker.SampleNumber=0
            start = time.perf_counter()
            self.deltaTime = 0
            while self.deltaTime < self.SampleTimeDict[Tracker.Note]:
                self.clock.tick(self.GUIframeRate)
                # if the Psuh data flag is False before i told it to be meaning something got wrong at the process.
                if not Tracker.trackerRunning:
                    return False
                if Tracker.Note == "Pre Baseline":
                    pygame.event.get()
                elif Tracker.Note == "Brightness Change":
                    if not self.ChangeCircle():
                        return False
                    pygame.display.update()
                    pygame.event.get()
                elif Tracker.Note == "Post Baseline":
                    pygame.event.get()
                self.deltaTime = time.perf_counter() - start
            # if you are at the end of the sequence turn off the thread whe condition to end is in startTracking
            if Tracker.Note == "Post Baseline":
                Tracker.EndWriting=True
                Log.AddToLog("Tracker.EndWriting changed to {} ".format(Tracker.EndWriting))
            return True
        except Exception as e:
            Log.AddToLog("exception in ChangeScreen :  {}".format(e), 2)
            return False

    def ChangeCircle(self):
        try:
            Log.AddToLog("ChangeCircle  started")
            self.object_color = (0, 0, 0)
            OneMiliSec=1000
            SecInMsec=1000
            #TimePerFram=OneMiliSec/self.GUIframeRate
            TimePerFram = OneMiliSec / self.clock.get_fps()
            # 2.5*1000/(1000/60)
            AmountOfEvents=((self.SampleTimeDict['Brightness Change']*SecInMsec)/TimePerFram)
            if self.bg_RGB >= self.initialRGB/AmountOfEvents:
                self.bg_RGB -= self.initialRGB/AmountOfEvents
            else:
                self.bg_RGB = 0
            self.bg_color = (self.bg_RGB, self.bg_RGB, self.bg_RGB)
            if self.circle_size >= self.initilCircleSize/AmountOfEvents:
                self.circle_size -= self.initilCircleSize/AmountOfEvents
            if self.deltaTime > (self.SampleTimeDict['Brightness Change'])/2:
                self.object_color = (255, 255, 255)
            self.backgroundDrawer()
            self.circleDrawer()
            return True
        except Exception as e:
            Log.AddToLog("exception in ChangeCircle :  {}".format(e), 2)
            return False

    def reset(self):
        self.bg_RGB = 255
        self.initialRGB = self.bg_RGB
        self.bg_color = (self.bg_RGB, self.bg_RGB, self.bg_RGB)
        self.object_color = (0, 0, 0)
        self.circle_size = self.window.valueResizer(416)
        self.initilCircleSize = self.circle_size
