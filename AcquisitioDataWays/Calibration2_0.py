# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"

import pygame
import pygame.gfxdraw
from EyeData import EyeData
from threading import Thread
import datetime
import numpy as np
import time
from GUI_Propertys import WindowPropertys


class Calibration():

    def __init__(self, newlogfile="None", logStatus="a"):
        pygame.init()
        self.frameRate = 60
        pygame.mouse.set_visible(False)

        self.window = WindowPropertys.windowPropertys()
        self.window.setScreenSize()
        self.window.getScreenWithBlackBackground()

        print("Calibration Screen = ({},{})".format(self.window.width, self.window.height))
        self.ED = EyeData.EyeData("Calibration", newlogfile, logStatus)
        self.clock = pygame.time.Clock()
        self.bg_RGB = 255
        self.bg_color = (self.bg_RGB, self.bg_RGB, self.bg_RGB)
        self.object_color = (0, 0, 0)
        self.circle_size = self.window.valueResizer(416)

    def backgroundDrawer(self):
        self.window.background.fill(self.bg_color)
        self.window.screen.blit(self.window.background, (0, 0))

    # pygame.display.update()

    def circleDrawer(self):
        if self.circle_size - 1 > 0:
            self.circlesurface = pygame.Surface((self.circle_size, self.circle_size))
            self.circlesurface.fill(self.bg_color)
            pygame.draw.circle(self.circlesurface, self.object_color,
                               (int(self.circle_size / 2), int(self.circle_size / 2)), int(self.circle_size / 2))
            pygame.draw.circle(self.circlesurface, self.bg_color,
                               (int(self.circle_size / 2), int(self.circle_size / 2)), int(self.circle_size / 2 - 1))
            pygame.draw.circle(self.circlesurface, self.object_color,
                               (int(self.circle_size / 2), int(self.circle_size / 2)), int(2))
            self.circlesurface.convert()
            self.window.screen.blit(self.circlesurface, (
            self.window.width / 2 - self.circle_size / 2, self.window.height / 2 - self.circle_size / 2))
            pygame.display.update()

    def startCalibration(self):
        t1 = Thread(target=self.ED.startTracking)
        self.ED.moveID += 1
        self.eyeX = []
        self.eyeY = []
        self.backgroundDrawer()
        pygame.display.update()
        start = time.time()
        deltaTime = 0
        t1.start()
        while deltaTime < 4:
            deltaTime = time.time() - start
        self.circleDrawer()
        # self.ED.connectTracker()
        self.ED.note = "Pre Baseline"
        print("Pre Baseline")
        self.ED.sampleNumber = 0
        while self.ED.sampleNumber < 120:
            pygame.event.get()
            self.clock.tick(self.frameRate)
        self.ED.sampleNumber = 0
        self.ED.note = "Brightness Change"
        print("Brightness Change")
        #Changed length of calibration to 2 * self.framerate because the movement in the
        #experimantal prototyp was much slower
        #TODO: test the influence on the indentification!!!
        while self.ED.sampleNumber < 120:
            self.object_color = (0, 0, 0)
            #pygame.event.get()
            if (self.ED.x != 0) and (self.ED.sampleNumber > self.frameRate/4) and (self.ED.sampleNumber < self.frameRate*(3/4)):
                self.eyeX.append(self.ED.eyeData['RawX'])
                self.eyeY.append(self.ED.eyeData['RawY'])
            if self.bg_RGB >= self.window.valueResizer(2)* (120 / self.frameRate):
                self.bg_RGB -= self.window.valueResizer(2) * (120 / self.frameRate)
            else:
                self.bg_RGB = 0
            self.bg_color = (self.bg_RGB, self.bg_RGB, self.bg_RGB)
            if self.circle_size > 120 / self.frameRate:
                self.circle_size -= self.window.valueResizer(2) * (120 / self.frameRate)
            if self.ED.sampleNumber > self.frameRate / 2:
                self.object_color = (255, 255, 255)
            self.backgroundDrawer()
            self.circleDrawer()
            self.clock.tick(self.frameRate)
        # pygame.display.update()
        self.ED.sampleNumber = 0
        self.ED.note = "Post Baseline"
        print("Post Baseline")
        while self.ED.sampleNumber < 30:
            self.clock.tick(self.frameRate)
        self.ED.stopTracking()
        self.ED.log.closeLogFile()
        try:
            return (self.window.width / 2) - np.mean(self.eyeX), (self.window.height / 2) - np.mean(self.eyeY)
        except TypeError:
            return 0, 0
    # pygame.display.quit()


if __name__ == '__main__':
    calib = Calibration()
    calib.startCalibration()
    calib.ED.log.closeLogFile()
