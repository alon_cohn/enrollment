from Configuration import Globales
from Configuration import ParameterManger as PM
from AcquisitioDataWays.Calibration import  cali
from AcquisitioDataWays.Pin import pin
from Utils import Log
import pandas as pd
class caliPin():
    def __init__(self, AqucName=None):
        self.repeats = PM.ReturnParameter(conf_file_name='//caliPin.json', ParameterName='self.repeats')
        self.pin = pin()
        self.cali = cali()

    def RunGUI(self, Tracker, Screen, amountOfFeatureVectores, digit=None):
        """
        feature vector is a cli +ful pin cycle!
        1. activate calibration
        2. copy the df from calibration and afterwards reset the tracker.
        3. activate pin
        4. concat the cali and pin df
        :param Tracker:
        :param Screen:
        :param amountOfFeatureVectores:
        :param digit: not needed only a legacy.
        :return:  df with calibration and pin df.
        """
        Log.AddToLog("RunGUI cali+pin", 1)
        try:
            if not self.cali.RunGUI(Tracker, Screen, amountOfFeatureVectores, digit):
                print("unfortunately we could not suceed in calibration, not point continuing to pin. try again ")
                Screen.cali_failed()
                return False

            print("finish step 1 - calibration - successfully. moving to pin")
            cali_df = Tracker.EyeDataDF.copy()
            Tracker.Reset()

            pin_df = self.pin_active(Tracker, Screen, amountOfFeatureVectores, digit)
            if isinstance(pin_df, bool):
                return False
            Tracker.EyeDataDF =  pd.concat([cali_df, pin_df],sort=False)
            return True
        except Exception as e:
            Log.AddToLog("exception in RunGUI cali+pin :  {}".format(e), 2)
            return False

    def pin_active(self, Tracker, Screen, amountOfFeatureVectores, digit=None):
        """
        activate pin.
        if failed in finding the digit more then X times return false
        collect data for every digit and concat it.
        :return: a pin df ( all digit combine together!
        """
        Log.AddToLog("pin_active cali+pin", 1)
        try:
            pin_df = pd.DataFrame(columns=Tracker.mycolumns)
            user_pin = str(Globales.USER_PIN)
            i = 0
            err = 0
            number_of_allowed_err = PM.ReturnParameter("number_of_allowed_err", "/caliPin.json")
            while i < len(user_pin):
                Tracker.Reset()
                self.pin.reset()
                digit = int(user_pin[i])
                if not self.pin.RunGUI(Tracker, Screen, amountOfFeatureVectores, digit):
                    err += 1
                    if err > number_of_allowed_err:
                        print("didn't manage to create pin more then 3 times in a row at cali+pin, returning false.")
                        Log.AddToLog("didn't manage to create pin more then "
                                     "3 times in a row at cali+pin, returning false.")
                        return False
                else:
                    err = 0
                    i += 1
                    amountOfFeatureVectores += 1
                    pin_df = pd.concat([pin_df, Tracker.EyeDataDF], sort=False)
            return pin_df
        except Exception as e:
            Log.AddToLog("exception in pin_active cali+pin :  {}".format(e), 2)
            return False

    def reset(self):
        try:
            pass
        except Exception as e:
            Log.AddToLog("exception in reset cali+pin :  {}".format(e), 2)







