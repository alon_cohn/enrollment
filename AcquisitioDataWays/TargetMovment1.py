import pygame
import pygame.gfxdraw
import time
import toGroup.WindowPropertys
from colorama import *
import matplotlib.pyplot as plt
import datetime
from threading import Thread
from Utils import Log
import  pandas as pd
import numpy as np
from AcquisitioDataWays.TargetPin import targetPin

class movment1T(targetPin):
    def __init__(self,groupMembersInnerGroup,OX,OY,Velocity,GroupDistance,FramRate,GroupLength):
        super().__init__(groupMembersInnerGroup,Velocity,GroupDistance,FramRate,GroupLength)
        self.OX=OX
        self.OY=OY
        self.TargetData()
        self.CreatTargetCoorDF()


    def TargetData(self):
        Log.AddToLog('TargetData started')
        try:
            mycolumns = ['XstartPoint', 'YstartPoint', 'Angle']#, 'XEndPoint', 'YEndPoint', 'A', 'B']
            start_angle = 180
            Angles = np.arange(start_angle, start_angle + 360, int(360 / self.MembersInGroup),dtype=int)
            self.TargetDataDF = pd.DataFrame(columns=mycolumns,index=Angles)
            XstartPoint, YstartPoint = self.RvresePolarToCartzien(self.TargetDataDF.index.to_series(), self.GroupLength, self.GroupLength,self.OX,self.OY)
            self.TargetDataDF = self.TargetDataDF.assign(YstartPoint=YstartPoint)
            self.TargetDataDF = self.TargetDataDF.assign(XstartPoint=XstartPoint)
            self.TargetDataDF = self.TargetDataDF.assign(Angle=Angles)

        except Exception as e:
            Log.AddToLog("exception in TargetData :  {}".format(e), 2)

    def CreatTargetCoorDF(self):
        # making a multi index matrix
        Log.AddToLog('CreatTargetCoorDF started')
        try:
            index_tuples = []
            for angle in self.TargetDataDF.Angle:
                for coor in ["X", "Y"]:
                    index_tuples.append([(angle), coor])
            index = pd.MultiIndex.from_tuples(index_tuples, names=["Angle", "Coordinate"])
            self.CooridnateDF = pd.DataFrame(columns=index)
        except Exception as e:
            Log.AddToLog("exception in CreatTargetCoorDF :  {}".format(e), 2)

    def BuildingTargetValues(self, sampels):
        Log.AddToLog('BuildingTargetValues started')
        try:
            # because time is constant 1/60 between step to step we can put the X,Y coordinate of the group location
            OneSecond = 1
            TotalTime = self.GroupDistance / self.Velocity
            TimeIntervals = np.arange(0, TotalTime, TotalTime / sampels)[0:sampels]
            if len(TimeIntervals) != sampels:
                print("Error in Building Target Vector")
            for Angle in self.TargetDataDF.Angle:
                angleRAD = Angle * 2 * np.pi / 360
                self.CooridnateDF.loc[:, ((Angle), 'X')] = float(
                    self.TargetDataDF.XstartPoint[Angle]) + (self.Velocity * TimeIntervals * np.cos(angleRAD))
                self.CooridnateDF.loc[:, ((Angle), 'Y')] = float(
                    self.TargetDataDF.YstartPoint[Angle]) + (self.Velocity * TimeIntervals * np.sin(angleRAD))
        except Exception as e:
            Log.AddToLog("exception in BuildingTargetValues :  {}".format(e), 2)


    '''
    def TargetMovmentLinearLine(self, EndPointX, EndPointY, OX, OY):
        # y=ax+b
        # calculatin a and b'
        Log.AddToLog('TargetMovmentLinearLine started')
        try:
            A = (EndPointY - OY) / (EndPointX - OX)
            B = EndPointY - A * EndPointX
            return (A, B)
        except Exception as e:
            Log.AddToLog("exception in TargetMovmentLinearLine :  {}".format(e), 2)
    
    
                
            for index, angle in enumerate(Angles):
                XstartPoint, YstartPoint = self.RvresePolarToCartzien(int(angle), self.GroupLength, self.GroupLength)
                XstartPoint += self.OX
                YstartPoint += self.OY
                
                # the following row not really needed, more for the future.
                XEndPoint, YEndPoint = self.RvresePolarToCartzien(int(angle), self.GroupDistance, self.GroupDistance)
                XEndPoint += XstartPoint
                YEndPoint += YstartPoint
                A, B = self.TargetMovmentLinearLine(XEndPoint, YEndPoint, XstartPoint, YstartPoint)
                # needed
                
                self.TargetDataDF[angle]= [XstartPoint,]
            
    '''
