import pygame
import pygame.gfxdraw
import time
import toGroup.WindowPropertys
from colorama import *
import matplotlib.pyplot as plt
import datetime
from threading import Thread
from Utils import Log
import pandas as pd
import numpy as np



class targetPin():
    def __init__(self,groupMembersInnerGroup,Velocity,GroupDistance,FramRate,GroupLength):
        self.MembersInGroup=groupMembersInnerGroup
        self.Velocity=Velocity
        self.GroupDistance=GroupDistance
        self.GroupLength=GroupLength
        self.FramRate=FramRate

    def RvresePolarToCartzien(self, angle, DistanceX, DistanceY,OX,OY):
        Log.AddToLog('RvresePolarToCartzien started')
        try:
            # gets an angle an the radius of the circle and returns the x and y points on the circle
            # 2pi=360
            # alpha[rad]
            X = DistanceX * np.cos(angle * 2 * np.pi / 360)+OX
            Y = DistanceY * np.sin(angle * 2 * np.pi / 360)+OY
            return (X, Y)
        except Exception as e:
            Log.AddToLog("exception in RvresePolarToCartzien :  {}".format(e), 2)



