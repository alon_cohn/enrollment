import cProfile
import pygame
import pygame.gfxdraw
from Utils import Log
import pandas as pd
import numpy as np
import time
from colorama import *

import matplotlib.pyplot as plt
from AcquisitioDataWays.TargetMovment1 import movment1T
from AcquisitioDataWays.TargetMovment2 import movment2T
from GUI.PinEntryGUI import pinEntryGUI
from threading import Thread, Lock
import os
from Configuration import ParameterManger as PM


# please look ata the attached pin chart
class pin:
    def __init__(self,AqucName=None):
        self.pinGUI = pinEntryGUI()

        #Not good enough , redandet  gui issue!
        self.groupMembersInnerGroup = self.pinGUI.groupMembersInnerGroup
        self.groupMembersOuterGroup = self.pinGUI.groupMembersOuterGroup
        self.OX = self.pinGUI.window.width / 2
        self.OY = self.pinGUI.window.height / 2
        self.Velocity = self.pinGUI.targetSpeed  # px/sec
        self.innerGroupLength = self.pinGUI.initialInnerGroupLength
        self.outerGroupLength = self.pinGUI.initialOuterGroupLength
        self.innerGroupRunDist = self.pinGUI.innerGroupRunDist  # should be 425 like in the experiment
        self.outerGroupRunDist = self.pinGUI.outerGroupRunDist
        self.FrameRate=self.pinGUI.frameRate
        self.window=self.pinGUI.window

        self.Target=movment1T(self.groupMembersInnerGroup,self.OX,self.OY,self.Velocity,self.innerGroupRunDist,self.FrameRate,self.innerGroupLength)
        self.Target2 = movment2T(self.groupMembersOuterGroup, self.Velocity, self.outerGroupRunDist, self.FrameRate,
                                 self.outerGroupLength,self.Target.TargetDataDF)
        self.DetectionAngle1 = PM.ReturnParameter('self.DetectionAngle1')
        self.DetectionAngle2 = PM.ReturnParameter('self.DetectionAngle2')
        self.DetectGaze = PM.ReturnParameter('self.DetectGaze')
        self.MaxDetectedAngle = 30
        self.DictAngle=self.NumbersAngleDict()
        self.power = 4
        self.password = 0
        self.FoundGroupOne = False
        self.FoundGroupTwo = False
        self.Movement1DF = None
        self.Movement2DF = None

    def NumbersAngleDict(self):
        """

        :return: a dict which translate angle to number
        """
        Log.AddToLog("NumbersAngleDict started ")
        try:
            DictAngle={}
            j=0
            step=(360)/self.groupMembersInnerGroup
            for i in range (0,self.groupMembersInnerGroup*2,2):
                Firstgroup = int(180 + j * step)
                DictAngle[str(Firstgroup)] = {}
                DictAngle[str(Firstgroup)]['180'] = i
                DictAngle[str(Firstgroup)]['360'] = i+1
                j+=1
            return DictAngle
        except Exception as e:
            Log.AddToLog("exception in NumbersAngleDict :  {}".format(e), 2)
            return False

    def RunGUI(self,Tracker, Screen, amountOfFeatureVectores,digit=None):
        """
        the main runner of the program!
        :param Tracker: an object which collect data
        :param Screen: diiferent program screens
        :param amountOfFeatureVectores: how many feature
        :param digit: what is the correct digit
        :return: True if succed to trace the correct digit!
        """
        Log.AddToLog("RunGUI started ")
        try:
            self.pinGUI.init_graphics()
            self.pinGUI.window.getScreenWithBlackBackground()
            Screen.ExplainPin(amountOfFeatureVectores+1)
            pygame.mouse.set_visible(False)
            self.thread_lock =Lock()
            StartCollectingData = Thread(target=Tracker.StartTracking, args=(self.thread_lock,))
            while not self.FoundGroupTwo:
                self.pinGUI.staticMainScreen()
                while not self.FoundGroupOne:
                    self.pinGUI.staticMainScreen()

                    '''------------------------------------------------Movement1-------------------------------------'''
                    Tracker.Note = "Movement1"
                    #thread can not run twice, therethore it will only start the thread if it not running yet.
                    if not Tracker.trackerRunning:
                        print('Start to collect Data, pin')
                        StartCollectingData.start()
                    self.pinGUI.dynamicScreenMovement1()

                    '''------------------Transition---------------------Transition-----------------------------------'''
                    Tracker.Note = "Transition"
                    self.pinGUI.staticTransitionScreen()
                    if not self.calculationMovement1(Tracker):
                        print(Fore.RED + 'Failed to collect ValidData at Movement1 ,Again ' + Fore.RESET)
                        self.reset_movement(movment_number=1, Tracker=Tracker)
                        Screen.MovementFailed()
                    else:
                        self.FoundGroupOne = True
                        print(Fore.GREEN + 'collect ValidData at Movement1  ' + Fore.RESET)

                '''------------------Movement2------------------------Movement2--------------------------------------'''
                Tracker.Note = "Movement2"
                self.pinGUI.dynamicScreenMovement2()

                '''-------------------------------------------------FinalCalculation---------------------------------'''
                Tracker.Note = "FinalCalculation"
                self.pinGUI.dynamicReturnScreen()
                if not self.calculationMovement2(Tracker):
                    print(Fore.RED + 'Failed to collect ValidData at Movement2 ,Again ' + Fore.RESET)
                    self.reset_movement(movment_number=2, Tracker=Tracker)
                    Screen.MovementFailed()
                else:
                    self.FoundGroupTwo = True
                    Tracker.EndWriting = True
                    # while the thread have not ended!
                    while Tracker.trackerRunning:
                        time.sleep(0.001)
                    Log.AddToLog("Tracker.EndWriting changed to {} ".format(Tracker.EndWriting))
                    print(Fore.GREEN + 'collect ValidData at Movement2' + Fore.RESET)

            return(self.after_process(Tracker, Screen, digit))
        except Exception as e:
            Log.AddToLog("exception in RunGUI :  {}".format(e), 2)
            return False

    def after_process(self,Tracker, Screen, digit):
        """
        1. check for digit correctness
        2. the df is including target position
        :param Tracker:
        :param Screen:
        :param digit:
        :return: True if the digit is correct false otherwise
        """
        Log.AddToLog("after_process started ")
        try:
            if not self.Combine_DF(Tracker):
                print(Fore.RED + 'Did not manage to build a One data frame  ' + Fore.RESET)
                Log.AddToLog("Did not manage to build a One data frame, exiting the program ")
                return False
            detected_digit =  self.DictAngle[str(self.ChoosenAngle1)][str(self.ChoosenAngle2)]
            print ('The chosen number is {} and the target number is {}'.format(detected_digit, digit))
            #self.plotshow(self.Target.CooridnateDF, self.Movement1DF, '1')
            #self.plotshow(self.Target2.CooridnateDF[self.ChoosenAngle1], self.Movement2DF, '2')
            if digit != None:
                if digit == detected_digit:
                    print(Fore.GREEN + 'the user indentifyed correct digit ' + Fore.RESET)
                    Screen.correct_digit()
                    return True
                else:
                    print(Fore.RED + 'the user indentifyed incorrect digit ' + Fore.RESET)
                    Screen.wrong_digit()
                    return False
            else:
                return True
        except Exception as e:
            Log.AddToLog("exception in after_process :  {}".format(e), 2)
            return False

    def Combine_DF(self,Tracker):
        """

        :param Tracker:
        :return: Tracker.EyeDataDF is now includes the 2 more columns with the target position
        """
        Log.AddToLog("Combine_DF started ")
        try:
            Tracker.EyeDataDF = Tracker.EyeDataDF.assign(TargetX=np.nan)
            Tracker.EyeDataDF = Tracker.EyeDataDF.assign(TargetY=np.nan)

            Log.AddToLog(
                'shape of EyeDataDF Movement1 ={} ,shape of EyeDataDF Movement2={} ,shape of Target Movement1 = {},shape of target Movement2 ={}'.format(
                    Tracker.EyeDataDF.TargetX[Tracker.EyeDataDF.Note == 'Movement1'].shape[0],
                    Tracker.EyeDataDF.TargetX[Tracker.EyeDataDF.Note == 'Movement2'].shape[0],
                    self.Target.CooridnateDF[self.ChoosenAngle1]['X'].shape[0],
                    self.Target2.CooridnateDF[self.ChoosenAngle1][self.ChoosenAngle2]['Y'].shape[0]))
            print(
                'shape of EyeDataDF Movement1 ={} ,shape of EyeDataDF Movement2={} ,shape of Target Movement1 = {},shape of target Movement2 ={}'.format(
                    Tracker.EyeDataDF.TargetX[Tracker.EyeDataDF.Note == 'Movement1'].shape[0],
                    Tracker.EyeDataDF.TargetX[Tracker.EyeDataDF.Note == 'Movement2'].shape[0],
                    self.Target.CooridnateDF[self.ChoosenAngle1]['X'].shape[0],
                    self.Target2.CooridnateDF[self.ChoosenAngle1][self.ChoosenAngle2]['Y'].shape[0]))

            Tracker.EyeDataDF.loc[Tracker.EyeDataDF.index[Tracker.EyeDataDF.Note == 'Movement1'], 'TargetX'] = \
                self.Target.CooridnateDF[self.ChoosenAngle1]['X'].values
            Tracker.EyeDataDF.loc[Tracker.EyeDataDF.index[Tracker.EyeDataDF.Note == 'Movement1'], 'TargetY'] = \
                self.Target.CooridnateDF[self.ChoosenAngle1]['Y'].values
            Tracker.EyeDataDF.loc[Tracker.EyeDataDF.index[Tracker.EyeDataDF.Note == 'Movement2'], 'TargetX'] = \
                self.Target2.CooridnateDF[self.ChoosenAngle1][self.ChoosenAngle2]['X'].values
            Tracker.EyeDataDF.loc[Tracker.EyeDataDF.index[Tracker.EyeDataDF.Note == 'Movement2'], 'TargetY'] = \
                self.Target2.CooridnateDF[self.ChoosenAngle1][self.ChoosenAngle2]['Y'].values

            return True
        except Exception as e:
            Log.AddToLog("exception in Combine_DF :  {}".format(e), 2)
            return False


    def calculationMovement1(self,Tracker):
        """
        detect the angle a according the to the classic algorithem and the alternative algorithem
        choose the smallest between the 2.
        detect the gaze length (magnitude)
        check if gaze>min gze lenth, angle is < then max angle.
        :param Tracker:
        :return: True if stand in condition, False other wsie
        """
        Log.AddToLog('calculationMovement1 started')
        try:
            # create a df from the data
            self.thread_lock.acquire()
            temp = pd.DataFrame(Tracker.gui_data_list)
            self.Movement1DF = temp.loc[temp['Note'] == 'Movement1'].copy()
            self.thread_lock.release()
            self.Movement1DF = self.Movement1DF.reset_index(drop=True)

            #start calculation
            start = time.perf_counter()
            self.EyeLineProperties(self.Movement1DF)
            m1_shape = self.Movement1DF.shape[0]
            self.Target.BuildingTargetValues(m1_shape)
            ClassicAlgorithemMinAngle, ClassicMinAngleGroup = self.ClassicAlgorithem(self.Target.CooridnateDF,self.Movement1DF)
            self.Target.TargetDataDF = self.Target.TargetDataDF.assign(CrossAnglesClassic=self.CorssAngleList)
            AlternativeAlgorithemMinAngle, AlternaticMinAngleGroup = self.AlternitveAlgorithem(Tracker)
            if (ClassicAlgorithemMinAngle<self.DetectionAngle1 or AlternativeAlgorithemMinAngle<self.DetectionAngle1) and self.GazeLength>self.DetectGaze:
                print('movement1 stands in requested critir criteria')
                if (ClassicMinAngleGroup)!= AlternaticMinAngleGroup:
                    print((Fore.RED + '"Pay attention that the Algorithems returned different results"' ))
                    if ClassicMinAngleGroup < AlternaticMinAngleGroup:
                        self.ChoosenAngle1=ClassicMinAngleGroup
                        Log.AddToLog('Choose angle = {}'.format(ClassicMinAngleGroup))
                    else:
                        self.ChoosenAngle1 = AlternaticMinAngleGroup
                        Log.AddToLog('Choose angle = {}'.format(AlternaticMinAngleGroup))
                else:
                    self.ChoosenAngle1 = AlternaticMinAngleGroup
                    Log.AddToLog('Choose angle = {}'.format(AlternaticMinAngleGroup))
                print('calculation time = {}'.format(time.perf_counter() - start))
                return True
            else:
                return False
        except Exception as e:
            Log.AddToLog("exception in Movement1 :  {}".format(e), 2)
            return False

    def reset_movement(self,movment_number, Tracker):
        """

        :param movment_number: 1/2 depend on the movement note
        :param Tracker:
        :return: inilitze tracker and pin object
        """
        Log.AddToLog("reset_movement started ")
        try:
            print('movement {} do not stands in criteria'.format(movment_number))
            Log.AddToLog('movement {} do not stands in criteria'.format(movment_number))
            Tracker.Reset()
            self.reset()
        except Exception as e:
            Log.AddToLog("exception in reset_movement1 :  {}".format(e), 2)

    def CorssAngleListCreator(self,CooridnateDF,EyeDF):
        Log.AddToLog("CorssAngleListCreator started")
        try:
            self.CorssAngleDict={}
            self.CorssAngleList =[]
            for Angle in CooridnateDF._info_axis._levels[0]:
                CrossAngle=self.VectorCompare(self.EyesXEndPoint-np.median(EyeDF.RawX[0:3]),self.EyesYEndPoint-np.median(EyeDF.RawY[0:3]),
                                              float(CooridnateDF[Angle,'X'][-1:]-CooridnateDF[Angle,'X'][0]),
                                              float(CooridnateDF[Angle,'Y'][-1:]-CooridnateDF[Angle,'Y'][0]))
                self.CorssAngleList.append(CrossAngle)
                self.CorssAngleDict[str(Angle)]=CrossAngle
        except Exception as e:
            Log.AddToLog("exception in CorssAngleListCreator :  {}".format(e), 2)

    def ClassicAlgorithem(self,CooridnateDF,EyeDF):
        """

        :param CooridnateDF:
        :param EyeDF:
        :return:
        """
        Log.AddToLog("ClassicAlgorithem started")
        try:
            #self.Target.TargetDataDF = self.Target.TargetDataDF.assign(CrossAnglesClassic=np.nan)
            # self.Target.TargetDataDF.CrossAnglesClassic[(Angle)]=CrossAngle
            self.CorssAngleListCreator(CooridnateDF,EyeDF)
            MinAngleGroup = min(self.CorssAngleDict,key=self.CorssAngleDict.get)
            ClassicAlgorithemMinAngle = self.CorssAngleDict[MinAngleGroup]
            Log.AddToLog('The Min angle according to the Classic Algorithm is {} which appear at Angle {}'.format(ClassicAlgorithemMinAngle,MinAngleGroup))
            print('The Min angle according to the Calssic Algorithm is {} which appear at Angle {}'.format(ClassicAlgorithemMinAngle,MinAngleGroup))
            return ClassicAlgorithemMinAngle,int(MinAngleGroup)
        except Exception as e:
            Log.AddToLog("exception in ClassicAlgorithem :  {}".format(e), 2)

    def AlternitveAlgorithem(self,Tracker):
        """

        :param Tracker:
        :return:
        """
        Log.AddToLog('AlternitveAlgorithem started')
        try:
            CorssAngleList = []
            for Angle in self.Target.TargetDataDF.Angle:
                CrossAngle = self.VectorCompareList(self.Movement1DF['RawX'], self.Movement1DF['RawY'] , self.Target.CooridnateDF[(Angle)]['X'],
                                                    self.Target.CooridnateDF[(Angle)]['Y'])
                CorssAngleList.append(CrossAngle)
            self.Target.TargetDataDF = self.Target.TargetDataDF.assign(CrossAnglesAlt=CorssAngleList)
            AltAlgorithemMinAngle = self.Target.TargetDataDF.CrossAnglesAlt.min()
            MinAngleGroup = int(self.Target.TargetDataDF.Angle[self.Target.TargetDataDF.CrossAnglesAlt == AltAlgorithemMinAngle].values)
            Log.AddToLog('The Min angle according to the Alternativ Algorithm is {} which appear at Angle {}'.format(
                AltAlgorithemMinAngle, MinAngleGroup))
            print('The Min angle according to the Alternativ Algorithm is {} which appear at Angle {}'.format(
                AltAlgorithemMinAngle, MinAngleGroup))
            return AltAlgorithemMinAngle,MinAngleGroup
        except Exception as e:
            Log.AddToLog("exception in AlternitveAlgorithem :  {}".format(e), 2)

    def plotshow(self,CooridnateDF ,EyeDF,MovemenNummber):
        """
        can show the gaze plot
        :param CooridnateDF:
        :param EyeDF:
        :param MovemenNummber:
        :return:
        """
        Now=time.perf_counter()
        Log.AddToLog('plotshow started')
        try:
            i=0
            colo=['b','g','r','y','m','k','c','w']
            angle=CooridnateDF.keys().levels[0][0]
            ax=CooridnateDF.plot(kind="scatter", x=(angle, 'X'), y=(angle, 'Y'), color=colo[0], label=angle)
            for angle in  CooridnateDF.keys().levels[0]:
                if angle==CooridnateDF.keys().levels[0][0]:
                    continue
                i=i+1
                if colo[i] == 'w':
                    i = 0
                CooridnateDF.plot(x=(angle, 'X'), y=(angle, 'Y'), color=colo[i], label=angle, ax=ax)
            EyeDF.plot(x='RawX', y='RawY', color=colo[i+1], label='Eye', ax=ax)
            ax.set_xlabel("X[PX]")
            ax.set_ylabel("Y[PX]")
            path = os.getcwd()
            SavingPath = path+'\LogFiles' + '\movement' + MovemenNummber + str(Now) + '.png'
            plt.savefig(SavingPath)
            Log.AddToLog('Finish Plotting')
        except Exception as e:
            Log.AddToLog("exception in plotshow :  {}".format(e), 2)

    def EyeLineProperties(self,EyesDF):
        """
        get an  df and calculate the gaze length (size)
        :param EyesDF:
        :return:
        """
        Log.AddToLog('TargetMovementLinearLine started')
        try:
            self.EyesXEndPoint = EyesDF.RawX.iloc[-3:].median()
            self.EyesYEndPoint = EyesDF.RawY.iloc[-3:].median()
            XabsoultSize = self.EyesXEndPoint - EyesDF.RawX.iloc[0]
            YabsoultSize = self.EyesYEndPoint - EyesDF.RawY.iloc[0]
            self.GazeLength=np.sqrt(XabsoultSize**2 + YabsoultSize**2)
            print('GazeLength Size = {}'.format(self.GazeLength))
            Log.AddToLog('GazeLength Size = {}'.format(self.GazeLength))
        except Exception as e:
            Log.AddToLog("exception in TargetEndPoints :  {}".format(e), 2)

    #def vectorcompare(self, speye_x, speye_y, target_x, target_y):
    def VectorCompare(self, EyesXEndPoint, EyesYEndPoint, target_x, target_y):
        """
        klaas!!@!@@
        :param EyesXEndPoint:
        :param EyesYEndPoint:
        :param target_x:
        :param target_y:
        :return:
        """
        Log.AddToLog('VectorCompare started')
        try:
            # compares the vector of eye movement to the vector of target
            vectorlisteye = (EyesXEndPoint, EyesYEndPoint)
            vectorlisttarget = (target_x, target_y)
            vector_eye = np.array(vectorlisteye)
            vector_target = np.array(vectorlisttarget)
            dot = np.dot(vector_eye, vector_target)
            x_modulus = np.sqrt((vector_eye * vector_eye).sum())
            y_modulus = np.sqrt((vector_target * vector_target).sum())
            try:
                cos_angle = dot / (x_modulus * y_modulus)  # cosine of angle between x and y
            except RuntimeWarning:
                print('why????')
            except Exception as e:
                print('ttttttt')
            else:
                angle = np.arccos(cos_angle)
                return (angle/(2*np.pi)*360)  #return a degrees Value
        except Exception as e:
            Log.AddToLog("exception in VectorCompare :  {}".format(e), 2)


    def VectorCompareList(self,eyeXList,eyeYList,targetXList,targetYList):
        """
        Klaas!@!!!!
        :param eyeXList:
        :param eyeYList:
        :param targetXList:
        :param targetYList:
        :return:
        """
        Log.AddToLog('VectorCompareList started')
        try:
            anglesMean = []
            eyeXList=list(eyeXList)
            eyeYList=list(eyeYList)
            targetXList = list(targetXList)
            targetYList = list(targetYList)
            for k in range(0,len(eyeXList)-1):
                    angle=((self.VectorCompare(eyeXList[-1]-eyeXList[k],eyeYList[-1]-eyeYList[k],
                                        targetXList[-1]-targetXList[k],targetYList[-1]-targetYList[k])))
                    anglesMean.append(angle)
            return np.nanmedian(anglesMean)
        except Exception as e:
            Log.AddToLog("exception in VectorCompareList :  {}".format(e), 2)

    def reset(self):
        try:
            del self.Target.CooridnateDF
            self.Target.CreatTargetCoorDF()
            del self.Target2.CooridnateDF
            self.Target2.CreatTargetCoorDFMovement2()
            self.FoundGroupOne = False
            self.FoundGroupTwo = False
            self.Movement1DF = None
            self.Movement2DF = None
        except Exception as e:
            Log.AddToLog("exception in reset PIN :  {}".format(e), 2)
        

    '''--------------------------------------------------------------------------------------------------------------'''

    def calculationMovement2(self,Tracker):
        # Analyzind the Data from Movement1
        Log.AddToLog('Movement2 strated')
        try:

            self.thread_lock.acquire()
            temp = pd.DataFrame(Tracker.gui_data_list)
            self.Movement2DF = temp.loc[temp['Note'] == 'Movement2'].copy()
            self.thread_lock.release()

            self.Movement2DF = self.Movement2DF.reset_index(drop= True)
            if self.Movement2DF.empty:
                print("Dataframe2 is empty!!!")
                return False
            try:
                print('sample in m2 = {} '.format(self.Movement2DF.shape[0]))
                self.Target2.EnterValues(self.Target.CooridnateDF,self.Movement1DF,self.Movement2DF.shape[0])
            except:
                print(self.Movement2DF)
            self.EyeLineProperties(self.Movement2DF)
            ClassicAlgorithemMinAngle, ClassicMinAngleGroup = self.ClassicAlgorithem(self.Target2.CooridnateDF[self.ChoosenAngle1], self.Movement2DF)
            if ClassicAlgorithemMinAngle < self.DetectionAngle2 and self.GazeLength > self.DetectGaze:
                print('movement2 stands in the Angle & the gaze length Cratiria')
                Log.AddToLog('Choose angle = {}'.format(ClassicMinAngleGroup))
                self.ChoosenAngle2 = ClassicMinAngleGroup
                Log.AddToLog('min angle = {}'.format(ClassicAlgorithemMinAngle))
                return True
            else:
                return False
        except Exception as e:
            Log.AddToLog("exception in Movement2 :  {}".format(e), 2)
            return False


if __name__ == "__main__":
    from Devices.GP3 import gp3
    from GUI.InfoScreens import infoScreens
    from Configuration import Globales
    Globales.initialize()
    tracker = gp3('gp3','pin','a')
    i = infoScreens()
    p = pin(AqucName='a')
    p.RunGUI(Tracker=tracker,Screen=i,amountOfFeatureVectores=1,digit=3)


"""
# # if you are at the end of the sequence turn off the thread whe condition to end is in startTracking
            # if Tracker.Note == "FinalCalculation":
            #     Tracker.EndWriting=True
            #     # while the thread have not ended!
            #     while Tracker.trackerRunning:
            #         continue
            #     self.calculationMovement2(Tracker)
            #     Log.AddToLog("Tracker.EndWriting changed to {} ".format(Tracker.EndWriting))
            # after collecting data asking if it is relevant.´for each Note
            
                def RunGUI(self,Tracker, Screen, amountOfFeatureVectores,digit=None):
        # digit only exist in enrollment
        Log.AddToLog("RunGUI started ")
        try:
            self.pinGUI.init_graphics()
            self.pinGUI.window.getScreenWithBlackBackground()
            Screen.ExplainPin(amountOfFeatureVectores+1)
            self.FoundGroupOne = False
            self.FoundGroupTwo = False
            pygame.mouse.set_visible(False)
            self.thread_lock =Lock()
            StartCollectingData = Thread(target=Tracker.StartTracking, args=(self.thread_lock,))
            print('Start to collect Data')
            while not self.FoundGroupTwo:
                self.pinGUI.staticMainScreen()
                '''------------------------------------------------Movement1-----------------------------------------'''
                while not self.FoundGroupOne:
                    self.pinGUI.staticMainScreen()
                    Tracker.Note = "Movement1"
                    #thread can not run twice, therethore it will only start the thread if it not running yet.
                    if not Tracker.trackerRunning:
                        StartCollectingData.start()
                    self.pinGUI.dynamicScreenMovement1()
                    # if not self.ChangeScreen(Tracker):
                    #     return False
                    '''------------------Transition---------------------Transition-----------------------------------'''
                    Tracker.Note = "Transition"
                    self.pinGUI.staticTransitionScreen()
                    self.calculationMovement1(Tracker)
                    # if not self.ChangeScreen(Tracker):
                    #     return False
                    if not self.FoundGroupOne:
                        print(Fore.RED + 'Failed to collect ValidData at Movement1 ,Again ' + Fore.RESET)
                        Screen.MovementFailed()
                    else:
                        print(Fore.GREEN + 'collect ValidData at Movement1  ' + Fore.RESET)
                '''------------------Movement2------------------------Movement2--------------------------------------'''
                Tracker.Note = "Movement2"
                if not self.ChangeScreen(Tracker):
                    return False
                '''-------------------------------------------------FinalCalculation---------------------------------'''
                Tracker.Note = "FinalCalculation"
                if not self.ChangeScreen(Tracker):
                    return False
                if not self.FoundGroupTwo:
                    print(Fore.RED + 'Failed to collect ValidData at Movement2 ,Again ' + Fore.RESET)
                    Screen.MovementFailed()
                else:
                    print(Fore.GREEN + 'collect ValidData at Movement2' + Fore.RESET)
            return(self.after_process(Tracker, Screen, digit))
        except Exception as e:
            Log.AddToLog("exception in RunGUI :  {}".format(e), 2)
            return False
            
        # # in case the thread i slower.
            # while Tracker.EyeDataDF.Note.iloc[-1] != 'FinalCalculation':
            #     pass
            # #temp=Tracker.EyeDataDF.copy()
            # self.Movement2DF = pd.DataFrame()
            # boolenIndexer = Tracker.EyeDataDF.Note == 'Movement2'
            # self.Movement2DF = Tracker.EyeDataDF[boolenIndexer].copy() 
            
            # while Tracker.EyeDataDF.Note.iloc[-1] != 'Transition':
            #     time.sleep(0.001)
            #self.Movement1DF = Tracker.EyeDataDF.loc[Tracker.EyeDataDF['Note'] == 'Movement1'].copy()
            # boolenanSeries = Tracker.EyeDataDF.Note == 'Movement1'
            # self.Movement1DF = Tracker.EyeDataDF[boolenanSeries].copy()
            #if  self.Movement1DF.shape[0]      
            
            
                def ChangeScreen(self,Tracker):
        Log.AddToLog("ChangeScreen started for {}".format(Tracker.Note))
        try:
            Tracker.SampleNumber=0
            Tracker.TimeUniqueness = 0
            # if the Psuh data flag is False before i told it to be meaning something got wrong at the process.
            if not Tracker.trackerRunning:
                return False

            if Tracker.Note == "Movement1":
                self.pinGUI.dynamicScreenMovement1()

            elif Tracker.Note == "Transition":
                self.pinGUI.staticTransitionScreen()
                self.calculationMovement1(Tracker)

            elif Tracker.Note == "Movement2":
                self.pinGUI.dynamicScreenMovement2()

            elif Tracker.Note == "FinalCalculation":
                self.pinGUI.dynamicReturnScreen()
                Tracker.EndWriting = True
                # while the thread have not ended!
                while Tracker.trackerRunning:
                    continue
                self.calculationMovement2(Tracker)
                Log.AddToLog("Tracker.EndWriting changed to {} ".format(Tracker.EndWriting))

            if not Tracker.IsDataValid(self.window.width, self.window.height):
                self.reset_movement(1,Tracker=Tracker)
                print(Fore.RED + 'Failed to collect ValidData at PIN return False ' + Fore.RESET)
                return False
            return True
        except Exception as e:
            print('Error in changeScreen: {}'.format(e))
            Log.AddToLog("exception in ChangeScreen :  {}".format(e), 2)
            return False
            
            
                '''
        self.window = WindowPropertys.windowPropertys()
        self.window.setScreenSize()#left top is zero zero
        self.groupMembersInnerGroup = 5
        self.groupMembersOuterGroup = 2
        self.OX=self.window.width/2
        self.OY = self.window.height/2
        self.Velocity=500 # px/sec
        self.innerGroupLength = self.window.valueResizer(50)       #320
        self.outerGroupLength = self.window.valueResizer(70)  # 70
        self.innerGroupRunDist = self.window.valueResizer(370)  # 370
        self.outerGroupRunDist = self.window.valueResizer(265)  #265
        self.FrameRate=60
        '''
        
        # Tracker.EyeDataDF.TargetX[Tracker.EyeDataDF.Note == 'Movement1'] = self.Target.CooridnateDF[self.ChoosenAngle1]['X'].values
            # Tracker.EyeDataDF.TargetY[Tracker.EyeDataDF.Note == 'Movement1'] = self.Target.CooridnateDF[self.ChoosenAngle1]['Y'].values
            #
            # Tracker.EyeDataDF.TargetX[Tracker.EyeDataDF.Note == 'Movement2'] = self.Target2.CooridnateDF[self.ChoosenAngle1][self.ChoosenAngle2]['X'].values
            # Tracker.EyeDataDF.TargetY[Tracker.EyeDataDF.Note == 'Movement2'] = self.Target2.CooridnateDF[self.ChoosenAngle1][self.ChoosenAngle2]['Y'].values
        
        
"""