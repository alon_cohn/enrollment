import os
import glob
import pandas as pd
import datetime
from DataSc.CaliData import caliData
from DataSc import Log
from colorama import *


class preprocessingMatrix():
    def __init__(self):
        #Todo: This only works locally
        self.PathDataCollection = 'C:\\Users\\Stefan\\Documents\\oculid\\03_Technik\\01_MachineLearning\\02_Prototyp_v1\\01_Data\\Testungen\\Testung_03\\DatenFiles\\*.txt'
        #self.PathDataPrototypeMeta = 'C:/Users/Stefan/Documents/oculid/03_Technik/01_MachineLearning/02_Prototyp_v1/01_Data/PrototypeData/*.txt'
        #self.PathDataPrototype = 'C:/Users/Stefan/Documents/oculid/03_Technik/02_SoftwareDevelopment/oculid/LogFiles/'
        store_folder = datetime.datetime.now().strftime("%I%M%p_%B%d_%Y")
        store_newpath_top = os.path.join('C:/Users/Stefan/Documents/oculid/03_Technik/01_MachineLearning/02_Prototyp_v1/06_Preprocessing', store_folder)
        try:
            os.makedirs(store_newpath_top)
        except:
            os.makedirs(store_newpath_top + '_v1')
        self.PathStorePreprocessing = store_newpath_top
        self.runner()

    def runner(self):
        #read the data
        self.readAppendDataCollection()
        self.dfFeatureDataCollection =  self.doCaliPreprocessing(self.dfDataCollection)
        self.dfFeatureDataCollection.to_csv(os.path.join(self.PathStorePreprocessing, 'CompleteCaliFeatureMatrix.csv'),index = False)
        '''
        self.readAppendDataPrototype()
        self.dfFeatureDataPrototype =  self.doCaliPreprocessing(self.dfDataCollectionPrototype)
        self.dfFeatureDataPrototype.to_csv(os.path.join(self.PathStorePreprocessing, 'PrototypeCaliFeatureMatrix.csv'),index = False)

        self.CompleteFeatureMatrix = pd.concat([self.dfFeatureDataCollection, self.dfFeatureDataPrototype], axis = 0)
        self.CompleteFeatureMatrix.to_csv(os.path.join(self.PathStorePreprocessing, 'CompleteCaliFeatureMatrix.csv'),index = False)
        '''
    def readAppendDataCollection(self, sequence = 'Calibration'):
        Log.AddToLog('readAppendDataCollection started ')
        try:
            '''Read all Files to a DataFrame'''
            # get paths for all files
            filenames = glob.glob(self.PathDataCollection)
            self.dfDataCollection = pd.concat((pd.read_csv(f) for f in filenames))
            self.dfDataCollection = self.dfDataCollection[self.dfDataCollection.Marker == sequence]
            '''
            # create empty DataFrame
            self.dfDataCollection = pd.DataFrame()
            for file in filenames:
                Log.AddToLog('Entering read file loop in readAppendDataCollection')
                try:
                    print(file)
                    dfTemp = pd.read_csv(file, low_memory=False)
                    dfTemp = dfTemp[dfTemp.Marker == sequence]
                    print(dfTemp.shape)
                    self.dfDataCollection = pd.concat([self.dfDataCollection, dfTemp], axis = 0)
                    print(self.dfDataCollection.shape)
                except Exception as e:
                    print('Exception {} in File: {}\n could not be read'.format(e, file))
                    Log.AddToLog('Exception {} in for loop for File: {}\n could not be read'.format(e, file))
                    pass
            '''
            self.dfDataCollection.to_csv(os.path.join(self.PathStorePreprocessing, 'dfDataCollectionCaliRaw.csv'),index = False)

        except Exception as e:
            Log.AddToLog("Exception in readAppendDataCollection :  {}".format(e), 2)
            print(Fore.RED + 'Exception in readAppendDataCollection. See Log' + Fore.RESET)

    def readAppendDataPrototype(self):
        Log.AddToLog('readAppendDataPrototype started')
        try:
            filenames = glob.glob(self.PathDataPrototypeMeta)
            self.dfDataCollectionPrototype = pd.DataFrame()
            file_nr = 0
            '''Read all Files to a DataFrame'''
            for file in filenames:
                try:
                    Log.AddToLog('Entering read file loop in readAppendDataPrototype')
                    # create artificial MoveID
                    file_nr += 1
                    # read in meta files
                    dfvalid = pd.read_csv(file)
                    # check ID in Metafile, if nan discard this file and continue
                    if (str(dfvalid.ID[0]) == 'nan') | (str(dfvalid.ID[0]) == ''):
                        print('Prototype File not valid and will be discarded')
                        Log.AddToLog('Prototype File not valid and will be discarded')

                        pass
                    else:
                        ID = str(dfvalid.ID[0]) + '_prot'
                        print(ID)

                    input_link = pd.read_csv(file, sep=',')
                    # read data from logfile
                    dfTemp = pd.read_csv(self.PathDataPrototype + input_link.CALIBRATION_LOG_FILE.iloc[0])
                    # Filter for Brightness Change, Pre Baseline and Post Baseline
                    dfTemp = dfTemp[(dfTemp['Note'] == 'Pre Baseline') | (dfTemp['Note'] == 'Post Baseline') | (dfTemp['Note'] == 'Brightness Change')]
                    dfTemp['ID'] = ID
                    dfTemp['MoveID'] = file_nr
                    self.dfDataCollectionPrototype = pd.concat([self.dfDataCollectionPrototype, dfTemp], axis=0)

                except Exception as e:
                    print('Exception {} in File: {}\n could not be read'.format(e, file))
                    Log.AddToLog('Exception {} in for loop for File: {}\n could not be read'.format(e, file))
                    pass

            self.dfDataCollectionPrototype.to_csv(os.path.join(self.PathStorePreprocessing, 'dfDataCollectionPrototypeCaliRaw.csv'),index = False)
        except Exception as e:
            Log.AddToLog("Exception in readAppendDataPrototype :  {}".format(e), 2)
            print(Fore.RED + 'Exception in readAppendDataPrototype. See Log' + Fore.RESET)



    ''' Preprocessing Function '''
    def doCaliPreprocessing(self, dfRaw):
        Log.AddToLog('doCaliPreprocessing started')
        try:
            Log.AddToLog('Entering read file loop in readAppendDataPrototype')
            dfFeatures = pd.DataFrame()
            for key, df in dfRaw.groupby(['ID', 'MoveID']):
                print(key)
                p = caliData(df, 'SMI')
                if p.runner():
                    tempFeature = p.FinalFeatures
                    tempFeature = tempFeature.assign(ID=key[0])
                    tempFeature = tempFeature.assign(MoveID=key[1])
                    dfFeatures = pd.concat([dfFeatures, tempFeature], axis=0)
                else:
                    print('Data Frame could not be processed')
                    continue
        except Exception as e:
            Log.AddToLog("Exception in doCaliPreprocessing:  {}".format(e), 2)
            print(Fore.RED + 'Exception in doCaliPreprocessing. See Log' + Fore.RESET)
        return dfFeatures