import pandas as pd
import numpy as np
from xgboost import XGBClassifier
from imblearn.pipeline import Pipeline
from imblearn.over_sampling import SMOTE
from DataSc import Log
from sklearn import preprocessing
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score, roc_curve
from scipy import interp
import matplotlib.pyplot as plt
import random
from colorama import *
from Configuration import ParameterManger as PM


class pinmodel():
    def __init__(self, FiveV):
        self.dfNoUser = pd.read_csv('C:\Qsync\Temp_Data\Pin_Pre_Processing_Matrix.csv')
        self.dfUser = FiveV

    def Modelrunner(self):
        Log.AddToLog('Modelrunner for Model Building started')
        try:
            if self.createFeatureMatrix(n_imposter=2):
                print(Fore.GREEN + 'createFeatureMatrix: sucessfull' + Fore.RESET)
                if self.setupModel():
                    print(Fore.GREEN + 'setupModel: sucessfull' + Fore.RESET)
                    # Threshold just for debug
                    AucThreshold = PM.ReturnParameter('AucThreshold')
                    if self.validateModel(aucThreshold=AucThreshold):
                        print(Fore.GREEN + 'validateModel: sucessfull' + Fore.RESET)
                    else:
                        print(Fore.RED + 'validateModel: Failed' + Fore.RESET)
                        return False, 0, 0, 0
                else:
                    print(Fore.RED + 'setupModel: Failed' + Fore.RESET)
                    return False, 0, 0, 0
            else:
                print(Fore.RED + 'createFeatureMatrix: sucessfull' + Fore.RESET)
                return False, 0, 0, 0
            return (True, self.clf, self.eerThreshold, self.eer)
        except Exception as e:
            Log.AddToLog("exception in Modelrunner :  {}".format(e), 2)
            return False, 0, 0, 0

    # sets up the data to be used for ModelBuilding
    def createFeatureMatrix(self, n_imposter):
        Log.AddToLog('createFeatureMatrix for Model Building started')
        try:
            # print(self.dfNoUser.Revision.unique(), self.dfUser.Revision.unique())
            # check revision of Feature Matrix and Five Feature vectors
            if self.dfNoUser.Revision.isin(self.dfUser.Revision).all():
                Log.AddToLog('createFeatureMatrix: Revision of Feature Vectors and Feature Matrix matches')
                # remove unnecessary columns
                self.dfNoUser = self.dfNoUser.drop(columns=['MoveID', 'Revision', 'Note','Target_velocity_px_s_Movement1','Target_velocity_px_s_Movement2'])
                self.dfUser = self.dfUser.drop(columns=['Revision', 'Note','Target_velocity_px_s_Movement1','Target_velocity_px_s_Movement2'])

                self.dfNoUser.ID = [s.split('_')[0] for s in self.dfNoUser.ID]
                self.dfUser['ID'] = 'User'
                # check if feature columns of user/no user match
                c=0
                for column in self.dfNoUser.columns:
                    if column in self.dfUser.columns:
                        c += 1
                    else:
                        print(column)
                if (self.dfNoUser.columns == self.dfUser.columns).all():
                    # merge data frames
                    dfModel = pd.concat([self.dfNoUser, self.dfUser], axis=0)

                    # current users in the data base
                    curr_sub = list(dfModel.ID[~dfModel.ID.isin(['User'])].unique())
                    # choose datasets not included for the model builduing to test for imposters
                    imposter_id = list(random.sample(curr_sub, n_imposter))

                    # extract
                    self.X_Imp = dfModel[dfModel.ID.isin(imposter_id)].drop(columns=['ID']).values
                    self.y_Imp = dfModel.ID[dfModel.ID.isin(imposter_id)]
                    self.X_Model = dfModel[~dfModel.ID.isin(imposter_id)].drop(columns=['ID']).values
                    self.y_Model = dfModel.ID[~dfModel.ID.isin(imposter_id)]

                    # check consistency of X and y shapes
                    # check consistency of X columns for imposters and modeling data
                    # and if there are no imposters in the modeling data

                    if (self.X_Imp.shape[0] == self.y_Imp.shape[0]) and \
                            (self.X_Model.shape[0] == self.y_Model.shape[0]) and \
                            (self.X_Model.shape[1] == self.X_Imp.shape[1]) and \
                            (~self.y_Model.isin(self.y_Imp).any()):
                        # rename IDs in Model and Imposter set to be NoUser
                        self.y_Imp[:] = 'NoUser'
                        self.y_Model[self.y_Model != 'User'] = 'NoUser'

                        # encode labels
                        le = preprocessing.LabelEncoder()
                        le.fit(['NoUser', 'User'])
                        self.y_Model = le.fit_transform(self.y_Model)
                        self.y_Imp = le.fit_transform(self.y_Imp)
                        return True

                    else:
                        Log.AddToLog('createFeatureMatrix: Mismatch in shape of modeling data')
                        return False
                else:
                    Log.AddToLog(
                        'createFeatureMatrix: Feature Columns of Feature Vectors and Feature Matrix do not match')
                    return False

            else:
                Log.AddToLog('createFeatureMatrix: Revision of Feature Vectors and Feature Matrix do not match')
                return False
        except Exception as e:
            Log.AddToLog("exception in createFeatureMatrix :  {}".format(e), 2)
            return False

    # sets up the model
    def setupModel(self):
        Log.AddToLog('setupModel for Enrolment started')
        try:
            # 1. set up pipeline for model creation
            # determine Number of positive and negative samples (User/NoUser)
            nPos = np.bincount(self.y_Model)[1]
            nNeg = np.bincount(self.y_Model)[0]

            # Define ML algorithm to use
            XGB_fixed = XGBClassifier(n_jobs=1,
                                      max_depth=4)
            # set up pipeline
            self.pipe = Pipeline([
                # impute column means for NaN values
                ('impute_mean', preprocessing.Imputer(missing_values='NaN', strategy='mean', axis=0)),
                # add complexity by polynominal features
                # ('poly_feature', PolynomialFeatures(2)),
                # standardscaler -1 to 1
                # ('standardize', StandardScaler()),
                # remove features that have the same values 80%
                # ('remove_zero', VarianceThreshold(threshold=(.8 * (1 - .8)))),
                # Use SMOTE oversampling to overcome imbalance
                ('oversample_smote', SMOTE(ratio='auto', k_neighbors=1)),
                # ('over_under_sample', SMOTEENN(ratio = 'auto', n_neighbors = max_n)),
                ('classify', XGB_fixed)])

            '''
            # 2. If possible set up a stratified cross validation
            #With only 5 features this becomes really hard
            cv_limit = 10
            cv_lower_limit = 2
            max_folds = nPos

            if max_folds <= cv_limit and max_folds >= cv_lower_limit:
                cvsplit = max_folds
            elif max_folds > cv_limit:
                cvsplit = cv_limit
            elif max_folds < cv_lower_limit:
                print('\n Model Building not possible.  Only n = {} (< 2) CV splits possible'.format(max_folds))
                Log.AddToLog('createModel failed. No CV possible')
                return False
            self.cvStrat = StratifiedKFold(n_splits=cvsplit, shuffle=True)
            '''
            self.cvStrat = StratifiedKFold(n_splits=5, shuffle=True)

            return True

        except Exception as e:
            Log.AddToLog("exception in createModel :  {}".format(e), 2)
            return False

    # validates the model
    def validateModel(self, aucThreshold):
        Log.AddToLog('validateModel for Enrolment started')

        try:
            # setup storing variables
            test_set_roc_auc = []
            threshs = []
            tprs = []
            y_pred = []
            y_true = []
            fimp_list = []
            fold = 0
            self.mean_fpr = np.linspace(0, 1, 100)

            df_roc_mean = pd.DataFrame({
                'ID': [],
                'tprs': [],
                'threshold': [],
                'aucs': []})

            df_cm_mean = pd.DataFrame({
                'ID': [],
                'ypred': [],
                'ytrue': []})

            try:
                for train, test in self.cvStrat.split(self.X_Model, self.y_Model):
                    # set trainings and test sets according to outer loop
                    X_train, X_test, y_train, y_test = self.X_Model[train], self.X_Model[test], \
                                                       self.y_Model[train], self.y_Model[test]

                    # calculate class counts in trainings and test sets
                    class_counts = dict(zip(*np.unique(y_test, return_counts=True)))
                    class_imp_counts = dict(zip(*np.unique(self.y_Imp, return_counts=True)))

                    # TO test: reduce group size of non users

                    # select n = class_counts[0] imposter feature vectors to balance imposters
                    # and no users

                    # if less feature vectors in imposter set than in class counts than take all imposters
                    if len(self.y_Imp) < class_counts[0]:
                        id_imp = np.random.choice(np.arange(len(self.y_Imp)), len(self.y_Imp), replace=False)

                    else:
                        id_imp = np.random.choice(np.arange(len(self.y_Imp)), class_counts[0], replace=False)

                    X_Imp_test = self.X_Imp[id_imp]
                    y_Imp_test = self.y_Imp[id_imp]

                    # append imposter data to test set
                    X_test = np.concatenate((X_test, X_Imp_test), axis=0)
                    y_test = np.append(y_test, y_Imp_test)
                    try:
                        self.pipe.fit(X_train, y_train)
                    except Exception as e:
                        print(Fore.RED + 'Exception {} in validateModel. Model Error'.format(e) + Fore.RESET)

                    # calculate  prediction probabilities for each fold in testset
                    probas_ = self.pipe.predict_proba(X_test)
                    # calculate weighted roc_auc score
                    test_set_roc_auc.append(roc_auc_score(y_test, probas_[:, 1], average='weighted'))
                    fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1], pos_label=1)

                    tprs.append(interp(self.mean_fpr, fpr, tpr))
                    tprs[-1][0] = 0.0
                    threshs.append(interp(self.mean_fpr, fpr, thresholds))
                    # otherwise max threshold is max(y_score) + 1 (see sklearn roc_curve)
                    threshs[0] = 1

                    # confusion matrix
                    y_pred = np.append(y_pred, self.pipe.predict(X_test))
                    y_true = np.append(y_true, y_test)
                    fold += 1

                print(Fore.GREEN + 'Mean AUC score: {} \n Std AUC Score {}'.format(np.mean(test_set_roc_auc),
                                                                                   np.std(
                                                                                       test_set_roc_auc)) + Fore.RESET)

                # Plot ROC Curve with EER and Thresholds
                df_roc = pd.DataFrame({
                    'ID': 'User',
                    'tprs': tprs,
                    'threshold': threshs,
                    'aucs': test_set_roc_auc})

                df_roc_mean = df_roc_mean.append(df_roc)

                # calculate EER
                self.calcEER(df_roc_mean)
                # plot roc curve
                # self.plotMeanRoc()

                # test if model performs as wanted
                if np.mean(test_set_roc_auc) >= aucThreshold:
                    # if eer <= .2:
                    self.X_Model_All = np.vstack([self.X_Model, self.X_Imp])
                    self.y_Model_All = np.hstack([self.y_Model, self.y_Imp])
                    self.clf = self.pipe.fit(self.X_Model_All, self.y_Model_All)
                    print(Fore.GREEN + 'Model for ID valid!' + Fore.RESET)
                    return True

                else:
                    print(Fore.RED + 'Alon! Need more Data for Model Building' + Fore.RESET)
                    return False



            except Exception as e:
                print(Fore.RED + 'Exception in validateModel. Crossvalidation loop failed  {} '.format(e) + Fore.RESET)
                return False
        except Exception as e:
            print(Fore.RED + 'Exception in validateModel :  {}'.format(e) + Fore.RESET)
            Log.AddToLog("exception in validateModel :  {}".format(e), 2)
            return False

    '''
    def plotROC(self, df, single):
        Log.AddToLog('plotROC started')
        try:


            return rocplot
        except:
            Log.AddToLog("exception in plotROC :  {}".format(e), 2)
            return 0
    '''

    # Function to plot ROC Curve and calculate EER
    # needs a data frame like this:
    def plotMeanRoc(self, ModelPath):
        Log.AddToLog('plotMeanRoc started')
        try:
            color_vec = ['red', 'blue', 'orange', 'lightgreen', 'dodgerblue']

            id_name = 'User'
            fig, ax1 = plt.subplots()
            ax2 = ax1.twinx()
            ax1.plot(self.mean_fpr, self.mean_tpr,
                     label='ID {}: Mean ROC (AUC = {: 0.2f} +/- {: 0.2f})'.format(id_name, self.mean_auc, self.std_auc),
                     lw=2, alpha=.8, color=color_vec[0])

            tprs_upper = np.minimum(self.mean_tpr + self.std_tpr, 1)
            tprs_lower = np.maximum(self.mean_tpr - self.std_tpr, 0)

            ax1.fill_between(self.mean_fpr, tprs_lower, tprs_upper, alpha=.2,
                             color=color_vec[0])
            ax1.plot([self.eer], [1 - self.eer], marker='o', color=color_vec[1],
                     label='EER={:0.2f}'.format(self.eer))

            ax1.set_xlim([-0.05, 1.05])
            ax1.set_ylim([-0.05, 1.05])
            ax1.set_xlabel('False Positive Rate')
            ax1.set_ylabel('True Positive Rate')

            ax2.set_ylim([-0.05, 1.05])
            ax2.set_ylabel('Decision Threshold')

            plt.title('ROC curve for {}'.format(id_name))

            ax2.plot(self.mean_fpr, self.mean_thresh, 'k--')

            threshs_upper = np.minimum(self.mean_thresh + self.std_thresh, 1)
            threshs_lower = np.maximum(self.mean_thresh - self.std_thresh, 0)
            ax2.fill_between(self.mean_fpr, threshs_lower, threshs_upper, alpha=.2,
                             color='k')
            ax2.plot([self.eer], self.eerThreshold, marker='x', color='k',
                     label='EER_threshold = {:0.2f}'.format(self.eerThreshold))
            # ax1.legend(loc= .5)
            h1, l1 = ax1.get_legend_handles_labels()
            h2, l2 = ax2.get_legend_handles_labels()
            ax1.legend(h1 + h2, l1 + l2, loc=5)

            plt.tight_layout()
            SavingPath = ModelPath + 'ErrorRate' + '.png'
            plt.savefig(SavingPath)
            # return EER, EER_Threshold

        except Exception as e:
            Log.AddToLog("exception in plotMeanRocThresh :  {}".format(e), 2)

    def calcEER(self, df):
        Log.AddToLog('calcEER started')
        try:
            self.mean_auc = np.mean(df.aucs)
            self.std_auc = np.std(df.aucs)
            self.mean_tpr = np.mean(df.tprs.values, axis=0)
            self.mean_tpr[-1] = 1.0
            self.std_tpr = np.std(df.tprs.values, axis=0)
            self.mean_thresh = np.mean(df.threshold.values, axis=0)
            self.mean_thresh[0] = 1.0
            self.std_thresh = np.std(df.threshold.values, axis=0)
            self.mean_fnr = 1 - self.mean_tpr
            self.eer = self.mean_fpr[np.nanargmin(np.absolute((self.mean_fnr - self.mean_fpr)))]
            self.eerThreshold = self.mean_thresh[np.nanargmin(np.absolute((self.mean_fnr - self.mean_fpr)))]
            return True

        except Exception as e:
            Log.AddToLog("exception in calcEER :  {}".format(e), 2)
            return False

