from ModelCreator.CaliModel  import caliModel
import pandas as pd
from Utils import Log
from ModelCreator.PreprocessingMatrix import  preprocessingMatrix
import os
import glob
from colorama import  *
import pickle
import os
from ModelCreator.PIN_model  import pinmodel


class model:
    def __init__(self,path,aquc):
        Log.AddToLog('Model class started to work,path: {}'.format(path))
        self.ModelDict = {'CALI': caliModel, 'PIN': pinmodel}
        self.featurePath=path+'\\'
        self.aquc=aquc
        self.DataFramFrom5V()
        self.Classifier=self.InitModel()

    def DataFramFrom5V(self):
        #Creating one data frame form 5 feature vectoers
            Log.AddToLog('DataFramFrom5V started')
            try:
                featureFiles= os.listdir(self.featurePath)
                #featureFiles = glob.glob(self.featurePath)
                self.FiveV = pd.DataFrame()
                for file in featureFiles:
                    tempDf = pd.read_csv(self.featurePath + '\\' + file)
                    self.FiveV = pd.concat([self.FiveV, tempDf], axis=0)
                #self.FiveV = self.FiveV.assign(Revision=self.revision)
            except Exception as e:
                Log.AddToLog("exception in DataFramFrom5V :  {}".format(e), 2)
                exit()

    def InitModel(self):
            Log.AddToLog('InitModel started')
            try:
                if self.aquc not in self.ModelDict:
                    raise RuntimeError
                else:
                    Log.AddToLog("The choosen Model is : {} trying to connect".format(self.aquc),1)
                    ModelObject=self.ModelDict[self.aquc](self.FiveV)
                    if not ModelObject:
                        Log.AddToLog("ModelObject is False Check the chosen module exiting the program".format(self.FiveV), 1)
                        print(Fore.BLUE + 'ModelObject is False Check the chosen module exiting the program, exiting the program' + Fore.RESET)
                        raise Exception
                    else:
                        return ModelObject
            except Exception as e:
                Log.AddToLog("exception in InitModel :  {}".format(e), 2)
                exit()

    def SaveModel(self,MyModel, path):
        Log.AddToLog('SaveModel started')
        try:
            if not self.MakeDir(path):
                return False
            output = open(self.ModelPath + '\\' + 'model.pkl', 'wb')
            pickle.dump(MyModel, output)
            output.close()
        except Exception as e:
            Log.AddToLog("exception in SaveModel :  {}".format(e), 2)

    def MakeDir(self,Path):
        Log.AddToLog('MakeDir started')
        try:
            self.ModelPath = Path + "\\" + 'Model'
            os.makedirs(self.ModelPath)
            print('MakeDir Created  dir Model : {} '.format(self.ModelPath))
            Log.AddToLog('MakeDirectory Created 2  dir Model : {} '.format(self.ModelPath))
            return True
        except Exception as e:
            Log.AddToLog("exception in MakeDir :  {}".format(e), 2)
            return False

