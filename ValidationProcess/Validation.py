_Author = "Alon"

import wmi
from colorama import *
import numpy as np
import math
import datetime
import matplotlib.pyplot as plt
import os
import time

from Utils import Log
from Utils.CSVdataCreator import cSVcreator
from ValidationProcess import VarData

from Configuration import ParameterManger as PM


class validation():
    def __init__(self,ID):
        self.smi = "iViewREDm-Client.exe"
        self.csdemo = "csdemo.exe"
        self.tobi ="Not_on_Alienware_Tobii.EyeX.Interaction.exe"
        self.gp3 = "Gazepoint.exe"
        self.possible_process = {self.gp3: "gp3", self.tobi:  "tobi", self.csdemo: "smi_cs", self.smi: "smi"}
        #self.targetProcessList = [self.gp3, self.smi, self.csdemo, self.tobi]
        self.smiflag = 0
        self.tobiflag = 0
        self.ID = ID
        self.DeviceName=self.ProcessInTaskManager()
        self.FileName=''



    def ProcessInTaskManager(self):
        """
        the program search for one of the the self.possible_process in the process manger
        :return: string as the name of the device activating, if no process found , mouse activate.
        """
        ProcessList = []
        Log.AddToLog("ProcessInTaskManager started", 1)
        try:
            device = ""
            for target in self.possible_process.keys():
                process=wmi.WMI().Win32_Process(name = target)
                if process:
                    ProcessList.append(process)
            for i in range(len(ProcessList)):
                ProcessList[i] = ProcessList[i][0].name

            if ProcessList:
                # smi is a special case cause it need 2 process to run for activating
                if self.smi in ProcessList and self.csdemo in ProcessList:
                    Log.AddToLog("SMI was detected")
                    print(Fore.GREEN + 'SMI device is turned on!' + Fore.RESET)
                    return ('SMI')
                elif self.smi in ProcessList or self.csdemo in ProcessList:
                    print(
                        Fore.RED + 'Something is wrong , it seems like only one of the process of the SMI is connected, check ProcessManger ' + Fore.RESET)
                    Log.AddToLog(
                        "Error. it seems like the smi is not connected,check ProcessManger smiflag = {}".format(
                            self.smiflag), 2)
                    exit()

                elif len(ProcessList) == 1 :
                    device = self.possible_process[ProcessList[0]]
                    print((Fore.GREEN + '{} device is turned on!' + Fore.RESET).
                          format(device))
                    Log.AddToLog("{} was detected ".format(device))
                    return (device)

            else:
                print(Fore.GREEN + 'MOUSE device is turned on!' + Fore.RESET)
                Log.AddToLog("MOUSE was detected 2", 1)
                return ('MOUSE')

        except Exception as e:
            Log.AddToLog("exception in ProcessInTaskManager started :  {}".format(e), 2)

    def CheckIfQuit(self,NotSMI=None):
        # when to leave the loop!!!
        Log.AddToLog("CheckIfQuit started", 1)
        try:
            # if all SMI process are found
            if self.smiflag==3: #if you find both process for the SMI
                print(Fore.GREEN + 'SMI device is turned on!' + Fore.RESET)
                return True
            #if we are at the end of the program and it found only one of the needed smi process
            if NotSMI and (self.smiflag==1 or self.smiflag==2):
                print(Fore.RED + 'Something is wrong , it seems like only one of the process of the SMI is connected, check ProcessManger ' + Fore.RESET)
                Log.AddToLog("Error. it seems like the smi is not connected,check ProcessManger smiflag = {}".format(self.smiflag), 2)
                return False
            # if you are at the end of the program and didnt found any device
            elif NotSMI and not self.smiflag and not self.tobiflag:
                return True
            #other ways it say you just picked on of them
            return False
        except Exception as e:
            Log.AddToLog("CheckIfQuit :  {}".format(e), 2)
            exit()

    def UsbPortsList(self):
        #read the usb names form the device manger.
        # not implemented at the moment.
        import win32com.client
        wmi = win32com.client.GetObject("winmgmts:")
        for usb in wmi.InstancesOf("Win32_UsbHub"):
            print('DeviceID: ' + str(usb.DeviceID))
            print('SerialNumber : ' + str(usb.SerialNumber))



    def runner(self, Tracker, width, height, ShowEyesGUI,logpath,Screen):
        Log.AddToLog("runner validation started", 1)
        try:
            self.logpath=logpath
            Log.AddToLog("Validation. runner started")
            NumberOfTries=PM.ReturnParameter('NumberOfTries')
            cycle=0
            now = datetime.datetime.now()
            ValTimeUntilQuit=PM.ReturnParameter('ValTimeUntilQuit')
            NowPluse2 = now + datetime.timedelta(minutes=ValTimeUntilQuit)
            while cycle<NumberOfTries or now>NowPluse2:
                # if you created data
                if self.CreateData(Tracker, cycle, ShowEyesGUI):
                    ValidData = self.CheckDeviceData(Tracker, width, height)
                    #if the data is valid
                    if ValidData:
                        self.ErrorAreaPix = self.CalcError(width, height)
                        self.FileName= str(cycle) + "_" + self.ID + "_" + Tracker.name + "_" + self.LogNote + "_" + str(int(time.time()))
                        Tracker.EyeDataDF.to_csv(self.logpath+'\\'+self.FileName +'.txt', header=True, index=None, sep=' ', mode='a')
                        #self.plotGaze(width, height)
                        if self.FeedBack(Tracker, width, height):
                            Screen.ValidationSuccess()
                            return True
                Tracker.Reset()
                cycle+=1
                now = datetime.datetime.now()
                Tracker.EyeDataDF.to_csv(self.logpath + '\\' + self.FileName + '_'+'False', header=True, index=None, sep=' ', mode='a')
                if (NumberOfTries-cycle):
                    Screen.ValidationFailed(NumberOfTries-cycle)
                #self.ValidLog.ChangeLogName(UserNewName=self.ValidLog.logname + '_False')
            print(Fore.RED + 'Failed to collect ValidData exiting the program ' + Fore.RESET)
            Screen.ValidationExit()
            exit()
        except Exception as e:
            Log.AddToLog("Validation.runner :  {}".format(e), 2)
            exit()

    def CreateData(self,Tracker,cycle, ShowEyeGUI):
        try:
            Log.AddToLog("CreateData in Validation started", 2)
            #crating Data for the validation program
            self.LogNote='VALIDATION'
            Tracker.Note=self.LogNote
            #self.ValidLog=cSVcreator(self.DeviceName,self.LogNote,self.id,'a',cycle) #Logname= LogNumber+self.if+LogNote+self.DeviceName
            #Tracker.log=self.ValidLog
            try:
                print('Starting collectong data - Validation ')
                if not ShowEyeGUI.startShowing(Tracker):
                    #User information screen not valid data
                    return False
                # inform user that data is valid
                return True
            except Exception as e:
                Log.AddToLog("Error in ShowEyes :  {}".format(e), 2)
        except Exception as e:
            Log.AddToLog("CreateData.runner :  {}".format(e), 2)
            return False

    def CheckDeviceData(self,Tracker,width,height):
        ValidData = Tracker.IsDataValid(width, height)
        if ValidData:
            self.Xcor = Tracker.EyeDataDF["RawX"].copy()
            self.Ycor = Tracker.EyeDataDF["RawY"].copy()
            print((Fore.GREEN + 'Data from the {} is valid!' + Fore.RESET).format(self.DeviceName))
            return True
        else:
            #Klaas pop up massege!!! error Data not Valid
            return False

    def FeedBack(self,Tracker,width,height):
        Log.AddToLog("FeedBack  started", 2)
        try:
            threshold = PM.ReturnParameter('threshold')
            diamaterX = np.amax(self.Xcor)-np.min(self.Xcor)
            diamaterY = np.amax(self.Ycor) - np.amin(self.Ycor)
            daiamter=np.maximum(diamaterX,diamaterY)
            SampleArea=(math.pi * ((daiamter/2)**2))
            VarX = np.var(self.Xcor)
            VarY = np.var(self.Ycor)
            if Tracker.name=='SMI':
                self.WrtieVarToLog(VarX,VarY)
            Log.AddToLog("sample area is {} Error area is {} [unite - Pixel]".format(SampleArea,self.ErrorAreaPix))
            if (threshold * SampleArea)>self.ErrorAreaPix:
                print(Fore.RED + 'feedback failed ' + Fore.RESET)
                return False
            else:
                print(Fore.GREEN + 'feedback succeeded ' + Fore.RESET)
                return True
        except Exception as e:
            Log.AddToLog("FeedBack :  {}".format(e), 2)
            return False

    def CalcError(self,width,higth):
        self.DiagonalInInch=PM.ReturnParameter('self.DiagonalInInch')   ##not gooddddddddddd
        self.DPI=(math.sqrt(width**2+higth**2))/self.DiagonalInInch
        self.DeviceErrorInDeg=1
        DeviceErrorInRad=math.radians(self.DeviceErrorInDeg)
        InchCm=2.54
        self.UserDistanceFromScreen=60 #cm
        DistanceFromScreenInInch=self.UserDistanceFromScreen/InchCm
        self.radiusInPix=(math.tan(DeviceErrorInRad))*DistanceFromScreenInInch*self.DPI
        ErrorAreaPix=(math.pi * (self.radiusInPix**2))
        return ErrorAreaPix

    def plotGaze(self,width, height):
        try:

            ErrorCircle = plt.Circle((np.mean(self.Xcor), np.mean(self.Ycor)), self.radiusInPix, color='b', fill=False)
            ax = plt.gca()
            ax.cla()
            ax.plot(self.Xcor,self.Ycor, 'ro' )
            ax.add_artist(ErrorCircle)
            plt.axis([0, int(width), 0, int(height)])
            SavingPath=self.logpath +'\\' + self.FileName+'.png'
            plt.savefig(SavingPath)
        except Exception as e:
            Log.AddToLog("WriteVarToLog :  {}".format(e), 2)

    def WrtieVarToLog(self,VarX,VarY):
        Log.AddToLog("WriteVarToLog started", 2)
        try:
            path = os.path.join("C:\MyOculidProject\oculid\EnrollmentProcess\ValidationProcess\VarData.txt")
            VarLog = open(path,'a')
            VarLog.write(str(VarX) + ",")
            VarLog.write(str(VarY) + ",")
            VarLog.write("\n")
            VarLog.close()
        except Exception as e:
            Log.AddToLog("WrtieVarToLog :  {}".format(e), 2)
            return False

if __name__ == "__main__":
    validation("alon")






