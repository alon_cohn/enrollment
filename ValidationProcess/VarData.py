__author__ = "Alon Cohn,06.02.2018"
'''The target is to create a file called a DevLog.txt in the repository,
the Log will contain software Data'''

import logging
import datetime

logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.basicConfig(filename='VarLog.log',level=logging.DEBUG)
'''the data can be divided to 3 different cases information debug or error before each line 
 added to the log default is INFO '''
VINFO = 0
VDEBUG = 1
VERROR = 2
#            Logger.AddToLog("hereou write your msg  {variable}".format(x))

def AddToLog(msg: str, level: int = VINFO) -> bool:
    try:
        msg = str(datetime.datetime.now()) +":  " +  msg
        if level is VINFO:
            logging.info(msg)
        elif level is VDEBUG:
            logging.debug(msg)
        elif level is VERROR:
            logging.error(msg)
        else:
            raise Exception("unknown level for logger {}".format(level))
    except Exception as e:
        AddToLog("exception in AddToLog :  {}".format(e),2)