from toGroup  import Enrollment
from colorama import *
from GUI.HelloScreen import helloScreenn
from ModelCreator.Model import model
from threading import Thread
from GUI.InfoScreens import infoScreens
import time
from Utils import CodeWriter
from Configuration import ParameterManger as PM
from toGroup.AfterParty import TryModel
from Configuration import Globales



# for the chronological sequence please look at the attached tables.
# function creating an enrollment object.
# get user input to create a user name/password
# creating a model
# activate the TRY ME - trying the model

def PreEnrollment():
    try:
        print('starting')
        Globales.initialize()
        Screens = infoScreens()
        Screens.startScreen()
        IDcreator = helloScreenn()
        '''
        #ID, path = IDcreator.CreatOculidID()
        ID, path, pin= IDcreator.CreatOculidID()
        # in case a developer don't want to enter an input
        '''
        Globales.USER_ID = 'Admin'
        path = 'C:\Qsync\Temp_Data'
        Globales.USER_PIN = 1 #PM.ReturnParameter("self.password")
        ID = Globales.USER_ID
        pin = Globales.USER_PIN
        CodeWriter.storePasswordHash(str(ID), str(pin))
        Screens.PresentID(ID)
        Enroll = Enrollment.enrollment()
        ID_Thread = Thread(target= Enroll.init, args=(ID, path, pin))
        ID_Thread.start()
        Screens.ExplainValidation()
        ID_Thread.join()
        Enroll.Valid.runner(Enroll.Device.Tracker, Screens.window.width, Screens.window.height, Enroll.showEyes,Enroll.logpath,Screens)
        print(Fore.GREEN + 'Finish creating Enrollment object' + Fore.RESET)
        succededToCreat = False

        TargetFeatureVectore=PM.ReturnParameter('TargetFeatureVectore')

        while not succededToCreat:
            FeatureVectors=Enroll.EnrollRunner(Screens,TargetFeatureVectore)
            if FeatureVectors:
                print(Fore.GREEN + 'Finish Creating Feature Vectors' + Fore.RESET)
                MyModel=model(Enroll.FeatureVectorPath,Enroll.moudle)
                print(Fore.GREEN +  'Finish creating Model object' + Fore.RESET)
                succededToCreat, Model, eerThreshold, equalErrorRate = MyModel.Classifier.Modelrunner()
                ListOfModelAndErrToSave = [equalErrorRate, eerThreshold, Model]
                time.sleep(2) #just for showing for at least 2 seconds the screen of #pricess is being calculated'
                Screens.running=False
                if not succededToCreat:
                    print(Fore.RED + 'Did not succeed to creating Model ' + Fore.RESET)
                    Screens.ModelFaild()
        print(Fore.GREEN + 'Finish Creating Model' + Fore.RESET)
        MyModel.SaveModel(ListOfModelAndErrToSave,Enroll.SaveFilePath)
        #Enroll.InsertNewFvToMatrix(MyModel.FiveV,IDcreator.FileLocationPath,99)
        #Enroll.Valid.plotGaze(Screens.window.width,Screens.window.height)
        #MyModel.Classifier.plotMeanRoc(MyModel.ModelPath)
        print(Fore.YELLOW + '!!!!!!!!!!!!!!!!Finish Enrollment!!!!!!!!!!!!!!!!!' + Fore.RESET)
        if Screens.ModelSucsses():
            TestProtoType = TryModel(ListOfModelAndErrToSave,Screens,Enroll,IDcreator.FileLocationPath)
            TestProtoType.MakeOneFeatureVector()
    except Exception as e:
        Enroll.Device.Tracker.gp3.stop_tracking()


if __name__ == "__main__":
    PreEnrollment()