
from GUI.showTextinWindow import showTextPage
import os
from Utils import Log
import win32api
import re
from colorama import *
import string
from Configuration import ParameterManger as PM


class helloScreenn(showTextPage):
    def __init__(self):
        super().__init__()
        self.Qsync='Qsync'
        self.FileToFind='FeatureVectorSavingDirectory'
        self.FindFileInAllDrives(self.FileToFind)
        self.AllIdPath = self.FileLocationPath
        self.ListColor = ['BLUE','GREEN','BROWN','OTHER']
        self.current_password = PM.ReturnParameter('self.password')

    # runner
    # present in the end the user password - only time at the process the user see it.
    def CreatOculidID(self):
        auth_way = PM.ReturnParameter('self.moudle')
        next_password = self.current_password + 1
        self.create_user_name()
        self.textInput('The following number is your pin please write it down and save it \n'
                           ' your pin is: {} \n'
                       'please enter space to move on'.format(next_password))
        self.showText()
        PM.update_paramter('self.password',next_password)
        return self.ID,self.FileLocationPath,next_password

    # get user input
    #if one of the input is not correct the process will start from intial and not from end point
    def create_user_name(self):
        IdUniqe = False
        while not IdUniqe:
            motherFirstname = self.inputBox(
                "Please enter your mothers first name \nwithout white space or special characters "
                "and finish with Enter.\n")
            motherLastName = self.inputBox(
                "Please enter your mothers maiden name \nwithout white space or special characters "
                "and finish with Enter.\n")
            if  not motherFirstname.isalpha() or not motherLastName.isalpha():
                self.textInput('Sorry not legal input please enter again, name must be English letters A-Z')
                self.showText(time=4)
                continue
            fatherBirthDay = self.inputBox(
                "Please enter the day of your fathers birthday \nwithout white space or special characters "
                "e.g (15) and finish with Enter.\n")
            if fatherBirthDay.isdigit():
                IntFatherBirth=int(fatherBirthDay)
                if ((IntFatherBirth<1 or IntFatherBirth>31)  ):
                    self.textInput('Sorry not legal input please enter again, nummber have to be between 1 to 31')
                    self.showText(time=4)
                    continue
            else:
                self.textInput('Sorry not legal input please enter again, input must be a number!')
                self.showText(time=4)
                continue
            EyesColor = self.inputBox(
                'Please enter your eyes color from the option below: \nBlue, Brown or Green. \n if you dont know your eyes color type "other" \n '
                "finish with Enter.\n")
            EyesColor=EyesColor.upper()
            if not (EyesColor in self.ListColor) or not EyesColor.isalpha():
                self.textInput('Sorry , we did not understand which eye color you meant, try again')
                self.showText(time=4)
                continue

            # to close the windows

            self.ID = string.capwords(motherFirstname)[0] + string.capwords(motherLastName)[0] + EyesColor[0]  + EyesColor[1] + string.capwords(fatherBirthDay)
            IdUniqe=self.IDuNIQE()
            if not IdUniqe:
                self.textInput('Sorry ID is already exist please try again')
                self.showText(time=2)

    # check if in   Qsync temp data such a user name exist
    #if  exist the process will start over
    def IDuNIQE(self):
        Log.AddToLog('IDuNIQE started')
        try:
            AllId=os.listdir(self.FileLocationPath)
            if self.ID in AllId:
                print('ID Already exist!!!')
                Log.AddToLog('ID Already exist!!!')
                return False
            else:
                return True
        except Exception as e:
            Log.AddToLog("exception in IDuNIQE :  {}".format(e), 2)

    #needs to find file for knowing if user name is unique
    def FindFile(self,root_folder, rex):
        Log.AddToLog('FindFile.GUI started')
        try:
            dirs=os.listdir(root_folder)
            if self.Qsync in dirs:
                for root, dir, file in os.walk(root_folder  + self.Qsync+'\\'+ 'Temp_Data'):
                    for f in file:
                        result = rex.search(f)
                        if result:
                            print('Found the file the location is : ',os.path.join(root, f))
                            self.FileLocationPath = root    # the path to wehre you can find the Feature veactors.
                            Log.AddToLog('File was found FeatureVectorPath is : {}'.format(self.FileLocationPath))
                            return True
            return False
        except Exception as e:
            Log.AddToLog("exception in FindFile.GUI :  {}".format(e), 2)

    def FindFileInAllDrives(self,file_name):
        Log.AddToLog('FindFileInAllDrives.gui started')
        try:
            # create a regular expression for the file
            rex = re.compile(file_name)
            for drive in win32api.GetLogicalDriveStrings().split('\000')[:-1]:
                if self.FindFile(drive, rex):
                    return
                print('Following  drive : {} , file is not exist'.format(drive))
                Log.AddToLog('Following  drive : {} , file is not exist'.format(drive))
            print(Fore.BLUE + 'File was not traced , do not know where to save feature vectors, exiting the program' + Fore.RESET)
            Log.AddToLog('File was not traced , do not know where to save feature vectors, exiting the program')
            exit()
        except Exception as e:
            Log.AddToLog("exception in FindFileInAllDrives.gui :  {}".format(e), 2)
            exit()

