# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"
import pygame
import pygame.gfxdraw
import numpy as np
import os
import sys
from threading import Thread
from random import shuffle
from toGroup import WindowPropertys
import time
import winsound
from Utils import Log
from Configuration import ParameterManger as PM

# comment

class pinEntryGUI():
    def __init__(self):
        Log.AddToLog("pin GUI init started ")
        try:
            self.GUI_folder_path  = 'D:\code_project\enrollment/' + 'GUI/'
            self.window = WindowPropertys.windowPropertys()
            self.window.setScreenSize()
            #self.window.getScreenWithBlackBackground()
            #TODO:Change repeats
            self.repeats = 1
            # variable definitions
            self.running = True
            self.frameRate = 60
            self.clock = pygame.time.Clock()
            self.bg_color = (0, 0, 0)
            self.object_Color = (40, 40, 40)
            self.middle_Color = (10, 10, 10)
            self.font_Color = (200, 200, 200)
            self.dotColor = (200, 200, 200)
            self.circle_size = self.window.valueResizer(104)
            self.targetSpeed = PM.ReturnParameter('self.targetSpeed')
            self.frequency = 500
            self.beepDuration = 150
            self.groupMembersInnerGroup = 5
            self.groupMembersOuterGroup = 2
            self.step = round(self.targetSpeed / self.frameRate)
            self.innerGroupRunDist = self.window.valueResizer(370)
            self.startAngleInner = 180
            self.outerGroupRunDist = self.window.valueResizer(265)
            self.startAngleOuter = 180
            self.middleDiameter = self.window.valueResizer(175)
            self.initialInnerGroupLength = self.window.valueResizer(320)
            self.initialOuterGroupLength = self.window.valueResizer(70)
            self.startDelay = 0.8
            self.initializePositionVariables()
            self.numbers = [i for i in range(self.groupMembersInnerGroup*self.groupMembersOuterGroup)]
        except Exception as e:
            Log.AddToLog("exception in init pin entery gui :  {}".format(e), 2)
    def init_graphics(self):
        self.sortGroups()
        self.loadGraphics()
        self.createPygameSurfaces()
        self.loadSpokenNumbers()        

    def createPygameSurfaces(self):
        # initialize pygame
        pygame.init()
        pygame.font.init()
        pygame.mixer.init()
        self.mouse = False
        pygame.mouse.set_visible(self.mouse)
        self.middlesurface = pygame.Surface((self.middleDiameter * 2, self.middleDiameter * 2))
        self.middlesurface.fill(self.bg_color)
        pygame.draw.circle(self.middlesurface, self.middle_Color, (int(self.middleDiameter), int(self.middleDiameter)),
                           int(self.middleDiameter))
        self.middlesurface = self.middlesurface.convert()

    def loadSpokenNumbers(self):
        self.Numbers = []
        path = self.GUI_folder_path + 'Zahlen'
        for i in range(10):
            self.Numbers.append(pygame.mixer.Sound(os.path.join(path, "{}.wav".format(i))))

    def loadGraphics(self):
        # load Logo and Graphics
        path = self.GUI_folder_path + 'Graphics/'
        self.logo = pygame.image.load(path + 'logo_oculid2.png')
        self.logo = pygame.transform.rotozoom(self.logo, 0.0, 0.25)
        self.logoRect = self.logo.get_rect()
        self.numbersPng = []
        for i in range(10):
            self.numbersPng.append(
                pygame.image.load(path + 'button_grey_white/104x104px/{}.png'.format(i)))
            self.numberRect = self.numbersPng[i].get_rect()
            if int(self.numberRect[2]) != self.circle_size:
                factor = self.circle_size / int(self.numberRect[2])
                self.numbersPng[i] = pygame.transform.rotozoom(self.numbersPng[i], 0.0, factor)
                self.numbersPng[i] = self.numbersPng[i].convert()
        self.numberRect = self.numbersPng[0].get_rect()

    def sortGroups(self):
        self.Groups = []
        for i in range(self.groupMembersInnerGroup * self.groupMembersOuterGroup):
            if i % 2 == 0:
                self.Groups.append([self.numbers[i], self.numbers[i + 1]])

    def initializePositionVariables(self):
        Log.AddToLog("initialize position variables ")
        try:
            self.innerGroupLength = self.initialInnerGroupLength
            self.outerGroupLength = self.initialOuterGroupLength
        except Exception as e:
            Log.AddToLog("exception in initialize variables :  {}".format(e), 2)

    def generateTargetNumbers(self, length):
        Log.AddToLog("generate target number set ")
        self.TargetNumberList = [i for i in range(length)]
        shuffle(self.TargetNumberList)
        return self.TargetNumberList

    def initialDraw(self):
        #self.checkForKeyPress()
        self.window.screen.blit(self.window.background, (0, 0))
        self.window.screen.blit(self.logo,
                                (self.window.width - self.logoRect[2], self.window.height - self.logoRect[3]))
        self.window.screen.blit(self.middlesurface, (
        (self.window.width / 2) - self.middleDiameter, (self.window.height / 2) - self.middleDiameter))

        self.midPointsInnerX, self.midPointsInnerY, self.anglesInner = self.getMidpoints((self.window.width / 2),
                                                                                         self.window.height / 2,
                                                                                         self.groupMembersInnerGroup,
                                                                                         self.innerGroupLength,
                                                                                         self.startAngleInner)
        self.drawGroups()
        pygame.display.update()

    def checkForKeyPress(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:  # event is quit
                print("quit")
                self.running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:  # event is escape key
                    print("esc")
                    self.running = False

                elif event.key == pygame.K_m:
                    if self.mouse == True:
                        self.mouse = False
                    else:
                        self.mouse = True
                    pygame.mouse.set_visible(self.mouse)

                else:
                    return event.key  # key found return with it
        # no quit or key events in queue so return None
        pygame.display.update()
        return

    def getMidpoints(self, x, y, corners, group_length, start_angle):
        angles_deg = range(start_angle, start_angle + 360, int(360 / corners))
        angles = []
        midpoints_x = []
        midpoints_y = []
        for i in range(len(angles_deg)):
            angles.append(angles_deg[i] / 360 * 2 * np.pi)
            midpoints_x.append(int(x + (group_length * np.cos(angles[i]))))
            midpoints_y.append(int(y + (group_length * np.sin(angles[i]))))
        return midpoints_x, midpoints_y, angles

    def drawGroups(self):
        for i in range(len(self.midPointsInnerX)):
            self.midPointsOuterX, self.midPointsOuterY, self.anglesOuter = self.getMidpoints(self.midPointsInnerX[i],
                                                                                             self.midPointsInnerY[i],
                                                                                             self.groupMembersOuterGroup,
                                                                                             self.outerGroupLength,
                                                                                             self.startAngleOuter)

            for j in range(len(self.midPointsOuterX)):
                self.window.screen.blit(self.numbersPng[self.Groups[i][j]], (
                int(self.midPointsOuterX[j]) - self.circle_size / 2,
                int(self.midPointsOuterY[j]) - self.circle_size / 2))

    def staticMainScreen(self):
        Log.AddToLog("draw main screen ")
        try:
            self.initializePositionVariables()
            self.initialDraw()
        except Exception as e:
            Log.AddToLog("exception in staticMainScreen :  {}".format(e), 2)

    def dynamicScreenMovement1(self):
        Log.AddToLog("start movement 1  ")
        try:

            deltaTime = 0
            start = time.perf_counter()
            #delay before movement starts
            while deltaTime < self.startDelay:
                deltaTime = time.perf_counter() - start
            runstart = time.perf_counter()
            runDist = 0

            while runDist < self.innerGroupRunDist:
                self.clock.tick(self.frameRate)
                self.initialDraw()
                self.innerGroupLength += self.step
                runDist += self.step
            print('Movement 1 duration = {}'.format(time.perf_counter()-runstart))
            return self.innerGroupLength
        except Exception as e:
            Log.AddToLog("exception in Movement 1 :  {}".format(e), 2)

    def staticTransitionScreen(self):
        Log.AddToLog("draw transition screen screen ")
        try:
            self.initialDraw()
            #for k in range(int(self.frameRate / 10)):
            #    self.clock.tick(self.frameRate)
        except Exception as e:
            Log.AddToLog("exception in TransitionScreen :  {}".format(e), 2)

    def dynamicScreenMovement2(self):
        Log.AddToLog("start movement 2  ")
        try:
            runstart = time.perf_counter()
            runDist = 0
            while runDist < self.outerGroupRunDist:
                self.clock.tick(self.frameRate)
                self.initialDraw()
                self.outerGroupLength += self.step
                runDist += self.step
            print('Movement 2 duration = {}'.format(time.perf_counter()-runstart))
        except Exception as e:
            Log.AddToLog("exception in Movement 2 :  {}".format(e), 2)

    def dynamicReturnScreen(self):
        Log.AddToLog("start return screen  ")
        try:
            while self.innerGroupLength > 0:
                self.initialDraw()
                self.innerGroupLength -= 60
                if self.outerGroupLength > self.initialInnerGroupLength:
                    self.outerGroupLength -= 60
                else:
                    self.outerGroupLength = self.initialOuterGroupLength
                self.clock.tick(self.frameRate)
            self.initializePositionVariables()
            self.initialDraw()
        except Exception as e:
            Log.AddToLog("exception in ReturnScreen :  {}".format(e), 2)

    def sayNumber(self,number):
        self.Numbers[number].play()

    def makeABeep(self):
        winsound.Beep(self.frequency, self.beepDuration)

if __name__ == '__main__':
    #Exapmle for four digits
    pin = pinEntryGUI()
    targetNumbers = pin.generateTargetNumbers(4)
    for numbers in targetNumbers:
        pin.sayNumber(numbers)
        pin.staticMainScreen()
        pin.dynamicScreenMovement1()
        pin.staticTransitionScreen()
        pin.dynamicScreenMovement2()
        pin.makeABeep()
        pin.dynamicReturnScreen()
    print(targetNumbers)


