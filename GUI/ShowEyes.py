# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"
import json
import pygame
from threading import Thread, Lock
from toGroup import WindowPropertys
import numpy as np
import sys
import time
from Utils import Log
from Configuration import ParameterManger as PM

class showEyes():
    def __init__(self):
        Log.AddToLog("initialize showEyes")
        pygame.init()
        self.clock = pygame.time.Clock()
        self.bg_color = (0, 0, 0)
        self.frameRate = 60
        self.object_Color = (102, 204, 0)
        self.surfaceColor = (33,33,33)
        self.eyeHintColor = (20,20,20)
        pygame.font.init()
        self.window = WindowPropertys.windowPropertys()
        self.window.setScreenSize() 
        print("Show Eyes Screen = ({},{})".format(self.window.width,self.window.height))
        self.myfont = pygame.font.SysFont('Calibri', self.window.valueResizer(50))
        self.surfaceSizeX = self.window.valueResizer(1040)
        self.surfaceSizeY = self.window.valueResizer(475)
        self.eyeSizeX = self.window.valueResizer(260)
        self.eyeSizeY = self.window.valueResizer(155)
        self.eyeHintXSize = self.window.valueResizer(260)
        self.eyeHintYSize = self.window.valueResizer(155)
        if self.window.width == 1280:
            self.eyeDistance = 7
        else:
            self.eyeDistance = 6.5


        
    def drawing(self):
        try:
            self.window.drawBackground((0,0,0))
            self.middleSurface = pygame.Surface((self.surfaceSizeX,self.surfaceSizeY))
            self.middleSurface.fill(self.surfaceColor)
            self.middleSurface.set_colorkey(self.surfaceColor)
            self.middelRect = self.middleSurface.get_rect(center = (self.window.width/2,self.window.height/2))

            self.eyeOneSurface = pygame.Surface((self.eyeSizeX, self.eyeSizeY))
            self.eyeOneSurface.fill(self.surfaceColor)
            self.eyeOneSurface.set_colorkey(self.surfaceColor)
            self.eyeTwoSurface = pygame.Surface((self.eyeSizeX, self.eyeSizeY))
            self.eyeTwoSurface.fill(self.surfaceColor)
            self.eyeTwoSurface.set_colorkey(self.surfaceColor)

            self.eyeOneHintSurface= pygame.Surface((self.window.valueResizer(260), self.window.valueResizer(155)))
            self.eyeOneHintSurface.fill(self.surfaceColor)
            self.eyeOneHintSurface.set_colorkey(self.surfaceColor)
            self.eyeTwoHintSurface = pygame.Surface((self.window.valueResizer(260), self.window.valueResizer(155)))
            self.eyeTwoHintSurface.fill(self.surfaceColor)
            self.eyeTwoHintSurface.set_colorkey(self.surfaceColor)

            self.crossSurface = pygame.Surface((40,40))
            self.crossSurface.fill(self.surfaceColor)
            self.crossSurface.set_colorkey(self.surfaceColor)
            self.crossRect = self.crossSurface.get_rect(center = (self.window.width/2,self.window.height/2))

            pygame.draw.ellipse(self.eyeOneSurface, self.object_Color,
                               (0.0,0.0,float(self.eyeSizeX),float(self.eyeSizeY)),0)
            pygame.draw.ellipse(self.eyeTwoSurface, self.object_Color,
                               (0.0,0.0,float(self.eyeSizeX),float(self.eyeSizeY)),0)
            pygame.draw.ellipse(self.middleSurface, (40,40,40),
                               (0.0,0.0,float(self.surfaceSizeX),float(self.surfaceSizeY)),0)

            pygame.draw.ellipse(self.eyeOneHintSurface, self.eyeHintColor,
                                (0.0, 0.0, float(self.eyeHintXSize), float(self.eyeHintYSize)), 0)
            pygame.draw.ellipse(self.eyeTwoHintSurface, self.eyeHintColor,
                                (0.0, 0.0, float(self.eyeHintXSize), float(self.eyeHintYSize)), 0)

            self.eyeOneSurface.set_alpha(85)
            self.eyeTwoSurface.set_alpha(85)
            self.eyeOneRect = self.eyeOneHintSurface.get_rect(center = (self.window.width/2-self.window.valueResizer(200),self.window.height/2))
            self.eyeTwoRect = self.eyeTwoHintSurface.get_rect(center = (self.window.width/2+self.window.valueResizer(200),self.window.height/2))

            self.eyeOneHintSurface.set_alpha(75)
            self.eyeTwoHintSurface.set_alpha(75)
            self.eyeOneHintRect = self.eyeOneHintSurface.get_rect(center = (self.window.width/2-self.window.valueResizer(200),self.window.height/2))
            self.eyeTwoHintRect = self.eyeTwoHintSurface.get_rect(center = (self.window.width/2+self.window.valueResizer(200),self.window.height/2))

            pygame.draw.line(self.crossSurface, (0, 0, 0), (0, 20), (40, 20),2)
            pygame.draw.line(self.crossSurface, (0, 0, 0), (20, 0), (20, 40),2)

            self.middleSurface = self.middleSurface.convert()
            self.eyeOneSurface = self.eyeOneSurface.convert()
            self.eyeTwoSurface = self.eyeTwoSurface.convert()
            self.crossSurface = self.crossSurface.convert()
            pygame.display.update()
        except Exception as e:
            Log.AddToLog("Validation.runner :  {}".format(e), 2)

    def checkForKeyPress(self,Tracker):
        try:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:      #event is quit
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:   #event is escape key
                        Tracker.EndWriting = True
                        Tracker.disconnectTracker()
                        pygame.quit()
                        sys.exit()
                    else:
                        return event.key   #key found return with it
        # no quit or key events in queue so return None
            return
        except Exception as e:
            Log.AddToLog("checkForKeyPress :  {}".format(e), 2)
    

    def startShowing(self,Tracker):
        Log.AddToLog("Start showEyes GUI")
        try:
            pygame.mouse.set_visible(False)
            if Tracker.name == 'MOUSE':
                pygame.mouse.set_visible(True)
            self.window.getScreenWithBlackBackground()
            self.drawing()
            # lastPlx = 0
            # lastPly = 0
            # lastPrx = 0
            # lastPry = 0
            ContinuesTime = 4 #s econds
            thread_lock = Lock()
            CollectingData = Thread(target=Tracker.StartTracking, args=(thread_lock,))
            CollectingData.start()

            timeout = PM.ReturnParameter("gp3")["timeout"]
            t0 = time.time()
            while not Tracker.eyeData:
                time.sleep(0.001)
                now = time.time()
                if ( now - t0 )> timeout:
                    print("timeout not data enter! ")
                    Log.AddToLog("timeout not data enter!")
                    return False

            start = time.time()
            deltaTime = 0
            j=0
            while deltaTime < ContinuesTime:
                t0 = time.time()
                try:

                    j+=1
                    print("start show eyes cycle numner : {}".format(j))
                    Log.AddToLog("start show eyes cycle numner : {}".format(j))
                    pygame.event.get()
                    self.clock.tick(self.frameRate)
                    self.checkForKeyPress(Tracker)

                    #print("after init: {}".format(time.time() - t0))
                    thread_lock.acquire()
                    Tracker.lock.acquire()

                    if Tracker.eyeData['Right_PupilCenterZ'] :
                        self.eyeSizeX = abs((self.window.valueResizer(260) / Tracker.eyeData['Right_PupilCenterZ']) * 600)
                        self.eyeSizeY = abs((self.window.valueResizer(155) / Tracker.eyeData['Right_PupilCenterZ']) * 600)
                        self.pupilDistance = np.sqrt((Tracker.eyeData['Left_PupilCenterX']-Tracker.eyeData['Right_PupilCenterX'])**2 +
                                                     (Tracker.eyeData['Left_PupilCenterY']-Tracker.eyeData['Right_PupilCenterY'])**2)
                        self.drawing()

                    #print("after Right_PupilCenterZ: {}".format(time.time() - t0))

                    if Tracker.eyeData['Left_PupilCenterX'] != 0 or Tracker.eyeData['Right_PupilCenterX'] != 0:
                        self.window.screen.blit(self.window.background,(0,0))
                        self.window.screen.blit(self.middleSurface, self.middelRect)
                        self.window.screen.blit(self.eyeOneHintSurface,self.eyeOneHintRect)
                        self.window.screen.blit(self.eyeTwoHintSurface,self.eyeTwoHintRect)

                    #print("after Left_PupilCenterX: {}".format(time.time() - t0))

                    self.window.screen.blit(self.crossSurface, self.crossRect)
                    if Tracker.eyeData['Left_PupilCenterX'] != 0:
                        lastPlx = Tracker.eyeData['Left_PupilCenterX']
                        lastPly = Tracker.eyeData['Left_PupilCenterY']
                        self.eyeOneRect = self.eyeOneSurface.get_rect(center = ((self.window.width/2)+Tracker.eyeData['Left_PupilCenterX']*self.window.valueResizer(self.eyeDistance),
                                                                                (self.window.height/2)-Tracker.eyeData['Left_PupilCenterY']*self.window.valueResizer(3)))
                        self.window.screen.blit(self.eyeOneSurface, self.eyeOneRect)
                        self.eyeOneoHintRect  = self.eyeOneHintSurface.get_rect(center = ((self.window.width/2)+self.pupilDistance/2*self.window.valueResizer(self.eyeDistance),self.window.height/2))

                    #print("after Left_PupilCenterX: {}".format(time.time() - t0))

                    #else:
                     #   self.eyeOneRect = self.eyeOneSurface.get_rect(center=(
                     #       (self.window.width / 2) + lastPlx * self.window.valueResizer(self.eyeDistance),
                     #       (self.window.height / 2) - lastPly * self.window.valueResizer(3)))

                    if Tracker.eyeData['Right_PupilCenterX'] != 0:
                        lastPrx = Tracker.eyeData['Right_PupilCenterX']
                        lastPry = Tracker.eyeData['Right_PupilCenterY']
                        self.eyeTwoRect = self.eyeTwoSurface.get_rect(center = ((self.window.width/2)+Tracker.eyeData['Right_PupilCenterX']*self.window.valueResizer(self.eyeDistance),(self.window.height/2)-Tracker.eyeData['Right_PupilCenterY']*self.window.valueResizer(3)))
                        self.window.screen.blit(self.eyeTwoSurface, self.eyeTwoRect)
                        self.eyeTwoHintRect = self.eyeOneHintSurface.get_rect(center = ((self.window.width/2)-self.pupilDistance/2*self.window.valueResizer(self.eyeDistance),self.window.height/2))

                    Tracker.lock.release()
                    thread_lock.release()
                    #print("after Right_PupilCenterX: {}".format(time.time() - t0))

                    #else:
                    #    self.eyeTwoRect = self.eyeTwoSurface.get_rect(center=(
                    #    (self.window.width / 2) + lastPrx * self.window.valueResizer(self.eyeDistance),
                    #    (self.window.height / 2) - lastPry * self.window.valueResizer(3)))
                    pygame.display.update()

                    deltaTime = time.time() - start

                    print("finish cycle")
                    Log.AddToLog("finish cycle")
                except Exception as e:
                    Log.AddToLog("exception in showEyes GUI while:  {}".format(e), 2)
                    Tracker.lock.release()
                    thread_lock.release()
            pygame.display.update()
            Tracker.EndWriting = True
            while Tracker.trackerRunning:
                time.sleep(0.001)
            #pygame.display.quit()
            return True
        except Exception as e:
            Log.AddToLog("exception in showEyes GUI:  {}".format(e), 2)
            Tracker.EndWriting = True
            return False





# for be able to run it as stand alone.
# class Tracker():
#     def __init__(self):
#         import json
#         with open ("Tracker_obj.json") as data_file:
#             data = json.load(data_file)
#             self.name = data["name"]
#             #self.flag = True
#             self.data = data["list_data"]
#             self.eyeData = self.data[0]
#
#     def StartTracking(self):
#         for eye_data in self.data:
#             self.eyeData = eye_data
#             time.sleep(1/60)
#     def stopTracking(self):
#         pass

"""     
if __name__ == '__main__':
    import csv
    st = showEyes()
    tracker = Tracker()
    st.startShowing(tracker)
    # st.ED.disconnectTracker()
    # st.pygame.display.quit()
    """