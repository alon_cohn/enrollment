# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"

import pygame
import pygame.gfxdraw
from EyeData import EyeData
from threading import Thread
import datetime
import numpy as np
import time
import math
from EnrollmentProcess import WindowPropertys


class Calibration():

    def __init__(self, newlogfile="None", logStatus="a"):
        self.ED = EyeData.EyeData("Calibration", newlogfile, logStatus)
        t1 = Thread(target=self.ED.startTracking)
        t1.start()
        pygame.init()
        self.frameRate = 60
        pygame.mouse.set_visible(False)
        self.eye_data_samples = 240
        self.corrected_loop_counter = self.eye_data_samples * 0.01 * self.frameRate
        print("loop counter = {}".format(self.corrected_loop_counter))
        self.window = WindowPropertys.windowPropertys()
        self.window.setScreenSize()
        self.window.getScreenWithBlackBackground()

        print("Calibration Screen = ({},{})".format(self.window.width, self.window.height))

        self.clock = pygame.time.Clock()
        self.bg_RGB = 255
        self.bg_color = (self.bg_RGB, self.bg_RGB, self.bg_RGB)
        self.object_color = (0, 0, 0)
        self.circle_size = self.window.valueResizer(416)
        self.initial_circle_size = self.circle_size

    def backgroundDrawer(self):
        self.window.background.fill(self.bg_color)
        self.window.screen.blit(self.window.background, (0, 0))

    # pygame.display.update()

    def circleDrawer(self):
        if self.circle_size - 1 > 0:
            self.circlesurface = pygame.Surface((self.circle_size, self.circle_size))
            self.circlesurface.fill(self.bg_color)
            pygame.draw.circle(self.circlesurface, self.object_color,
                               (int(self.circle_size / 2), int(self.circle_size / 2)), int(self.circle_size / 2))
            pygame.draw.circle(self.circlesurface, self.bg_color,
                               (int(self.circle_size / 2), int(self.circle_size / 2)), int(self.circle_size / 2 - 1))
            pygame.draw.circle(self.circlesurface, self.object_color,
                               (int(self.circle_size / 2), int(self.circle_size / 2)), int(2))
            self.circlesurface.convert()
            self.window.screen.blit(self.circlesurface, (
            self.window.width / 2 - self.circle_size / 2, self.window.height / 2 - self.circle_size / 2))
            pygame.display.update()

    def startCalibration(self):

        self.ED.moveID += 1
        self.eyeX = []
        self.eyeY = []
        self.backgroundDrawer()
        pygame.display.update()
        start = time.time()
        deltaTime = 0
        self.ED.note = "White Screen"
        while deltaTime < 4:
            deltaTime = time.time() - start
        self.circleDrawer()
        # self.ED.connectTracker()
        self.ED.note = "Pre Baseline"
        print("Pre Baseline")
        self.ED.sampleNumber = 0
        start = time.time()
        deltaTime = 0
        while deltaTime < 2:
        #while self.ED.sampleNumber < 120:
            pygame.event.get()
            self.clock.tick(self.frameRate)
            deltaTime = time.time()-start
        self.ED.sampleNumber = 0
        self.ED.note = "Brightness Change"
        print("Brightness Change")
        #Changed length of calibration to 2 * self.framerate because the movement in the
        #experimantal prototyp was much slower
        #TODO: test the influence on the indentification!!!
        count = 0
        start = time.time()
        deltaTime = 0
        while deltaTime < 2.5:
        #for i in range(self.frameRate*2):
            self.object_color = (0, 0, 0)
            pygame.event.get()
            if (self.ED.x != 0) and (self.ED.sampleNumber > self.eye_data_samples/4) and (self.ED.sampleNumber < self.eye_data_samples*3/4):
                self.eyeX.append(self.ED.eyeData['RawX'])
                self.eyeY.append(self.ED.eyeData['RawY'])
            if self.bg_RGB >= math.ceil(254/self.corrected_loop_counter):
                self.bg_RGB -= math.ceil(254/self.corrected_loop_counter)
                #print("RGB = ({},{},{})".format(self.bg_RGB,self.bg_RGB,self.bg_RGB))
            else:
                self.bg_RGB = 0
            self.bg_color = (self.bg_RGB, self.bg_RGB, self.bg_RGB)
            if self.circle_size >= math.ceil(self.initial_circle_size/self.corrected_loop_counter) :
                self.circle_size -= math.ceil(self.initial_circle_size/self.corrected_loop_counter)
            if self.ED.sampleNumber > self.corrected_loop_counter/2:
                self.object_color = (255, 255, 255)
            self.backgroundDrawer()
            self.circleDrawer()
            #print("Circle size = {}".format(self.circle_size))
            self.clock.tick(self.frameRate)
            count += 1
            deltaTime = time.time()-start
        # pygame.display.update()
        print("counter = {}".format(count))
        print("time = {} sec".format(time.time()-start))
        self.ED.sampleNumber = 0
        self.ED.note = "Post Baseline"
        #print("Post Baseline")
        start = time.time()
        deltaTime = 0
        while deltaTime < 0.5:
            self.clock.tick(self.frameRate)
            deltaTime = time.time()-start
        self.ED.stopTracking()
        self.ED.log.closeLogFile()
        try:
            return (self.window.width / 2) - np.mean(self.eyeX), (self.window.height / 2) - np.mean(self.eyeY)
        except TypeError:
            return 0, 0
    # pygame.display.quit()


if __name__ == '__main__':
    calib = Calibration()
    calib.startCalibration()
    calib.ED.log.closeLogFile()


'''
Calculated timings from experiment:
Pre Baseline
Mean: 0.9009445930636206
Std: 0.10237893112956452
Brightness Change
Mean: 1.693020478442863
Std: 0.16782356509296184
Post Baseline
Mean: 0.22589497198568326
Std: 0.022111572516670275
Movement 1
Mean: 0.6239019481201844
Std: 0.04419269833792003
Transition
Mean: 0.032059919809004994
Std: 0.001777550599151573
Movement 2
Mean: 0.3902064159772122
Std: 0.023297111371087136
FinalCalculation
Mean: 0.30983626688294946
Std: 0.018268068167946253
ReturnPhase
Mean: 0.046543775346391696
Std: 0.005177133860661864
Wait for Return
Mean: 1.165113487987157
Std: 0.7577185347855012
movement not started
Mean: 0.2811995776619498
Std: 0.16528938164135476
'''