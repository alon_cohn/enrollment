# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"

import pygame
from toGroup import WindowPropertys
import os



class startWindow():
    def __init__(self):
        pygame.init()
        self.clock = pygame.time.Clock()
        pygame.font.init()
        self.bg_color = (0, 0, 0)
        self.running = True
        self.startProgram = False
        self.faceRecognition = False
        pygame.mouse.set_visible(True)
        self.frameRate = 60
        self.window = WindowPropertys.windowPropertys()
        self.window.setScreenSize()
        self.myfont = pygame.font.SysFont('Calibri', self.window.valueResizer(50))
        self.myfont_small = pygame.font.SysFont('Calibri', self.window.valueResizer(38))
        print("Show MainGUI Screen = ({},{})".format(self.window.width, self.window.height))
        self.loadGraphics()

    def loadGraphics(self):
        self.logo = self.window.graphicsResizer(pygame.image.load(os.path.join('Graphics', 'logo_oculid2.png')))
        self.logoRect = self.logo.get_rect()
        self.getBackground()

    def getBackground(self):
        self.window.getScreenWithBlackBackground()

    def drawElements(self):
        self.window.screen.blit(self.window.background, (0, 0))
        self.logoPos = self.window.screen.blit(self.logo, (self.window.width / 2 - int(self.logoRect[2]) / 2, self.logoRect[3]/4))

        #start Button
        self.startText = self.myfont.render('Let´s have a look!', 1, (200, 200, 200))
        self.startTextWidth, self.exitTextHeight = self.myfont.size('Let´s have a look!')
        self.startPos = self.window.screen.blit(self.startText, (
            self.window.width/2 - self.startTextWidth/2, self.window.height/4*3 - self.exitTextHeight/2))

        #Exit Button
        self.exitText = self.myfont_small.render('EXIT', 1, (200, 200, 200))
        self.exitTextWidth, self.exitTextHeight = self.myfont_small.size('EXIT')
        self.exitPos = self.window.screen.blit(self.exitText, (
            self.window.width - self.exitTextWidth*2, self.window.height - self.exitTextHeight*2))

        pygame.display.update()

    def checkForKeyPress(self):
        for event in pygame.event.get():
            self.checkForMouseClick(event)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    self.exitProgram()

    def checkForMouseClick(self,event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            self.mousePos = pygame.mouse.get_pos()
            if self.startPos.collidepoint(self.mousePos):
                self.startProgram = True
            if self.exitPos.collidepoint(self.mousePos):
                self.exitProgram()

    def runMainGUI(self):
        while self.running:
            self.clock.tick(self.frameRate)
            self.drawElements()
            self.checkForKeyPress()
            if self.startProgram:
                return True
        return False

    def exitProgram(self):
        print("quit")
        self.running = False


if __name__ == '__main__':
    main = startWindow()
    main.runMainGUI()