# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"

import pygame
# import EyeData
import ctypes
import os
from toGroup import WindowPropertys
from Utils import Log
from GUI import Buttons
import sys
from Configuration import Globales


class showTextPage():
    def __init__(self):
        pygame.init()
        pygame.font.init()

        self.clock = pygame.time.Clock()
        pygame.mouse.set_visible(False)
        self.bgColor = (0, 0, 0)
        self.fontColor = (200, 200, 200)
        self.frameRate = 60
        self.drawLogo = False
        self.window = WindowPropertys.windowPropertys()
        self.window.setScreenSize()
        self.myfont = pygame.font.SysFont('Arial', self.window.valueResizer(50))
        self.myfont_small = pygame.font.SysFont('Arial', self.window.valueResizer(38))
        self.ButtonDict = {}
        self.ButtonPositionDict = {}
        self.activeButtons = False
        print("Text Screen = ({},{})".format(self.window.width, self.window.height))
        self.window.getScreenWithBlackBackground()

    def textInput(self, text):
        Log.AddToLog("Show Text to User: {}".format(text.replace("\n", "")))
        self.text = text
        if self.drawLogo:
            self.text = "\n " * 10 + self.text
        self.msg = self.text.split("\n")
        self.textRender = {}
        self.textWidth = {}
        self.textHeight = {}
        for i in range(len(self.msg)):
            self.textRender[i] = self.myfont.render(self.msg[i], 1, self.fontColor, self.bgColor)
            self.textWidth[i], self.textHeight[i] = self.myfont.size(self.msg[i])
        self.drawing()

    def drawing(self):
        self.window.screen.blit(self.window.background, (0, 0))
        if self.drawLogo:
            self.logo = pygame.image.load(os.path.join('GUI\Graphics', 'logo_oculid2.png'))
            self.logoRect = self.logo.get_rect()
            self.window.screen.blit(self.logo, (self.window.width / 2 - int(self.logoRect[2]) / 2, 300))
        if self.activeButtons:
            for button in self.ButtonDict:
                self.ButtonDict[button].placeButton(self.ButtonPositionDict[button])
        for i in range(len(self.textRender)):
            self.window.screen.blit(self.textRender[i], (self.window.width / 2 - self.textWidth[i] / 2,
                                                         self.window.height / 2 - self.textHeight[i] / 2 + (
                                                                     i * self.textHeight[i]) - (
                                                                     len(self.textRender) / 2 * self.textHeight[i])))

        pygame.display.update()

    def showText(self, final=False, time="keyPress"):
        self.running = True
        counter = 0
        while self.running:
            if self.activeButtons:
                self.drawing()
            MouseClick = self.checkForKeyPress()
            self.clock.tick(self.frameRate)
            counter += 1
            if time != "keyPress":
                if counter == time * self.frameRate:
                    break
        if final:
            # self.ED.disconnectTracker()
            # self.ED.log.closeLogFile()
            pygame.display.quit()
            pass
        return MouseClick

    def inputBox(self, prompt, final=False):
        self.textInput(prompt)
        self.showText(final, time=1)
        #self.textInput("****")
        lastChar = None
        user = ""
        capital = False
        while lastChar != "return":
            lastChar = self.checkForKeyPress()
            if lastChar != None and lastChar != "return":
                if lastChar == "right shift" or lastChar == "left shift":
                    capital = True
                else:
                    if capital == True:
                        user += lastChar.upper()
                        capital = False
                    else:
                        if lastChar == "backspace":
                            user = user[:-1]
                        else:
                            if lastChar != 0:
                                user += lastChar
                    self.textInput(user)

        return user

    def checkForKeyPress(self):
        for event in pygame.event.get():
            if self.activeButtons:
                return self.checkForMouseClick(event)
            if event.type == pygame.QUIT:  # event is quit
                print("quit")
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    self.running = False
                elif event.key == pygame.K_ESCAPE:
                    print("quit")
                    self.running = False
                    self.ExitKillMe()
                else:
                    char = (pygame.key.name(event.key))
                    return char
        return

    def checkForMouseClick(self, event):
        for buttons in self.ButtonDict:
            if self.ButtonDict[buttons].checkIfButtonWasPressed(event):
                self.ClickedButton = buttons
                self.running = False
                return buttons

    def ExitKillMe(self):
        Globales.EXIT = True
        pygame.display.quit()
        pygame.quit()
        sys.exit()

    def FinishKillMe(self):
        Globales.EXIT = True
        pygame.display.quit()
        pygame.quit()
        sys.exit()

    def LetsStartPilot(self):
        self.resetButtons()
        return True

    def showLogoSwitch(self):
        if self.drawLogo:
            self.drawLogo = False
        else:
            self.drawLogo = True

    def makeButtons(self,text):
        pygame.mouse.set_visible(True)
        self.activeButtons = True
        self.ButtonDict[text] = Buttons.buttons(self.window,text)
        # EXIT Button

    def buttonPosition(self,text,pos):
        if text in self.ButtonDict:
            self.ButtonPositionDict[text] = pos
        else:
            print('Button {} does not exist.'.format(text))

    def resetButtons(self):
        self.ButtonDict = {}
        self.ButtonPositionDict = {}

if __name__ == '__main__':
    shtxt = showTextPage()
    shtxt.showLogoSwitch()
    shtxt.textInput("Welcome to oculid!\n")

    shtxt.makeButtons('FINISH')
    shtxt.buttonPosition('FINISH',( shtxt.window.width / 2 - shtxt.ButtonDict['FINISH'].buttonTextWidth * 2,
                                    shtxt.window.height - shtxt.ButtonDict['FINISH'].buttonTextHeight * 2))
    shtxt.makeButtons('NEXT')
    shtxt.buttonPosition('NEXT',(shtxt.window.width / 2 + shtxt.ButtonDict['NEXT'].buttonTextWidth * 2,
                                    shtxt.window.height - shtxt.ButtonDict['NEXT'].buttonTextHeight * 2))

    print(shtxt.showText())