# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"
from GUI.showTextinWindow import showTextPage
from Utils import Log

import sys
import time
import pygame


class infoScreens(showTextPage):
    def __init__(self):
        super().__init__()
        self.functionDict = {'EXIT': self.ExitKillMe, 'FINISH': self.FinishKillMe, 'NEXT': self.Next,
                             'TryMe': self.LetsStartPilot}

    def MakeEXIT(self):

        Button = 'EXIT'
        self.makeButtons(Button)
        self.buttonPosition(Button, (self.window.width / 2 - self.ButtonDict['EXIT'].buttonTextWidth * 2,
                                     self.window.height - self.ButtonDict['EXIT'].buttonTextHeight * 2))

    def MakeNEXT(self):
        Button = 'NEXT'
        self.makeButtons(Button)
        self.buttonPosition(Button, (self.window.width / 2 + self.ButtonDict['NEXT'].buttonTextWidth * 2,
                                     self.window.height - self.ButtonDict['NEXT'].buttonTextHeight * 2))

    def MakeFINISH(self):
        Button = 'FINISH'
        self.makeButtons(Button)
        self.buttonPosition(Button, (self.window.width / 2 - self.ButtonDict['FINISH'].buttonTextWidth * 2,
                                     self.window.height - self.ButtonDict['FINISH'].buttonTextHeight * 2))

    def MakeTRYME(self):
        Button = 'TryMe'
        self.makeButtons(Button)
        self.makeButtons(Button)
        self.buttonPosition(Button, (self.window.width / 2 + self.ButtonDict[Button].buttonTextWidth * 2,
                                     self.window.height - self.ButtonDict[Button].buttonTextHeight * 2))

    def startScreen(self):
        # self.textObject = showTextinWindow.showTextPage()
        self.showLogoSwitch()
        self.textInput('Hello and welcome to the enrollment process!\nPlease press Space to start...')
        self.showText()
        self.showLogoSwitch()

    def Next(self):
        self.resetButtons()

    def calibrationInfo(self, calibration, infoText=''):
        Log.AddToLog("calibration info screen started", 1)

        if calibration == True:
            maxtime = 5
            for i in range(50):
                self.textInput('OK, we´re almost done.\n '
                               'Now we need to perform some calculations.'
                               '\nPlease wait...\n{}'.format('|' * i * 2))
                self.showText(time=maxtime / 50)
        if calibration == False:
            self.textInput(
                'We´re having some trouble getting valid data,\n'
                'please look at the dot in the middle of the screen.\n{}'.format(infoText))
            self.showText(time=4)

    def calibrationOngoing(self, calibrationNumber):
        Log.AddToLog("ongoing calibration info screen started", 1)

        if calibrationNumber > 1:
            self.textInput('Alright, now we have {} correct datasets.'.format(calibrationNumber))
        else:
            self.textInput('Alright, now we have the first correct dataset.')
        self.showText(time=5)

    def PresentID(self, ID):
        Log.AddToLog("PresentID started", 1)
        try:
            self.textInput('Your oculid ID is :' + ID)
            self.showText(time=5)

        except Exception as e:
            Log.AddToLog("exception in PresentID : {}".format(e), 2)

    def ExplainValidation(self):
        self.textInput('We now validate the data quality of the used tracker.\n'
                       'Please look at the cross in the middle of the screen.\n'
                       'Position your eyes within the designated areas,\n'
                       'the size of your eyes will correspond to the distance \nyour sitting away from the screen.\n'
                       'Press NEXT to continue')

        self.MakeEXIT()
        self.MakeNEXT()
        action = self.showText()
        self.functionDict[action]()

    def ExplainCalibration(self):
        self.textInput('Please look at the dot in the middle of screen.')
        #self.showText(time=1.5)
        self.MakeEXIT()
        self.MakeNEXT()
        action = self.showText()
        self.functionDict[action]()

    def ExplainPin(self, digit):
        step = {1: 'first', 2: 'second', 3: 'third', 4: 'forth', 5: 'fifth'}
        number = step[digit]
        if number == 1:
            self.textInput('Now we need to record some data.\nPlease enter the ' +number + ' digit out of five digit pin.')
        else:
            self.textInput('Please enter the ' + number + ' digit out of five digit pin.')
        self.showText(time = 5)

    def FeatureVecroteSucsses(self,AmountOfFeature,TargetFeatureVectore):
        AmountOfFeature=str(AmountOfFeature)
        TargetFeatureVectore=str(TargetFeatureVectore)
        self.textInput('Good work, we collected ' + AmountOfFeature + ' out of ' + TargetFeatureVectore + '.')
        self.showText(time = 2)

    def FeatureVecroteFailed(self):

        self.textInput('Creating feature vector failed.\n'
                       'Please try again...\nPress NEXT to continue.')
        self.MakeEXIT()
        self.MakeNEXT()
        action = self.showText()
        self.functionDict[action]()

    def ModelSucsses(self):
        self.textInput('Great it worked You are now enrolled to oculid system!\nThanks for your time.\n'
                       'if you wish to continue and try the program press on the TryMe button')
        self.MakeFINISH()
        self.MakeTRYME()
        action = self.showText()
        return (self.functionDict[action]())

    def ModelFaild(self):
        self.textInput('Creating Model Failed, we need to collect more data.')
        self.MakeEXIT()
        self.MakeNEXT()
        action = self.showText()
        self.functionDict[action]()

    def FiveCollected(self):
        # BEING CLOSE FROM THE MAIN WINDOW
        self.textInput('The software is processing the data, it may take a couple of seconds.')
        self.showText()

    def ValidationSuccess(self):
        Log.AddToLog("validation info screen started", 1)
        self.textInput('That looks good, moving on...')
        self.MakeNEXT()
        action = self.showText()
        self.functionDict[action]()

    def ValidationFailed(self, tries):
        tries = str(tries)
        self.textInput(
            'Validation failed you still have ' + tries + ' tries .\nMake sure your eyes are positioned in the designated area')
        self.MakeEXIT()
        self.MakeNEXT()
        action = self.showText()
        self.functionDict[action]()

    def ValidationExit(self):

        self.textInput('Validation failed. exiting the app, please contact customer support')
        self.showText(time=8)
        self.ExitKillMe()

    def ItsMe(self, ID):
        self.textInput('Hi ' + ID + ',Good to have you back')
        self.MakeFINISH()
        self.MakeTRYME()
        action = self.showText()
        self.functionDict[action]()

    def ItsNotMe(self):
        self.textInput('Probability too low.')
        self.MakeFINISH()
        self.MakeTRYME()
        action = self.showText()
        self.functionDict[action]()

    def MovementFailed(self):
        Log.AddToLog("validation info screen started", 1)
        self.textInput('oha, oha something happend we colud not trace your pin , please repeat the task')
        self.MakeNEXT()
        action = self.showText()
        self.functionDict[action]()

    def correct_digit(self):
        Log.AddToLog("validation info screen started", 1)
        self.textInput('correct digit')
        self.showText(time=3)

    def wrong_digit(self):
            Log.AddToLog("validation info screen started", 1)
            self.textInput('wrong digit, please try again to enter the last digit')
            self.showText(time=5)

    def cali_faild(self):
        Log.AddToLog("cali_faild info screen started", 1)
        self.textInput('oha, oha something happend , please repeat the task')
        self.MakeEXIT()
        self.MakeNEXT()
        action = self.showText()
        self.functionDict[action]()

