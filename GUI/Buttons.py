# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"

import pygame
from toGroup import WindowPropertys

class buttons():
    def __init__(self,window,text):
        pygame.init()
        pygame.font.init()
        if isinstance(window, WindowPropertys.windowPropertys):
            self.window = window
            self.myfont = pygame.font.SysFont('Arial', self.window.valueResizer(50))
            self.myfont_small = pygame.font.SysFont('Arial', self.window.valueResizer(38))
            self.makeButton(text)

        else:
            raise(TypeError)

    def makeButton(self,text):
        '''Returns a button with a given text and its width and height'''
        self.buttonText = self.myfont_small.render(text, 1, (200, 200, 200))
        self.buttonTextWidth, self.buttonTextHeight = self.myfont_small.size(text)


    def placeButton(self,pos):
        self.buttonPos = self.window.screen.blit(self.buttonText, (pos))

    def checkIfButtonWasPressed(self,event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            self.mousePos = pygame.mouse.get_pos()
            if self.buttonPos.collidepoint(self.mousePos):
                return True
            else:
                return False
