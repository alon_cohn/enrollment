# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"
from GUI import showTextinWindow
from Utils import Log
from GUI.showTextinWindow import showTextPage


class explanations(showTextPage):
    def __init__(self):
        super().__init__()


    def ExplainValidation(self):
        self.textInput('All Germanic languages, including English, derive from a\n'
                       ' language or group of languages (reconstructed as Proto-Germanic)\n'
                       ' that had very free word order just like Latin because the inflections\n'
                       ' made it possible to move parts of speech around and still know which was which.\n '
                       'Since German still has more inflections, you can still do this in German to a greater extent.\n'
                       ' “Den Mann schlägt die Frau” (answering the question: “whom does the woman beat?”) is an unusual word order\n '
                       'even in German, but it’s correct and clearly distinguishable from “Der Mann schlägt die Frau”, which is the more likely meaning\n '
                       '(“the man beats the woman”) expressed in the more standard word order.')
        self.showText(time=20)


    def ExplainCalibration(self):
        self.textInput('Dont be an ASS \n'
                       'in the next screen there will be a point in the middele of the screen\n'
                       'just look on it and try not to blink')
        self.showText(time=10)



    from Utils import Log
    import time
    import pygame

