import os
import json
from Utils import Log

MY_FILE= '\\Conf.json'#''\\MouseConf.json'#'\\Conf.json' #'\\Conf.json'# '\\MouseConf.json'

def    ReturnParameter(ParameterName=None, conf_file_name='\\Conf.json',):
    Log.AddToLog('ReturnParameter started',1)
    try:
        ConfFolder = os.path.dirname(__file__)
        ConfFile=ConfFolder + conf_file_name
        with open(ConfFile) as json_data_file:
            Data = json.load(json_data_file)
            ParamterValue=Data[ParameterName]
            Log.AddToLog('ReturnParameter return - {} = {}'.format(ParameterName,ParamterValue),1)
            return ParamterValue
    except Exception as e:
        Log.AddToLog("exception in ReturnParameter :  {}".format(e),2)

def update_paramter(Parameter_name, parameter_value):
    Log.AddToLog('update_paramter started',1)
    try:
        ConfFolder = os.path.dirname(__file__)
        ConfFile = ConfFolder + MY_FILE  # '\\MouseConf.json'

        with open(ConfFile, "r") as jsonFile:
            data = json.load(jsonFile)

        tmp = data[Parameter_name]
        data[Parameter_name] = parameter_value

        with open(ConfFile, "w") as jsonFile:
            json.dump(data, jsonFile)
    except Exception as e:
        Log.AddToLog("exception in update_paramter :  {}".format(e),2)