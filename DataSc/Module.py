from DataSc.CaliData import caliData
from DataSc.PIN_DATA import pinData
from DataSc.CaliPinData import caliPinData

from DataSc import Log
import pandas as pd
from colorama import  *
import os
import time
from DataSc import Log

class module:
    def __init__(self,InitDf,FileName):
        Log.AddToLog('module class started to work, file name: {}'.format(FileName))
        self.ModuleDict = {'CALI': caliData, 'PIN': pinData, 'CALI+PIN':caliPinData}
        self.MyData = InitDf.copy()
        self.FileName=FileName
        self.WhichDataType()
        self.Aqcu=self.InitModule()

    def WhichDataType(self):
        Log.AddToLog('WhichDataType started ')
        try:
            seperateNames=self.FileName.split('_')
            self.cycle = seperateNames[0]
            self.id = seperateNames[1]
            self.device = seperateNames[2]
            self.module = seperateNames[3]
        except Exception as e:
            Log.AddToLog("exception in WhichDataType :  {}".format(e), 2)
            exit()

    def InitModule(self):
        Log.AddToLog('InitModule started')
        try:
            if self.module not in self.ModuleDict:
                raise RuntimeError
            else:
                Log.AddToLog("The choosen module is : {} trying to connect".format(self.module),1)
                DataAnalsysObj=self.ModuleDict[self.module](self.MyData, self.device)
                if not DataAnalsysObj:
                    Log.AddToLog("DataAnalsysObj is False Check the chosen module exiting the program".format(self.module), 1)
                    print(Fore.BLUE + 'DataAnalsysObj is False Check the chosen module exiting the program, exiting the program' + Fore.RESET)
                    raise Exception
                else:
                    return DataAnalsysObj
        except Exception as e:
            Log.AddToLog("exception in initMoudle :  {}".format(e), 2)
            exit()


