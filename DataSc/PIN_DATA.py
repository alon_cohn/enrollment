from DataSc.Data import data
import pandas as pd
import numpy as np
from DataSc import Log
from colorama import *
from Configuration import ParameterManger as PM
from scipy.stats import linregress
import matplotlib.pyplot as plt
import itertools
from sklearn.metrics import mean_squared_error
from statsmodels.tools.eval_measures import rmse

from scipy import stats
from math import sqrt


class pinData(data):
    def __init__(self, MyData,Device):
        Log.AddToLog('PIN was choosen')
        super().__init__()
        self.PinSequence = ['Movement1','Movement2']
        self.SeperationFlag = True
        self.StopFlag = False
        self.MyData = MyData[MyData.Note.isin(self.PinSequence)]
        self.revision = PM.ReturnParameter('self.revision')
        # need to be syncronize with the reales number at Data_Sc GIT. This should always be
        # changed when adding/removing features, also Preprocessingmatrix has to be updated
        self.device = Device
        self.rollwindowlength = PM.ReturnParameter('self.rollwindowlength')  # time in seconds
        self.window_size = PM.ReturnParameter('self.window_size')  # was 10 transfer to 70% of the amount of samples.
        self.shift = self.window_size - 1

    def runner(self):
        Log.AddToLog('runner.caliData started')
        try:
            # if filter dont work
            self.FilterBy ='Note'
            if not self.Filter(self.MyData.copy(), self.FilterBy):
                return False
            # TODO: This only works if we group the data frame, when only one group is available this will fail!!!!!!!!!!!!
            # TODO: implement check
            '''--------------------------clean dada--------------------------------------------------------'''
            print('Filter ended ,starting to clean')
            self.CleanThreshold =PM.ReturnParameter('self.CleanThreshold')
            # self.CleanedData = self.FilteredData.apply(lambda x: self.CleanData(x, CleaningThreshold = CleanThreshold))
            self.CleanedData = self.LambdaReplace( self.FilteredData ,1)
            if isinstance(self.CleanedData, bool): return False
            #if isinstance(self.CleanedData, bool):
            print('Cleaning ended')
            # only go on with calculation if all subsequences of calibration passed Cleaning process
            if self.CleanedData.Clean.all():
                Log.AddToLog('Cleaning occured and worked,start calculation '.format(self.CleanThreshold))

                '''--------------------------CalculateAdditionalParameters--------------------------------------------------------'''
                # define the number of samples for rolling mean window
                # TODO: should be changed to a fixed time and not samples

                # self.CleanedAddCalcData = self.CleanedData.reset_index(drop=True).groupby(self.FilterBy).apply(lambda x: self.CalculateAdditionalParameters(x,rollwindowsize))
                self.CleanedAddCalcData = self.LambdaReplace(self.CleanedData, 2)
                if isinstance(self.CleanedAddCalcData, bool): return False

                '''--------------------------CalculateRawFeatures--------------------------------------------------------'''
                # self.RawFeatureData = self.CleanedAddCalcData.reset_index(drop=True).groupby(self.FilterBy).apply(lambda x: self.CalculateRawFeatures(x))
                self.RawFeatureData = self.LambdaReplace(self.CleanedAddCalcData , 3)
                if isinstance(self.RawFeatureData, bool): return False

                '''--------------------------FinalFeatures--------------------------------------------------------'''
                # TODO:FUTURE This is only needed when data frames are grouped and not calculated over the whole calibration sequence
                self.FinalFeatures = pd.DataFrame(data=(self.RawFeatureData.sum(axis=0))).T
                #self.OneFeatureVector = self.OneFeatureVector.drop(columns=['Note'])
                #need to talk about it with stefan
                #pin model 56-57
                # after party 24/25
                self.FinalFeatures['Revision'] = pd.Series(self.revision)

                print(Fore.GREEN + 'DataSc: Creating Feature vector succeeded , Returning True' + Fore.RESET)
                return True
            else:
                # say to alon, I need new data because
                print \
                    (Fore.RED + 'Data is insufficent according to threshold  ,need to restart collecting data, returning False' + Fore.RESET)
                return False
        except Exception as e:
            print(Fore.RED + 'returning False, exaption' + Fore.RESET)
            Log.AddToLog("exception in runner.caliData :  {}".format(e), 2)
            return False

    def LambdaReplace(self, DataToFilter ,flag):
        Log.AddToLog('LambdaReplace started flag equal = {}'.format(flag))
        try:
            NameToIgnore = ['FinalCalculation', 'Transition']
            if flag != 1:
                Data = DataToFilter.groupby(self.FilterBy)
            else:
                Data =DataToFilter
            DataFrameToReturn = pd.DataFrame()
            for keys, df in Data:
                if flag == 1:
                    tempDf = self.CleanData(df, CleaningThreshold=self.CleanThreshold)
                elif flag == 2:
                    tempDf = self.calcaulte_add_params(df)
                elif flag == 3:
                    if df[self.FilterBy].values[0] in NameToIgnore:
                        continue
                    tempDf = self.CalculateRawFeatures(df)
                if isinstance(tempDf, bool):
                    Log.AddToLog('LambdaReplace key = {} and flag = {} return False'.format(keys, flag))
                    print((Fore.RED + 'key = {} and flag = {} return False' + Fore.RESET).format(keys, flag))
                    return False
                else:
                    Log.AddToLog('LambdaReplace key = {} and flag = {} succed'.format(keys, flag))
                    print(('key = {} and flag = {} succed').format(keys, flag))
                    DataFrameToReturn = pd.concat([DataFrameToReturn, tempDf], axis=0)
            return DataFrameToReturn
        except Exception as e:
            Log.AddToLog("exception in LambdaReplace :  {}".format(e), 2)
            return False


    # Function to groupe data by Note column
    # self.FilterData is a group obkect meaning its a dict that the keys are the nots and the valuse in the corresponding df
    def Filter(self,df, Filter=None):
        Log.AddToLog('Filter started')
        try:
            grouped = df.copy()
            if Filter is None:
                self.FilteredData = grouped
            else:
                KeysName = []
                grouped = grouped.groupby(Filter)
                # if DataToClean is an empty Group
                checkIfNotEmpty = (list(grouped))
                if not checkIfNotEmpty:
                    print(Fore.RED + 'DataToClean after group by is an empty DataFrame!!' + Fore.RESET)
                    Log.AddToLog('DataToClean after group by is a  empty DataFrame!!')
                    return False
                Log.AddToLog('Data Been split to the following groups: {}'.format(grouped.groups.keys()))
                print('Data Been split to the following groups: {}'.format(grouped.groups.keys()))
                self.FilteredData = grouped
            return True
        except Exception as e:
            Log.AddToLog("exception in Filter :  {}".format(e), 2)
            return False


    def CleanData(self, DfToClean, CleaningThreshold):
        Log.AddToLog('CleanData started for group : {} '.format(DfToClean[self.FilterBy].values[0]))
        try:
            NameToIgnore = ['FinalCalculation', 'Transition']
            if DfToClean[self.FilterBy].values[0] in NameToIgnore:
                return DfToClean
            # File Columns:
            '''
            TimeStamp, EyeTime, RawX, RawY, AverageX, AverageY, Left_RawX, Left_RawY, Left_PupilSize, Left_PupilCenterX, \
            Left_PupilCenterY, Left_PupilCenterZ, Right_RawX, Right_RawY, Right_PupilSize, Right_PupilCenterX, \
            Right_PupilCenterY, Right_PupilCenterZ, CorrectedX, CorrectedY, Note,
            '''
            # define columns that should be checked for outliers
            drop_names = ['RawX', 'RawY',
                          'Left_PupilSize', 'Right_PupilSize',
                          'Left_PupilCenterX', 'Left_PupilCenterY',
                          'Left_PupilCenterZ', 'Right_PupilCenterX',
                          'Right_PupilCenterY', 'Right_PupilCenterZ']

            raw_shape = DfToClean.shape
            # Remove duplicate rows ... introduced when Data Collection request is faster than the tracker
            DfToClean = DfToClean[~DfToClean.duplicated(subset='EyeTime')]
            row_miss_eyetime = ((raw_shape[0]-DfToClean.shape[0])/raw_shape[0])*100
            Log.AddToLog('Percentage of duplicate EyeTime Rows : {} '.format(row_miss_eyetime))

            # Remove rows where there is no change in both TargetX and TargetY
            DfToClean =  DfToClean[~((DfToClean.TargetX.diff() == 0) & (DfToClean.TargetY.diff() == 0))]
            row_miss_target = (((raw_shape[0] - DfToClean.shape[0]) / raw_shape[0]) * 100) - row_miss_eyetime
            Log.AddToLog('Percentage of duplicate TargetXY Rows : {} '.format(row_miss_target))

            print(Fore.RED + 'Duplicates: {: 0.2f}% EyeTime  + {: 0.2f}% TargetXY and from initial data shape'.format(row_miss_eyetime, row_miss_target)
                  + Fore.RESET)

            # get initial data frame dimensions
            shape_initial = DfToClean.shape

            # Find 4 consecutive zeros in raw data and remove them from dataframe to avoid single eyetracking
            flagBool, DfToClean = self.get_consecutive_zeros(DfToClean, 4)
            if not flagBool:
                return False
            # remove rows with for zeros
            # Remove outliers in all columns of interest
            DfToClean = DfToClean[DfToClean[drop_names].apply(lambda x: self.RemoveOutlier(x)).all(axis=1)]
            # Remove invalid pyhysiological values
            # remove pupil size < 1mm und > 9mm for left and right pupil
            # return all rows where left pupil size is between 1 and 9
            DfToClean = DfToClean[(DfToClean.Left_PupilSize > 1) & (DfToClean.Left_PupilSize < 9)]
            DfToClean.assign(Clean=pd.Series(np.nan))
            DfToClean.assign(Comment=pd.Series(np.nan))

            if DfToClean.empty:
                Log.AddToLog('No valid left pupil size data left, DfToClean.empty')
                return False
            DfToClean = DfToClean[(DfToClean.Right_PupilSize > 1) & (DfToClean.Right_PupilSize < 9)]
            if DfToClean.empty:
                Log.AddToLog('No valid right pupil size data left, DfToClean.empty')
                return False
            # remove all rows with NAs
            DfToClean = DfToClean.dropna(subset=drop_names)
            Log.AddToLog('CleanData main is finished, entering  IsDataAboveThreshold')
            return (self.IsDataAboveThreshold(DfToClean, CleaningThreshold, shape_initial))

        except Exception as e:
            Log.AddToLog("exception in CleanData :  {}".format(e), 2)
            return False

    def calcaulte_add_params(self, df):
        Log.AddToLog('callc_add_params started for group : {} '.format(df[self.FilterBy].values[0]))
        try:

            # this is a redandet which i made only because we still dont have
            # any calculation for FINAL CALCULATION and TRANSITION
            # FINAL CALCULATION and TRANSITION will give always NAN-> will resault returning False
            NameToIgnore = ['FinalCalculation', 'Transition']
            if df[self.FilterBy].values[0] in NameToIgnore:
                return df

            '''
            # if i am in debug mode shape might be small so need to adjust window width
            if df.shape[0] < self.window_size:
                self.window_size = int((1/7) * df.shape[0])
                self.shift = self.window_size-1
            rawx_roll = pd.Series(df.RawX.rolling(window=self.window_size, axis=0).mean().shift(-self.shift))
            rawy_roll = pd.Series(df.RawY.rolling(window=self.window_size, axis=0).mean().shift(-self.shift))
            df = df.assign(RawX_roll=rawx_roll)
            df = df.assign(RawY_roll=rawy_roll)
            '''

            for i in range(1,6):
                df = self.parser_add(df,i)
                if not (isinstance(df, pd.DataFrame)):
                    print('Problom at i = {} in parser_add'.format(i))
                    return False
            return df
        except Exception as e:
            Log.AddToLog("exception in callc_add_params :  {}".format(e), 2)
            return False

    def parser_add(self, df, flag):
        try:
            if flag == 1:
                df = self.CalculateRollmeans(df, self.rollwindowlength, 'TimeStamp',
                                             ['Left_PupilSize', 'Right_PupilSize', 'RawX', 'RawY'])
            elif flag == 2:
                df = self.CalculateSlope(df, 'TimeStamp',
                                         ['Left_PupilSize', 'Right_PupilSize', 'Left_PupilSize_Rollmean',
                                          'Right_PupilSize_Rollmean'])
            elif flag == 3:
                df = self.CalculatePupDist(df,
                                           'Right_PupilCenterX',
                                           'Right_PupilCenterY',
                                           'Right_PupilCenterZ',
                                           'Left_PupilCenterX',
                                           'Left_PupilCenterY',
                                           'Left_PupilCenterZ')
            elif flag == 4:
                df = self.dist_diff(df)
            elif flag ==5 :
                df = self.Velocity(df)
            return df
        except Exception as e:
            Log.AddToLog("exception in parser_add :  {}".format(e), 2)
            return False


    #calculate:
    #1. difference cetween the eye movment to the traget movment
    #2. the eecumlated error
    #3. eye movemnt (x^2+y^2)^0.5
    def dist_diff(self,df):
        try:
            eye_dx =  df.RawX.diff()
            eye_dy = df.RawY.diff()
            target_dx = df.TargetX.diff()
            target_dy = df.TargetY.diff()
            eye_dist = np.sqrt(eye_dx**2 + eye_dy**2)
            target_dist = np.sqrt(target_dx**2 + target_dy** 2)
            posdist_diff = target_dist - eye_dist
            # calculate cumulated step by step difference of Target to Eye
            posdist_diff_cum = np.cumsum(np.abs(posdist_diff))#.cumsum()
            df = df.assign(Eye_Dist_Traveled=eye_dist)
            df = df.assign(Target_Dist_Traveled=target_dist)
            df = df.assign(Target_Eye_Distance_Diff=posdist_diff)
            df = df.assign(Target_Eye_Accumulate_Dist_Diff=posdist_diff_cum)
            return df
        except Exception as e:
            Log.AddToLog("exception in dist_diff :  {}".format(e), 2)
            return False

    # calculate:
    # 1. p gain  the difference between the eye velocity to target.
    # 2. p gain roll the difference between the eye rolling velocity to target rolling.
    # 3. eye movemnt (x^2+y^2)^0.5
    def Velocity(self,df):
        try:

            dx_dt =  df.RawX.diff() / df.TimeStamp.diff()
            dy_dt = df.RawY.diff()  / df.TimeStamp.diff()
            #vel_magnitude
            velo_eye= np.sqrt(dx_dt**2 + dy_dt**2)
            #!!!!!!!!!!!!!!!!!!!!!!!!EXTREMLY important!!!!!!!!!!
            # In case we want to activate the  preprocceing matrix the speed in no longer can be assume as the current spedd.
            # the next IF statment is need only because of the preproccesing matrix
            # it ask eigentlich if the df is from the enrollment or the preprocessing.
            # if the trail word will be deleted from the df it wont work
            if 'Trial' in df.columns:
                x_speed = df.TargetX.diff()/df.TimeStamp.diff()
                y_speed = df.TargetY.diff() / df.TimeStamp.diff()
                self.velo_target = np.sqrt( x_speed**2 + y_speed**2)
            else:
                self.velo_target = PM.ReturnParameter('self.targetSpeed')
            '''
            # Todo: velo target sometimes zero which will lead to infinite values in gain calculation. Quick fix is to
            # remove those rows from data frame as they hold no usable information
            df = df[self.velo_target!=0]
            # remove zero values from velo target vector
            self.velo_target = self.velo_target[self.velo_target != 0]
            '''
            df = df.assign(Eye_velocity_px_s=velo_eye)
            p_gain = (velo_eye / self.velo_target)
            df = df.assign(Pursuit_Gain=p_gain)

            #rolling speed
            df = self.CalculateRollmeans(df,self.rollwindowlength,'TimeStamp',['Eye_velocity_px_s'])
            p_gain_roll = (df.Eye_velocity_px_s_Rollmean / self.velo_target)
            df = df.assign(Pursuit_Gain_Rollmean=p_gain_roll)


            # at the end of the function self.velvo_target expect to be an ine- one number
            #so even if we activate it from the old collected data we have to restore it.
            if 'Trial' in df.columns:
                self.velo_target = self.velo_target.mean()
            return df
            # saccade filter
            #df = df.assign(Eye_velocity_savgol_px_s=velo_eye_savgol)
            #p_gain_savgol = (velo_eye_savgol / velo_target)
            # p_gain = np.abs(p_gain - p_gain.mean()) / p_gain.std() < 3
            #df = df.assign(Pursuit_Gain_Savgol=p_gain_savgol)

        except Exception as e:
            Log.AddToLog("exception in CleanData :  {}".format(e), 2)
            return False

    def CalculateRawFeatures(self, df):
        try:


            Note = df[self.FilterBy].values[0]
            param_list = [
                        'Left_PupilSize',
                        'Right_PupilSize',
                        'Eye_Dist_Traveled',
                        'Target_Eye_Distance_Diff',
                        'Target_Eye_Accumulate_Dist_Diff',
                        'Eye_velocity_px_s',
                        'Eye_velocity_px_s_Rollmean',
                        'Pursuit_Gain',
                        'Pursuit_Gain_Rollmean'
                        ]#'Pursuit_Gain_Savgol''Eye_velocity_savgol_px_s','Target_velocity_px_s','Target_velocity_roll_px_s',

            slope, intercept, r_value, p_value, std_err = linregress(df.RawX.dropna(), df.RawY.dropna())
            slope_roll, intercept_roll, r_value_roll, p_value_roll, std_err_roll = linregress(df.RawX_Rollmean.dropna(),
                                                                                              df.RawY_Rollmean.dropna())
            rmse_x = rmse(df.RawX.dropna(), df.TargetX)
            rmse_y = rmse(df.RawY.dropna(), df.TargetY)
            # rmse_x_roll = 0
            rmse_x_roll = rmse(df.RawX_Rollmean.dropna(), df.TargetX[~np.isnan(df.RawX_Rollmean)])
            # rmse_y_roll = 0
            rmse_y_roll = rmse(df.RawY_Rollmean.dropna(), df.TargetY[~np.isnan(df.RawY_Rollmean)])

            parameter_add_values = pd.Series([
                                                slope,
                                                intercept,
                                                r_value,
                                                p_value,
                                                std_err,
                                                slope_roll,
                                                intercept_roll,
                                                r_value_roll,
                                                p_value_roll,
                                                std_err_roll,
                                                rmse_x,
                                                rmse_y,
                                                rmse_x_roll,
                                                rmse_y_roll,
                                                self.velo_target
                                              ])
            # create series to later concatenate to other parameters
            temp = ['Reg_slope_RawXY',
                    'Reg_intercept_RawXY',
                    'Reg_r_value_RawXY',
                    'Reg_p_value_RawXY',
                    'Reg_std_err_RawXY',
                    'Reg_slope_RawXY_Rollmean',
                    'Reg_intercept_RawXY_Rollmean',
                    'Reg_r_value_RawXY_Rollmean',
                    'Reg_p_value_RawXY_Rollmean',
                    'Reg_std_err_RawXY_Rollmean',
                    'RMSE_RawX',
                    'RMSE_RawY',
                    'RMSE_RawX_Rollmean',
                    'RMSE_RawY_Rollmean',
                    'Target_velocity_px_s']


            parameter_add_names = []
            for name in temp:
                name = name + '_' + Note
                parameter_add_names.append(name)


            mean_value = df[param_list].mean()
            median_value = df[param_list].median()
            std_value = df[param_list].std()
            min_value = df[param_list].min()

            # 10% of smallest values mean
            # TODO: order nach column ist irgendwie quatsch aber egal weil gleich der mean gebildet wird
            mean_min_value = df[param_list].dropna().nsmallest(np.round(df.shape[0] * .1).astype(np.int),
                                                               columns=param_list).mean()
            max_value = df[param_list].max()

            # 10% of largest values mean
            mean_max_value = df[param_list].dropna().nlargest(np.round(df.shape[0] * .1).astype(np.int),
                                                              columns=param_list).mean()

            parameter_values = pd.concat([mean_value, median_value, std_value,
                                          min_value, mean_min_value, max_value, mean_max_value,
                                          parameter_add_values])

            # create and transpose series to data frame
            df_feature_val = parameter_values.to_frame()
            df_feature_val = df_feature_val.T

            # concatenate the ID and the Value data frame
            # irgendwie müssen die indexe getdroppt werden sonst funktioniert das concat nicht und die dtypes gehen verloren
            df_feature = pd.DataFrame(df_feature_val)

            # , axis = 1, ignore_index=True
            # Define Parameter names
            param_list_mean_val = []
            param_list_median_val = []
            param_list_std_val = []
            param_list_min_val = []
            param_list_mean_min_val = []
            param_list_max_val = []
            param_list_mean_max_val = []
            for s in param_list:
                param_list_mean_val.append('Mean_' + s + '_' + Note)
                param_list_median_val.append('Median_' + s + '_' + Note)
                param_list_std_val.append('Std_' + s + '_' + Note)
                param_list_min_val.append('Min_' + s + '_' + Note)
                param_list_mean_min_val.append('MeanMin_' + s + '_'  + Note)
                param_list_max_val.append('Max_' + s + '_' + Note)
                param_list_mean_max_val.append('MeanMax_' + s + '_' + Note)

            parameter_names = param_list_mean_val + \
                              param_list_median_val + param_list_std_val + param_list_min_val + \
                              param_list_mean_min_val + param_list_max_val + \
                              param_list_mean_max_val + parameter_add_names

            df_feature.columns = parameter_names
            df_feature = df_feature.assign(Note = Note)
            # validate final feature data frame
            if  Note == 'Movement1' or  Note == 'Movement2':
                if df_feature.isna().values.any() :
                    print(Fore.RED + 'Error in CalculateRawFeatures : Calculation introduced NA feature values {}'.
                          format(df_feature.columns[df_feature.isna().any()].tolist()) + Fore.RESET)
                    Log.AddToLog('Error in CalculateRawFeatures : Calculation introduced NA feature values')
                    return False
                else:
                    Log.AddToLog(
                        'CalculateRawFeatures Group name is : {} finished successfully'.format(df[self.FilterBy].values[0]))
                    return df_feature
            else:
                return df_feature

        except Exception as e:
                Log.AddToLog("exception in CleanData :  {}".format(e), 2)
                return False