import pandas as pd
import numpy as np
from DataSc import Log
from colorama import *


'''Here you put properties which have to all data structre'''
class data:
    def __init__(self):
        a=1

    # helper function to remove outliers from a data column mean +/- 3 standard deviations
    def RemoveOutlier(self, DataColumn):
        Log.AddToLog('RemoveOutlier started column : {} '.format(DataColumn.name))
        try:
            #
            if DataColumn.std() == 0:
                logvec = [True] * len(DataColumn)
            else:
                logvec = np.abs(DataColumn - DataColumn.mean()) / DataColumn.std() < 3
            return logvec
        except Exception as e:
            Log.AddToLog("exception in RemoveOutlier :  {}".format(e), 2)
            return False



    def pos_neg_counts(self, mask):
        idx = np.flatnonzero(mask[1:] != mask[:-1])
        if len(idx) == 0:  # To handle all 0s or all 1s cases
            if mask[0]:
                return np.array([mask.size]), np.array([0])
            else:
                return np.array([0]), np.array([mask.size])
        else:
            count = np.r_[[idx[0] + 1], idx[1:] - idx[:-1], [mask.size - 1 - idx[-1]]]
            if mask[0]:
                return count[::2], count[1::2]  # True, False counts
            else:
                return count[1::2], count[::2]  # True, False counts

    def get_consecutive_zeros(self,df, zero_number):
        Log.AddToLog('get_consecutive_zeros started')
        try:
            arr = df.values
            mask = (arr == 0) | (arr == '0')
            zero_count = np.array([self.pos_neg_counts(i)[0].max() for i in mask])
            zero_count[zero_count < 2] = 0
            Log.AddToLog("{} rows with {} consecutive zeros removed".format(sum(zero_count==4), zero_number), 1)
            Log.AddToLog("{} rows with 2 or 3 consecutive ".format(sum((zero_count >= 2) & (zero_count < 4))), 1)

            if (zero_count !=0 ).any():
                print("{} rows with more than {} consecutive zeros removed".format(sum(zero_count >= 4), zero_number))
                print("{} rows with 2 or 3 consecutive ".format(sum((zero_count >= 2) & (zero_count < 4))))
            df = df.drop(df[zero_count >= zero_number].index)
            if df.empty:
                Log.AddToLog("Error in get_consecutive_zeros: Data Frame empty", 2)
                print(Fore.RED + "Error in get_consecutive_zeros: Data Frame empty"  + Fore.RESET)
                return False, 0
            else:
                return True, df
        except Exception as e:
            Log.AddToLog("get_consecutive_zeros:  {}".format(e), 2)
            return False, 0

        # check final shape of the data frame
        # get final df shape
        ## Check how much data is missing after cleaning. If it is below the threshold return False and request new data
    def IsDataAboveThreshold(self, DfToClean, CleaningThreshold, shape_initial):
        Log.AddToLog('IsDataAboveThreshold started group : {} '.format(DfToClean[self.FilterBy].values[0]))
        try:
            shape_final = DfToClean.shape
            AfterCleaning = CleaningThreshold * shape_initial[0]
            if shape_final[0] < CleaningThreshold * shape_initial[0]:
                # TODO: d
                DfToClean['Clean'] = False
                DfToClean['Comment'] = 'Data is shit'
                CleanedData = DfToClean
                Log.AddToLog(
                    'The data  started with shape {} after cleaning the shape change to {} program threshhold = {} after calc  = {} puting False in colum clean  '
                    .format(shape_initial[0], shape_final[0], CleaningThreshold,
                            CleaningThreshold * shape_initial[0]))

                print((Fore.RED + 'The data  started with shape {} after cleaning the shape change to {} program threshhold = {} after calc  = {} puting False in colum clean  '+ Fore.RESET)
                    .format(shape_initial[0], shape_final[0], CleaningThreshold,
                            CleaningThreshold * shape_initial[0]))
                Log.AddToLog(
                    'CleadData Failed returning False for Note : {} '.format(DfToClean[self.FilterBy].values[0]))
                print('False: ', DfToClean[self.FilterBy].values[0])
                return False
            else:
                DfToClean['Clean'] = True
                DfToClean['Comment'] = 'Data Fresh and Clean'
                CleanedData = DfToClean[DfToClean['Clean'] == True].copy()
                Log.AddToLog(
                    'The data  started with shape {} after cleaning the shape change to {} program threshhold = {} after calc  = {} , all good '
                    .format(shape_initial[0], shape_final[0], CleaningThreshold,
                            CleaningThreshold * shape_initial[0]))
                Log.AddToLog('IsDataAboveThreshold Succeded returning True for Note : {} '.format(
                    DfToClean[self.FilterBy].values[0]))
                print((Fore.GREEN + 'IsDataAboveThreshold,True: {}' + Fore.RESET).format(DfToClean[self.FilterBy].values[0]))
                return CleanedData
        except Exception as e:
            Log.AddToLog("exception in IsDataAboveThreshold :  {}".format(e), 2)
            return False



    def CalculateSlope(self, df, timestamp, parameters):
        '''only if pupil values are available'''
        Log.AddToLog('Calculate Slopes started')
        try:
            for singleParam in parameters:
                newName = singleParam + '_Slope'
                ParamSlope = pd.Series(np.gradient(df[singleParam]),
                                   df[timestamp], name='slope')
                # data validity check
                if ParamSlope.isnull().all() or ParamSlope.abs().sum() == 0:
                    print(Fore.RED + 'Error in CalculateSlope: invalid slope values introduced for {}'.format(singleParam) + Fore.RESET)
                    Log.AddToLog('Error in CalculateSlope: invalid slope values introduced for {}'.format(singleParam))
                    return False
                else:
                    df[newName] = ParamSlope.values

            return df
        except Exception as e:
            Log.AddToLog("exception in CalculatePupilSlope :  {}".format(e), 2)
            return False

    def CalculateRollmeans(self, df, rollwindowlength, timestamp , parameters, maxNATreshRatio = .5):
        '''only if pupil values are available'''
        Log.AddToLog('CalculateRollmeans started')
        try:
            for singleParam in parameters:
                newName = singleParam + '_Rollmean'
                #calculaze number of samples within rollwindowlength
                rollwindowsize = np.round((df.shape[0] * rollwindowlength) / df[timestamp].diff().sum()).astype(np.int)
                s = rollwindowsize - 1
                ParamRollmean  = pd.Series(df[singleParam].rolling(window=rollwindowsize, axis=0).mean().shift(-s))
                # data validity check
                nanRatio = ParamRollmean.isna().sum() / len(ParamRollmean)

                if nanRatio >= maxNATreshRatio:
                    print(
                        Fore.RED + 'Error in CalculateRollmeans for {}.Too many NAs introduced'.format(singleParam) + Fore.RESET)
                    Log.AddToLog(
                        'Error in CalculateRollmeans for {}.Too many NAs introduced'.format(singleParam))
                    return False
                else:
                    df[newName] = ParamRollmean
                    Log.AddToLog('Check i.O.: NA roll ratio in for param {}'.format(singleParam))
            return df

        except Exception as e:
            Log.AddToLog("exception in CalculateRollmeans:  {}".format(e), 2)
            return False


    def CalculatePupDist(self, df, RightX, RightY, RightZ, LeftX, LeftY, LeftZ):
        '''only if pupil values are available'''
        Log.AddToLog('CalculatePupDist started')
        try:
            pupdist2D = np.sqrt(((df[LeftX] - df[RightX]) ** 2) +
                                ((df[LeftY] - df[RightY]) ** 2))

            df = df.assign(Pupil_Distance_2D = pupdist2D)

            if ((RightZ == None)|(LeftZ == None)):
                Log.AddToLog('No 3D pupil parameters available')
                return df
            else:
                pupdist3D = np.sqrt(((df[LeftX] -
                                      df[RightX]) ** 2) +
                                    ((df[LeftY] -
                                      df[RightY]) ** 2) +
                                    ((df[LeftZ] -
                                      df[RightZ]) ** 2))

                df = df.assign(Pupil_Distance_3D = pupdist3D)
            return df

        except Exception as e:
            Log.AddToLog("exception in CalculatePupDist:  {}".format(e), 2)
            return False