__author__ = "Alon Cohn,06.02.2018"
'''The target is to create a file called a DevLog.txt in the repository,
the Log will contain software Data'''

import logging
import datetime
from threading import Thread

#logging.getLogger("urllib3").setLevel(logging.WARNING)
#logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('DataLog.log')
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)

'''the data can be divided to 3 different cases information debug or error before each line 
 added to the log default is INFO '''
INFO = 0
DEBUG = 1
ERROR = 2
#            Logger.AddToLog("hereou write your msg  {variable}".format(x))

def AddToLog(msg: str, level: int = INFO):
    t1 = Thread(target=AddToLogNow,args=(msg,level))
    t1.start()

def AddToLogNow(msg: str, level: int = INFO):
    try:
        msg = str(datetime.datetime.now()) +":  " +  msg
        if level is INFO:
            logger.info(msg)
        elif level is DEBUG:
            logger.debug(msg)
        elif level is ERROR:
            logger.error(msg)
        else:
            raise Exception("unknown level for logger {}".format(level))
    except Exception as e:
        AddToLog("exception in AddToLog :  {}".format(e),2)