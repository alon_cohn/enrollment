from DataSc.Data import data
import pandas as pd
import numpy as np
from DataSc import Log
from colorama import *
from Configuration import ParameterManger as PM

# *************not in use**************************
class pinData(data):
    def __init__(self, MyData,Device):
        Log.AddToLog('PIN was choosen')
        super().__init__()
        self.PinSequence = ['Movement 1','Movement 2']
        self.SeperationFlag = True
        self.StopFlag = False
        self.MyData = MyData
        self.revision = PM.ReturnParameter(
            'self.revision')  # need to be syncronize with the reales number at Data_Sc GIT. This should always be
        # changed when adding/removing features, also Preprocessingmatrix has to be updated
        self.device = Device
        self.rollwindowlength = PM.ReturnParameter('self.rollwindowlength')  # time in seconds

    @property
    def runner(self):
        Log.AddToLog('runner.pinData started')
        try:
            # Filter by Note groups and apply cleaning to all groups
            self.FilterBy = 'Note'
            FilteredData = self.Filter(self.MyData.copy(), self.FilterBy)
            if isinstance(FilteredData, bool):
                Log.AddToLog('DataFrame could not be grouped by {}'.format(self.FilterBy))
            else:
                print('Filter ended ,starting to clean')

            # Clean the data
            self.CleanThreshold = PM.ReturnParameter('self.CleanThreshold')
            CleanedData = pd.DataFrame()
            for keys, df in FilteredData:
                tempCleanedData = self.CleanData(df, self.CleanThreshold)
                if isinstance(tempCleanedData, bool):
                    # for now only return false when movement 1 or two could not be cleaned
                    if (keys == 'Movement 1') | (keys== 'Movement 2'):
                        Log.AddToLog('PIN sequence {} failed in cleaning. Need new samples'.format(keys))
                        return False
                    else:
                        Log.AddToLog('Warning: PIN sequence {} failed in cleaning.'.format(keys))
                else:
                    CleanedData = pd.concat([CleanedData, tempCleanedData], axis = 0)
            print('Cleaning ended')

            '''------------------------Calculate additional parameters-----------------------------------------------'''
            # Filter by Note groups and apply cleaning to all groups
            FilteredData = self.Filter(CleanedData.copy(), self.FilterBy)
            if isinstance(FilteredData, bool):
                Log.AddToLog('DataFrame could not be grouped by {}'.format(self.FilterBy))
            else:
                print('Filter ended ,starting to calculate additional parameters')

            CleanedAddRollmeans = pd.DataFrame()
            for keys, df in FilteredData:
                Log.AddToLog('Key {}'.format(keys))
                tempCleanedAddRollmeans = self.CalculateRollmeans(df,self.rollwindowlength, 'TimeStamp',
                                                                  ['Left_PupilSize', 'Right_PupilSize',
                                                                   'RawX', 'RawY'])
                if isinstance(tempCleanedAddRollmeans, bool):
                    # for now only return false when movement 1 or two could not be cleaned
                    if (keys == 'Movement 1') | (keys == 'Movement 2'):
                        Log.AddToLog('PIN sequence {} failed in rollmeans calculation. Need new samples'.format(keys))
                        return False
                    else:
                        Log.AddToLog('Warning: PIN sequence {} failed in rollmeans calculation.'.format(keys))
                else:
                    CleanedAddRollmeans = pd.concat([CleanedAddRollmeans, tempCleanedAddRollmeans], axis=0)
            print('Rollmean calculation ended')

            # Filter by Note groups and apply cleaning to all groups
            FilteredData = self.Filter(CleanedAddRollmeans.copy(), self.FilterBy)
            if isinstance(FilteredData, bool):
                Log.AddToLog('DataFrame could not be grouped by {}'.format(self.FilterBy))
            else:
                print('Filter ended ,starting to calculate additional parameters')

            CleanedAddSlopes = pd.DataFrame()
            for keys, df in FilteredData:
                Log.AddToLog('Key {}'.format(keys))
                tempCleanedAddSlopes = self.CalculateSlope(df, 'TimeStamp', ['Left_PupilSize',
                                                                           'Right_PupilSize',
                                                                           'Left_PupilSize_Rollmean',
                                                                           'Right_PupilSize_Rollmean'])
                if isinstance(tempCleanedAddSlopes, bool):
                    # for now only return false when movement 1 or two could not be cleaned
                    if (keys == 'Movement 1') | (keys == 'Movement 2'):
                        Log.AddToLog('PIN sequence {} failed in slope calculation. Need new samples'.format(keys))
                        return False
                    else:
                        Log.AddToLog('Warning: PIN sequence {} failed in slope calculation.'.format(keys))
                else:
                    CleanedAddSlopes = pd.concat([CleanedAddSlopes, tempCleanedAddSlopes], axis=0)
            print('Slope calculation ended')

            # Filter by Note groups and apply cleaning to all groups
            FilteredData = self.Filter(CleanedAddSlopes.copy(), self.FilterBy)
            if isinstance(FilteredData, bool):
                Log.AddToLog('DataFrame could not be grouped by {}'.format(self.FilterBy))
            else:
                print('Filter ended ,starting to calculate additional parameters')

            CleanedAddPupdist = pd.DataFrame()
            for keys, df in FilteredData:
                Log.AddToLog('Key {}'.format(keys))
                tempCleanedAddPupdist = self.CalculatePupDist(df,
                                                              'Right_PupilCenterX',
                                                              'Right_PupilCenterY',
                                                              'Right_PupilCenterZ',
                                                              'Left_PupilCenterX',
                                                              'Left_PupilCenterY',
                                                              'Left_PupilCenterZ')
                if isinstance(tempCleanedAddPupdist, bool):
                    # for now only return false when movement 1 or two could not be cleaned
                    if (keys == 'Movement 1') | (keys == 'Movement 2'):
                        Log.AddToLog('PIN sequence {} failed in pupdist calculation. Need new samples'.format(keys))
                        return False
                    else:
                        Log.AddToLog('Warning: PIN sequence {} failed in pupdist calculation.'.format(keys))
                else:
                    CleanedAddPupdist = pd.concat([CleanedAddPupdist, tempCleanedAddPupdist], axis=0)
            print('Pupdist calculation ended')

        except Exception as e:
            print(Fore.RED + 'returning False, exception' + Fore.RESET)
            Log.AddToLog("exception in runner.pinData :  {}".format(e), 2)
            return False


    '''Helper functions'''

    # Function to groupe data by Note column
    # self.FilterData is a group obkect meaning its a dict that the keys are the nots and the valuse in the corresponding df
    def Filter(self,df, Filter=None):
        Log.AddToLog('Filter started')
        try:
            grouped = df.copy()
            if Filter is None:
                FilteredData = grouped
            else:
                KeysName = []
                grouped = grouped.groupby(Filter)
                # if DataToClean is an empty Group
                checkIfNotEmpty = (list(grouped))
                if not checkIfNotEmpty:
                    print(Fore.RED + 'DataToClean after group by is an empty DataFrame!!' + Fore.RESET)
                    Log.AddToLog('DataToClean after group by is a  empty DataFrame!!')
                    return False
                Log.AddToLog('Data Been split to the following groups: {}'.format(grouped.groups.keys()))
                print('Data Been split to the following groups: {}'.format(grouped.groups.keys()))
                FilteredData = grouped
            return FilteredData
        except Exception as e:
            Log.AddToLog("exception in Filter :  {}".format(e), 2)
            return False





    ####################################################################################################################
    '''Main Functions for data preprocessing'''

    ####################################################################################################################
    def CleanData(self, DfToClean, CleaningThreshold):
        Log.AddToLog('CleanData started for group : {} '.format(DfToClean[self.FilterBy].values[0]))
        try:
            # File Columns:
            '''
            TimeStamp, EyeTime, RawX, RawY, AverageX, AverageY, Left_RawX, Left_RawY, Left_PupilSize, Left_PupilCenterX, \
            Left_PupilCenterY, Left_PupilCenterZ, Right_RawX, Right_RawY, Right_PupilSize, Right_PupilCenterX, \
            Right_PupilCenterY, Right_PupilCenterZ, CorrectedX, CorrectedY, Note,
            '''
            # define columns that should be checked for outliers
            drop_names = ['RawX', 'RawY',
                          'Left_PupilSize', 'Right_PupilSize',
                          'Left_PupilCenterX', 'Left_PupilCenterY',
                          'Left_PupilCenterZ', 'Right_PupilCenterX',
                          'Right_PupilCenterY', 'Right_PupilCenterZ']
            # get initial data frame dimensions
            shape_initial = DfToClean.shape

            # Find 4 consecutive zeros in raw data and remove them from dataframe to avoid single eyetracking
            flagBool, DfToClean = self.get_consecutive_zeros(DfToClean, 4)
            if not flagBool:
                return False
            # remove rows with for zeros
            # Remove outliers in all columns of interest
            DfToClean = DfToClean[DfToClean[drop_names].apply(lambda x: self.RemoveOutlier(x)).all(axis=1)]
            # Remove invalid pyhysiological values
            # remove pupil size < 1mm und > 9mm for left and right pupil
            # return all rows where left pupil size is between 1 and 9
            DfToClean = DfToClean[(DfToClean.Left_PupilSize > 1) & (DfToClean.Left_PupilSize < 9)]
            DfToClean.assign(Clean=pd.Series(np.nan))
            DfToClean.assign(Comment=pd.Series(np.nan))

            if DfToClean.empty:
                Log.AddToLog('No valid left pupil size data left, DfToClean.empty')
                return False
            DfToClean = DfToClean[(DfToClean.Right_PupilSize > 1) & (DfToClean.Right_PupilSize < 9)]
            if DfToClean.empty:
                Log.AddToLog('No valid right pupil size data left, DfToClean.empty')
                return False
            # remove all rows with NAs
            DfToClean = DfToClean.dropna(subset=drop_names)
            Log.AddToLog('CleanData main is finished, entering  IsDataAboveThreshold')
            return (self.IsDataAboveThreshold(DfToClean, CleaningThreshold, shape_initial))

        except Exception as e:
            Log.AddToLog("exception in CleanData :  {}".format(e), 2)
            return False