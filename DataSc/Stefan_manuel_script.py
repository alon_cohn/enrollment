import os
import glob
import pandas as pd
import datetime
from DataSc.PIN_DATA import pinData
from DataSc import Log
from colorama import *



class PinRawpreprocessingMatrix():
    def __init__(self):
        #Todo: This only works locally
        self.PathDataCollection = 'C:\Qsync\Alon\Raw_Data_DataCollection/*.txt'
        #self.PathDataPrototypeMeta = 'C:/Users/Stefan/Documents/oculid/03_Technik/01_MachineLearning/02_Prototyp_v1/01_Data/PrototypeData/*.txt'
        #self.PathDataPrototype = 'C:/Users/Stefan/Documents/oculid/03_Technik/02_SoftwareDevelopment/oculid/LogFiles/'
        store_folder = datetime.datetime.now().strftime("%I%M%p_%B%d_%Y")
        store_newpath_top = os.path.join('C:\Qsync\Stefan', store_folder)
        try:
            os.makedirs(store_newpath_top)
        except:
            os.makedirs(store_newpath_top + '_v1')
        self.PathStorePreprocessing = store_newpath_top
        self.runner()

    # at the moment onnly run on the data from old data collection
    def runner(self):
        #read the data
        self.readAppendDataCollection('PinGroups')

        '''
        self.dfFeatureDataCollection =  self.doCaliPreprocessing(self.dfDataCollection)
        #self.dfFeatureDataCollection.to_csv(os.path.join(self.PathStorePreprocessing, 'DataCollectionCaliFeatureMatrix.csv'),index = False)
        self.readAppendDataPrototype()
        self.dfFeatureDataPrototype =  self.doCaliPreprocessing(self.dfDataCollectionPrototype)
        self.dfFeatureDataPrototype.to_csv(os.path.join(self.PathStorePreprocessing, 'PrototypeCaliFeatureMatrix.csv'),index = False)
        self.CompleteFeatureMatrix = pd.concat([self.dfFeatureDataCollection, self.dfFeatureDataPrototype], axis = 0)
        self.CompleteFeatureMatrix.to_csv(os.path.join(self.PathStorePreprocessing, 'CompleteCaliFeatureMatrix.csv'),index = False)
        '''

    # read all the files in the folder
    # for each file:
        #read the dile to pandas
        #reoreder it for fit to PIN_DATA
        #add the data to the matrix.
    def readAppendDataCollection(self, sequence = 'Calibration'):
        Log.AddToLog('readAppendDataCollection started ')
        try:
            '''Read all Files to a DataFrame'''
            # get paths for all files
            filenames = glob.glob(self.PathDataCollection)
            # create empty DataFrame
            self.dfDataCollection = pd.DataFrame()
            for file in filenames:
                Log.AddToLog('Entering read file loop in readAppendDataCollection')
                try:
                    print(file)
                    dfTemp = pd.read_csv(file, low_memory=False)
                    self.reorder_df(dfTemp)
                    print(self.dfDataCollection.shape)
                    self.doCaliPreprocessing(self.dfDataCollection)
                except Exception as e:
                    print('Exception {} in File: {}\n could not be read'.format(e, file))
                    Log.AddToLog('Exception {} in for loop for File: {}\n could not be read'.format(e, file))
                    pass
            #self.dfDataCollection.to_csv(os.path.join(self.PathStorePreprocessing, 'dfDataCollectionCaliRaw.csv'),index = False)
        except Exception as e:
            Log.AddToLog("Exception in readAppendDataCollection :  {}".format(e), 2)
            print(Fore.RED + 'Exception in readAppendDataCollection. See Log' + Fore.RESET)

    ''' Preprocessing Function '''
    def doCaliPreprocessing(self, dfRaw):
        Log.AddToLog('doCaliPreprocessing started')
        try:
            Log.AddToLog('Entering read file loop in readAppendDataPrototype')
            dfFeatures = pd.DataFrame()
            for key, df in dfRaw.groupby(['ID', 'MoveID']):
                print(key)
                p = pinData(df, 'SMI')
                if p.runner():
                    tempFeature = (p.CleanedAddCalcData)
                    tempFeature = tempFeature.assign(ID=key[0])
                    tempFeature = tempFeature.assign(MoveID=key[1])
                    dfFeatures = pd.concat([dfFeatures, tempFeature], axis=0)
                else:
                    print('Data Frame could not be processed')
                    continue
            with open(self.PathStorePreprocessing+'\RawAfterCalc_'+key[0]+'.csv', 'a') as f:
                dfFeatures.to_csv(f,index=False)
        except Exception as e:
            Log.AddToLog("Exception in doCaliPreprocessing:  {}".format(e), 2)
            print(Fore.RED + 'Exception in doCaliPreprocessing. See Log' + Fore.RESET)

    def reorder_df(self, dfTemp):
        Log.AddToLog('reorder_df started ')
        try:
            dfTemp1 = dfTemp[dfTemp.Note == 'Movement 1']
            dfTemp2 = dfTemp[dfTemp.Note == 'Movement 2']
            self.dfDataCollection = pd.DataFrame()
            self.dfDataCollection = pd.concat([self.dfDataCollection, dfTemp1], axis=0)
            self.dfDataCollection = pd.concat([self.dfDataCollection, dfTemp2], axis=0)
            self.dfDataCollection.rename(columns={'TargetPosX': 'TargetX', 'TargetPosY': 'TargetY'}, inplace=True)
        except Exception as e:
            Log.AddToLog("Exception in reorder_df :  {}".format(e), 2)

