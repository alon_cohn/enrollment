# Define Split function 
# Split data according to markers in prototype 1

# Input: raw data frame
# Output: sliced data frames for all relevant sequences
# Todo: implement quick validity checks 


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itertools
from sklearn.metrics import mean_squared_error
from statsmodels.tools.eval_measures import rmse
from scipy.stats import linregress
from scipy import stats
from math import sqrt

'''
The purpose of this function is:

    1. Take a data frame that contains all interaction sequences 
    (Calibration, Movements etc.) and split it into 9 single dataframes. One
    for each interaction sequence.
    (dfcal_lum, dfcal_pre_bl, dfcal_post_bl, dfpin_mov1, dfpin_trans,
    dfpin_mov2, dfpin_calc, dfpin_return, dfpin_wait)
    2. This function is mainly used to work with training data (offline)
    
'''
def split_data_frame(dfall):
    
    # Data from calibration
    ## slice data: calibration all
    dfcal = dfall[dfall['Marker']=='Calibration'] # Calibration data frame (all)
    ## slice data: calibration - Luminance change
    dfcal_lum = dfcal[dfcal['Note']=='Brightness Change']
    ## Minimize data frame: include all relevant parameters
    
    
    dfcal_lum = dfcal_lum[['ID',
                           'MoveID', 
                           'Trial',
                           'Note',
                           'Left_PupilSize', 
                           'Right_PupilSize',
                           'Left_PupilCenterX',
                           'Left_PupilCenterY', 
                           'Left_PupilCenterZ',
                           'Right_PupilCenterX',
                           'Right_PupilCenterY', 
                           'Right_PupilCenterZ',
                           'RawX', 
                           'RawY', 
                           'TimeStamp', 
                           'EyeTime']]
    
    
    ## slice data: calibration - Baseline
    dfcal_pre_bl=dfcal[dfcal['Note']=='Pre Baseline'] # Calibration data frame (pre Baseline)
    dfcal_post_bl=dfcal[dfcal['Note']=='Post Baseline'] # Calibration data frame (post Baseline)
    
    
    ## Minimize data frame: include all relevant parameters
    dfcal_pre_bl = dfcal_pre_bl[['ID',
                                 'MoveID', 
                                 'Trial',
                                 'Note', 
                                 'Left_PupilSize', 
                                 'Right_PupilSize',
                                 'Left_PupilCenterX',
                                 'Left_PupilCenterY', 
                                 'Left_PupilCenterZ',
                                 'Right_PupilCenterX',
                                 'Right_PupilCenterY', 
                                 'Right_PupilCenterZ',
                                 'RawX', 
                                 'RawY', 
                                 'TimeStamp', 
                                 'EyeTime']]

    dfcal_post_bl = dfcal_post_bl[['ID',
                                   'MoveID', 
                                   'Trial',
                                   'Note', 
                                   'Left_PupilSize', 
                                   'Right_PupilSize',
                                   'Left_PupilCenterX',
                                   'Left_PupilCenterY', 
                                   'Left_PupilCenterZ',
                                   'Right_PupilCenterX',
                                   'Right_PupilCenterY', 
                                   'Right_PupilCenterZ',
                                   'RawX', 
                                   'RawY', 
                                   'TimeStamp', 
                                   'EyeTime']]
    
    # Data from Pin Groups
    ## slice data: pin groups all 
    dfpin=dfall[dfall['Marker']=='PinGroups'] # Pin Group Data frame
    
    
    # remove all rows where Note marker is NaN
    dfpin=dfpin[dfpin.Note.isnull()==False]
    
    ## slice data: pin groups movement 1
    dfpin_mov1=dfpin[dfpin['Note']=='Movement 1']
    ## slice data: pin groups movement 2
    dfpin_mov2=dfpin[dfpin['Note']=='Movement 2']
    
    # BugFix - Quick Fix movement 2 issue 15.06.2017
    # Anpassen wenn "Bug behoben"
    # subset of detection
    dfpin_mov2_detect=dfpin_mov2.dropna(subset=['Detected_Number'])
    # remove detected number part
    dfpin_mov2=dfpin_mov2[dfpin_mov2.Detected_Number.isnull()]
    # remove part where target number is present but no movement
    dfpin_mov2=dfpin_mov2.dropna(subset=['TargetPosX','TargetPosY'])
    
    ## Minimize data frame: include all relevant parameters
    dfpin_mov1 = dfpin_mov1[['ID',
                         'MoveID', 
                         'Trial',
                         'Note',
                         'Target_Number',
                         'Left_PupilSize', 
                         'Right_PupilSize', 
                         'Left_PupilCenterX',
                         'Left_PupilCenterY', 
                         'Left_PupilCenterZ',
                         'Right_PupilCenterX',
                         'Right_PupilCenterY', 
                         'Right_PupilCenterZ',
                         'CorrectedX',
                         'CorrectedY', 
                         'TargetPosX', 
                         'TargetPosY',
                         'RawX', 
                         'RawY', 
                         'TimeStamp', 
                         'EyeTime']]

    dfpin_mov2 = dfpin_mov2[['ID',
                             'MoveID', 
                             'Trial',
                             'Note',
                             #'Detected_Number',
                             'Target_Number',
                             'Left_PupilSize', 
                             'Right_PupilSize', 
                             'Left_PupilCenterX',
                             'Left_PupilCenterY', 
                             'Left_PupilCenterZ',
                             'Right_PupilCenterX',
                             'Right_PupilCenterY', 
                             'Right_PupilCenterZ',
                             'CorrectedX',
                             'CorrectedY', 
                             'TargetPosX', 
                             'TargetPosY',
                             'RawX', 
                             'RawY', 
                             'TimeStamp', 
                             'EyeTime']]

    
    
    #TODO - scharf machen
    ## slice data: Transition between movement 1 and 2
    dfpin_trans=dfpin[dfpin['Note']=='Transition']
    
    dfpin_trans = dfpin_trans[['ID',
                             'MoveID', 
                             'Trial',
                             'Note',
                             #'Detected_Number',
                             'Target_Number',
                             'Left_PupilSize', 
                             'Right_PupilSize', 
                             'Left_PupilCenterX',
                             'Left_PupilCenterY', 
                             'Left_PupilCenterZ',
                             'Right_PupilCenterX',
                             'Right_PupilCenterY', 
                             'Right_PupilCenterZ',
                             'CorrectedX',
                             'CorrectedY', 
                             'TargetPosX', 
                             'TargetPosY',
                             'RawX', 
                             'RawY', 
                             'TimeStamp', 
                             'EyeTime']]
    
    ## slice data: pin groups calculation after movement 2 end
    dfpin_calc=dfpin[dfpin['Note']=='FinalCalculation']
    
    dfpin_calc = dfpin_calc[['ID',
                             'MoveID', 
                             'Trial',
                             'Note',
                             #'Detected_Number',
                             'Target_Number',
                             'Left_PupilSize', 
                             'Right_PupilSize', 
                             'Left_PupilCenterX',
                             'Left_PupilCenterY', 
                             'Left_PupilCenterZ',
                             'Right_PupilCenterX',
                             'Right_PupilCenterY', 
                             'Right_PupilCenterZ',
                             'CorrectedX',
                             'CorrectedY', 
                             'TargetPosX', 
                             'TargetPosY',
                             'RawX', 
                             'RawY', 
                             'TimeStamp', 
                             'EyeTime']]
    
    
    ## slice data: Return of the Numbers back to the center
    dfpin_return=dfpin[dfpin['Note']=='ReturnPhase']
    
    dfpin_return = dfpin_return[['ID',
                             'MoveID', 
                             'Trial',
                             'Note',
                             #'Detected_Number',
                             'Target_Number',
                             'Left_PupilSize', 
                             'Right_PupilSize', 
                             'Left_PupilCenterX',
                             'Left_PupilCenterY', 
                             'Left_PupilCenterZ',
                             'Right_PupilCenterX',
                             'Right_PupilCenterY', 
                             'Right_PupilCenterZ',
                             'CorrectedX',
                             'CorrectedY', 
                             'TargetPosX', 
                             'TargetPosY',
                             'RawX', 
                             'RawY', 
                             'TimeStamp', 
                             'EyeTime']]
    
    ## slice data: Wait for the return of the eyes to the center 
    dfpin_wait=dfpin[dfpin['Note']=='Wait for Return']

    dfpin_wait = dfpin_wait[['ID',
                             'MoveID', 
                             'Trial',
                             'Note',
                             #'Detected_Number',
                             'Target_Number',
                             'Left_PupilSize', 
                             'Right_PupilSize', 
                             'Left_PupilCenterX',
                             'Left_PupilCenterY', 
                             'Left_PupilCenterZ',
                             'Right_PupilCenterX',
                             'Right_PupilCenterY', 
                             'Right_PupilCenterZ',
                             'CorrectedX',
                             'CorrectedY', 
                             'TargetPosX', 
                             'TargetPosY',
                             'RawX', 
                             'RawY', 
                             'TimeStamp', 
                             'EyeTime']]
    
    
    #return sliced data frames
    return dfcal_lum, dfcal_pre_bl, dfcal_post_bl, dfpin_mov1, dfpin_trans ,dfpin_mov2, dfpin_calc, dfpin_return, dfpin_wait



# Define clean data function

# Input: sliced data frames, data loss factor default .3 (30%)
# Output: clean sliced data frame
# Todo: implement quick validity checks and write a datalog 
# to see how many data per ID and MoveID is lost

'''
The purpose of this function is:

    1. Check the eye tracking data quality and clean it for all interaction 
    sequences
    2. Remove outliers
    3. Remove 'impossible' values with respect to physiological aspects
    
'''
def clean_data_frame(dftoclean, thresh=.7, printout=True):
    
    df = dftoclean.copy()
    print('ID:', df.ID.iloc[0])
    #df_log = pd.DataFrame(columns=['ID','MoveID', 'Trial', 'Note', 'Clean', 'Error', 'Warning'])
    #df_log = df_log.append([df.MoveID.iloc[1],])
    
    #df_log.to_csv( os.path.join(log_path, 'Log_Data.csv'), header=True, index=None, sep='', mode='a')
    ###################################################
    # Check initial number of samples for each MoveID #
    ###################################################
    
    ## get initial data frame dimensions
    shape_initial=df.shape
    # create 2 new columns filled with nan to later store whether
    # single frames passed the conditions
    df.assign(Clean = pd.Series(np.nan))
    df.assign(Comment = pd.Series(np.nan))
    

    ## print information about the sequence 
    #print('ID:', df.ID.iloc[0])
    #print('MoveID:', df.MoveID.iloc[0])
    #print('Trial:', df.Trial.iloc[0])
    #print('Note:', df.Note.iloc[0])
    #print('Initial number of rows',shape_initial[0], '\n')
    
    ## check for data integrity, remove +/- 10 Samples
    
    if df.Note.iloc[0]=='Brightness Change':
        min_samp = 100
        max_samp = 250
        #min_samp = 110
        #max_samp = 130
        drop_names = ['RawX','RawY',
                      'Left_PupilSize','Right_PupilSize',
                      'Left_PupilCenterX','Left_PupilCenterY', 
                      'Left_PupilCenterZ','Right_PupilCenterX',
                      'Right_PupilCenterY','Right_PupilCenterZ',
                      'EyeTime', 'TimeStamp']
    elif df.Note.iloc[0]=='Pre Baseline':
        min_samp = 100
        max_samp = 250
        #min_samp = 110
        #max_samp = 130
        drop_names = ['RawX', 'RawY', 
                     'Left_PupilSize', 'Right_PupilSize',
                     'Left_PupilCenterX','Left_PupilCenterY', 
                     'Left_PupilCenterZ','Right_PupilCenterX',
                     'Right_PupilCenterY','Right_PupilCenterZ',
                     'TimeStamp', 'EyeTime']

    elif df.Note.iloc[0]=='Post Baseline':
        min_samp = 20
        max_samp = 70
        
        #min_samp = 20 
        #max_samp = 40
        drop_names = ['RawX', 'RawY', 
                     'Left_PupilSize', 'Right_PupilSize',
                     'Left_PupilCenterX','Left_PupilCenterY', 
                     'Left_PupilCenterZ','Right_PupilCenterX',
                     'Right_PupilCenterY','Right_PupilCenterZ',
                     'TimeStamp', 'EyeTime']
    
    elif df.Note.iloc[0]=='Movement 1':
        min_samp = 70
        max_samp = 90
        drop_names = ['Left_PupilSize', 'Right_PupilSize', 
                      'Left_PupilCenterX','Left_PupilCenterY', 
                      'Left_PupilCenterZ','Right_PupilCenterX',
                      'Right_PupilCenterY','Right_PupilCenterZ',
                      'CorrectedX','CorrectedY', 
                      'TargetPosX', 'TargetPosY',
                      'RawX', 'RawY', 
                      'TimeStamp','EyeTime']
        
    elif df.Note.iloc[0]=='Transition':
        min_samp = 2
        max_samp = 15
        drop_names = ['Left_PupilSize', 'Right_PupilSize', 
                      'Left_PupilCenterX','Left_PupilCenterY', 
                      'Left_PupilCenterZ','Right_PupilCenterX',
                      'Right_PupilCenterY','Right_PupilCenterZ',
                      'CorrectedX','CorrectedY',
                      'RawX', 'RawY', 
                      'TimeStamp','EyeTime']
    
    elif df.Note.iloc[0]=='Movement 2':
        min_samp = 50
        max_samp = 70
        drop_names = ['Left_PupilSize', 'Right_PupilSize', 
                      'Left_PupilCenterX','Left_PupilCenterY', 
                      'Left_PupilCenterZ','Right_PupilCenterX',
                      'Right_PupilCenterY','Right_PupilCenterZ',
                      'CorrectedX','CorrectedY', 
                      'TargetPosX', 'TargetPosY',
                      'RawX', 'RawY', 
                      'TimeStamp','EyeTime']
        
    elif df.Note.iloc[0]=='FinalCalculation':
        #print('Final calculation section, Implement cleaning!?', df.Note.iloc[1])
        #df['Clean'] = True
        #return df
        
        min_samp = 30
        max_samp = 50
        drop_names = ['Left_PupilSize', 'Right_PupilSize', 
                      'Left_PupilCenterX','Left_PupilCenterY', 
                      'Left_PupilCenterZ','Right_PupilCenterX',
                      'Right_PupilCenterY','Right_PupilCenterZ',
                      'CorrectedX','CorrectedY', 
                      'TargetPosX', 'TargetPosY', # constant values
                      'RawX', 'RawY', 
                      'TimeStamp','EyeTime']
    
    elif df.Note.iloc[0]=='ReturnPhase':
        #print('Return target section, Implement cleaning!?', df.Note.iloc[1])
        #df['Clean'] = True
        #return df
        
        min_samp = 5
        max_samp = 10
        drop_names = ['Left_PupilSize', 'Right_PupilSize', 
                      'Left_PupilCenterX','Left_PupilCenterY', 
                      'Left_PupilCenterZ','Right_PupilCenterX',
                      'Right_PupilCenterY','Right_PupilCenterZ',
                      'CorrectedX','CorrectedY', 
                      'TargetPosX', 'TargetPosY',
                      'RawX', 'RawY', 
                      'TimeStamp','EyeTime']
    
    elif df.Note.iloc[0]=='Wait for Return':
        
        print('Wait for eye return section, Implement cleaning!?', df.Note.iloc[1])
        df['Clean'] = True
        return df
        #min_samp = 0
        #max_samp = 60
        #drop_names = ['Left_PupilSize', 'Right_PupilSize', 
        #              'Left_PupilCenterX','Left_PupilCenterY', 
        #              'Left_PupilCenterZ','Right_PupilCenterX',
        #              'Right_PupilCenterY','Right_PupilCenterZ',
        #              'CorrectedX','CorrectedY', 
        #              'TargetPosX', 'TargetPosY',
        #              'RawX', 'RawY', 
        #              'TimeStamp','EyeTime']
    else:
        print('Note marker not found', df.Note.iloc[0])
        df['Clean'] = False
        df['Comment'] = 'No Note marker Found'
        return df
    
    ###################################################
    # Check if initial number of samples is correct   #
    ###################################################
    
    
    if shape_initial[0] < min_samp:
        #TODO: explain over ID which DAOI betroffen ist 
        print('Number of samples: too small', shape_initial[0])
        df['Clean'] = False
        df['Comment'] = 'Initial number of samples: too small'
        return df
    elif shape_initial[0] > max_samp:
        print('Number of samples: too large',shape_initial[0])
        df['Clean'] = False
        df['Comment'] = 'Initial number of samples: too large'
        return df
    else:
        df['Clean'] = True
        df['Comment'] = 'Initial number of samples: match'
        print('number of samples correct')
    
        ###################################################
        # Calculate meaningful                 timestamps #
        ###################################################
        
        ## in seconds 
        df.TimeStamp = df.TimeStamp.diff().fillna(0.).cumsum()
        # in milliseconds
        df.EyeTime = df.EyeTime.diff().fillna(0.).cumsum()
        
        ## Print Duration for each MoveID
        #print('Duration (TimeStamp) in s:', df.TimeStamp.diff().sum())
       # print('Duration (EyeTime) in ms:', df.EyeTime.diff().sum())
    
        ###################################################
        # Drop NaN Rows                                   #
        ###################################################
        # TODO outlier machen bei Timestamps keinen Sinn bzw. sind gefährlich
        
        # TODO hier kann man ev. noch einen Interpolation step einbauen aber
        # vielleicht limitieren wieviele NAs übersprungen werden dürfen
        # .interpolate
        
        #df = df.interpolate()
        
        
        
        
        ###################################################
        # Remove Outliers                                 #
        ###################################################
        # remove outliers for dropna columns add others if needed
        # mean +/- 3 std
        def remove_outlier(x):
            if x.std() == 0:
                logvec= [True] * len(x)
            else:
                logvec=np.abs(x - x.mean()) / x.std() < 3
            return logvec
        
        # TODO drop names stimmt wahrscheinlich nicht sondern subset davon: Also wo macht es sinn
        df = df[df[drop_names].apply(lambda x: remove_outlier(x)).all(axis=1)]    
        
        ###################################################
        # Pupil Size Cleaning                             #
        ###################################################
        # remove pupil size < 1mm und > 9mm for left and right pupil 
        # TODO: Wirklich in mm???
        df = df[(df.Left_PupilSize > 1) & (df.Left_PupilSize < 9)]
        df = df[(df.Right_PupilSize > 1) & (df.Right_PupilSize < 9)]
        
        
        ###################################################
        # Check if NAs introduced: Warning                #
        ###################################################
        if df.isnull().values.any():
            print('Warning: Calculations introduced NA values')        
            #comment.append=('Warning: still NaN in data') 
        # other stuff, calculations etc. e.g. calculate meaningful times diff.cumsum()
        
        
        ###################################################
        # actually remove all rows with NAs
        ###################################################
        
        df = df.dropna(subset=drop_names)
        
        if df.isnull().values.any():
            print('Something went wrong: There is still NAs in the data')
            df['Clean'] = False
            df['Comment'] = 'Still NAs in Data after NA removal' 
            return df
        
        ###################################################
        # Check for data loss                             #
        ###################################################
        ## get final df shape
        shape_final=df.shape
        #print('final number of rows',shape_final[0])
        
        if shape_final[0] < thresh*shape_initial[0]: 
            print('Warning: Cleaning the data caused more than {} % data loss.\n\
            Trials have to be removed from data frame'.format(100 - (thresh*100)))
            df['Clean'] = False
            df['Comment'] = 'Final number of samples: too small' 
            return df
        
        #TODO: check for leading Zeroes in vectors, most often the beginningis the most important stuff
        
        # If here, then everything i.O.
        #df['Clean']=True
        
        ## Print Duration for each MoveID
        #print('Duration (TimeStamp) in s:', df.TimeStamp.diff().sum())
        #print('Duration (EyeTime) in ms:', df.EyeTime.diff().sum())
        #print('\n', df.ID.iloc[0],'\n')
        # return data frame
        return df
#
###############################################################################
# Define Function for the calculation of additional parameters

# Input: sliced clean data frames
# Output: clean sliced data frame with additional columns
# Todo: implement quick validity checks 
###############################################################################
'''
The purpose of this function is:

    1. Calculate additional parameters out of the raw data and add them to the 
    data frame for all interaction sequences
    
    
'''

def calc_add_params(df):
    from scipy.signal import savgol_filter
    ###################################
    # Calculations for all sequences
    ###################################
    # TODO hier muss aufgräumt werden und gecheckt werden was überhaupt nötig ist
    
    # Pupil Slopes Left and Right Eye
    slope_left = pd.Series(np.gradient(df.Left_PupilSize), 
                           df.TimeStamp, name='slope')
    slope_right = pd.Series(np.gradient(df.Right_PupilSize), 
                            df.TimeStamp, name='slope')
    
    df = df.assign(Pupil_Slope_Right=slope_right.values)
    df = df.assign(Pupil_Slope_Left=slope_left.values)
    
    # rollmeans left centred Pupil Left and Right Eye
    w=10
    s=w-1
    roll_mean_left = pd.Series(df.Left_PupilSize.rolling(window=w,axis=0).mean().shift(-s))
    roll_mean_right = pd.Series(df.Right_PupilSize.rolling(window=w,axis=0).mean().shift((-s)))
    df = df.assign(Pupil_Rollmean_Right=roll_mean_right.values)
    df = df.assign(Pupil_Rollmean_Left=roll_mean_left.values)
    
    
    # Pupil Slopes Left and Right Eye over Rollmean pupil values
    slope_rollmean_left = pd.Series(np.gradient(df.Pupil_Rollmean_Left), 
                                    df.TimeStamp, name='slope')
    slope_rollmean_right = pd.Series(np.gradient(df.Pupil_Rollmean_Right), 
                                     df.TimeStamp, name='slope')
    
    df = df.assign(Pupil_Slope_Rollmean_Right=slope_rollmean_right.values)
    df = df.assign(Pupil_Slope_Rollmean_Left=slope_rollmean_left.values)
    
    # calculate 2D pupil distance
    pupdist2D=np.sqrt(((df.Left_PupilCenterX - df.Right_PupilCenterX)**2)+
             ((df.Left_PupilCenterY - df.Right_PupilCenterY)**2))

    df = df.assign(Pupil_Distance_2D=pupdist2D)


    # calculate 3D pupil distance
    pupdist3D=np.sqrt(((df.Left_PupilCenterX - df.Right_PupilCenterX)**2)+
             ((df.Left_PupilCenterY - df.Right_PupilCenterY)**2)+
               ((df.Left_PupilCenterZ - df.Right_PupilCenterZ)**2))

    df = df.assign(Pupil_Distance_3D=pupdist3D)
    
    
    #
    
    ######################################
    # Calculations for Calibration
    ######################################
    
    
    ######################################
    # Calculations for Pin Group Movements
    ######################################
    
    if (df.Note.iloc[1]=='Movement 1' or df.Note.iloc[1]=='Movement 2'):
    
    
        # according to Ettinger 2003, filter XY Data wit (5) Point central average filter
        rawx_roll = pd.Series(df.RawX.rolling(window=w,axis=0).mean().shift(-s))
        rawy_roll = pd.Series(df.RawY.rolling(window=w,axis=0).mean().shift(-s))
        df = df.assign(RawX_roll = rawx_roll)
        df = df.assign(RawY_roll = rawy_roll)
        #plt.close()
        #plt.plot(df.RawX, df.RawY, 'blue')
        #plt.plot(df.RawX_roll, df.RawY_roll, 'red')

        # ettinger 2003: Detection of saccades during pursuit was based on 
        # criteria of minimum amplitude (1.5 degree) and velocity (30degrees/s)
        
        # calculate target eye distance
        #1 function
        posdist=np.sqrt(((df.RawX - df.TargetPosX)**2)+
                 ((df.RawY - df.TargetPosY)**2))

        # calculate step by step difference of Target to Eye 
        posdistdiff=posdist.diff()
        
        # calculate cumulated step by step difference of Target to Eye 
        posdistdiffcum=posdistdiff.cumsum()

        df = df.assign(Target_Eye_Distance=posdist)
        df = df.assign(Target_Eye_Distance_Diff=posdistdiff)
        df = df.assign(Target_Eye_Distance_CumDiff=posdistdiffcum)


        # Calculate x and y velocities of target and eye movement
        # Calculate pursuit gain in x and y direction and then overall ()
        # Hier geht der Spass jetzt richtig los! in der Eye velocity stecken saccaden drin die gefiltert werden müssen
        # Hier müssen wir noch mal in die Paper schauen wie man das machen kann auch bei 120Hz
        x_velo_target=df.TargetPosX.diff()/df.TimeStamp.diff()
        y_velo_target=df.TargetPosY.diff()/df.TimeStamp.diff()

        velo_target=np.sqrt(x_velo_target**2 + y_velo_target**2)
        #roll mean
        velo_target_roll=pd.Series(velo_target.rolling(window=w,axis=0).mean().shift(-s))
       
        
        df = df.assign(Target_velocity_px_s = velo_target)
        df = df.assign(Target_velocity_roll_px_s = velo_target_roll.values)
        

        x_velo_eye=df.RawX.diff()/df.TimeStamp.diff()
        y_velo_eye=df.RawY.diff()/df.TimeStamp.diff()

        velo_eye=np.sqrt(x_velo_eye**2 + y_velo_eye**2)
        velo_eye_roll=pd.Series(velo_eye.rolling(window=w,axis=0).mean().shift(-s))
        # filter FIR savgol filter nach nyström and holmqvist 2010
        
        min_saccade_dur_sample = 11 #(5 - 42ms, 3 - 25ms)
        velo_eye_savgol = savgol_filter(velo_eye, window_length = min_saccade_dur_sample, 
                                      polyorder = 3)

        
        
        df = df.assign(Eye_velocity_px_s = velo_eye)
        df = df.assign(Eye_velocity_roll_px_s = velo_eye_roll.values)
        df = df.assign(Eye_velocity_savgol_px_s = velo_eye_savgol)

        p_gain = (velo_eye/velo_target)
        p_gain_roll = (velo_eye_roll/velo_target_roll)
        p_gain_savgol = (velo_eye_savgol/velo_target)
        #p_gain = np.abs(p_gain - p_gain.mean()) / p_gain.std() < 3
        df = df.assign(Pursuit_Gain = p_gain)
        df = df.assign(Pursuit_Gain_Roll = p_gain_roll)
        df = df.assign(Pursuit_Gain_Savgol = p_gain_savgol)

    return df

'''
The purpose of this function is:

    1. Aggreagte/Calculate biometric features out of the raw data for all 
    interaction sequences 
    
    
'''


# Purpose of this function is ...
def calc_feature(df):
    #calculate features for the machine learning part
    print('debug:', df.ID.iloc[1])
    # Calculate standard measures by first definining a parameter list for
    # the different sections
    if (df.Note.iloc[1] == 'Brightness Change'):
        
        # calculate means, stds, median, min, max of all relevant features
        param_list = ['Left_PupilSize','Right_PupilSize',
                          'Pupil_Rollmean_Right','Pupil_Rollmean_Left',
                          'Pupil_Slope_Rollmean_Right',
                          'Pupil_Slope_Rollmean_Left', 'Pupil_Distance_2D',
                          'Pupil_Distance_3D', 'RawX', 'RawY']
        # calculate additional parameters
        # slope of linear regression pupil change over time
        from scipy.stats import linregress
        #linear regression slopes raw data
        lreg_leftpupil = linregress(df.TimeStamp, df.Left_PupilSize)
        lreg_rightpupil = linregress(df.TimeStamp, df.Right_PupilSize)
        lreg_leftpupil_slope = lreg_leftpupil.slope
        lreg_rightpupil_slope = lreg_rightpupil.slope
        
        #linear regression slopes roll mean
        # hier müssen noch die NAs bereinigt werden
        Left_Clean = df[['TimeStamp', 'Pupil_Rollmean_Left']].dropna()
        Right_Clean = df[['TimeStamp','Pupil_Rollmean_Right']].dropna()
        
        
        lreg_leftpupil = linregress(Left_Clean)
        lreg_rightpupil = linregress(Right_Clean)
        lreg_leftpupil_roll_slope = lreg_leftpupil.slope
        lreg_rightpupil_roll_slope = lreg_rightpupil.slope
        
        
        # Correlation between left and Right Pupil Size
        lr_corr_pupilsize = np.corrcoef(df.Left_PupilSize.values, 
                                        df.Right_PupilSize.values)[0][1]
        # Correlation between left and Right Pupil Size roll means
        lr_corr_pupilsize_roll = np.corrcoef(Left_Clean.Pupil_Rollmean_Left.values, 
                                        Right_Clean.Pupil_Rollmean_Right.values)[0][1]
        
        # Fläche unter der Pupillen veränderungen
        sum_leftpupil = df.Left_PupilSize.sum()
        sum_rightpupil = df.Right_PupilSize.sum()
        sum_rollmean_leftpupil = df.Pupil_Rollmean_Left.sum()
        sum_rollmean_rightpupil = df.Pupil_Rollmean_Right.sum()
        
        #calculate bivariate contour elipse Area as measure of fixation stability nach vikesdal2016
        # (makes most sense in the brightness change condition)
        # 1. calc standard deviation of RawY and RawY (dont save as it is calculated anyways later)
        std_RawX = np.std(df.RawX)
        std_RawY = np.std(df.RawY)
        # 2. BCEA
        k_68 = 2.28  
        k_95 = 5.98
        # (product-moment correlation of the two position components)
        # product_moment_correlation 
        pmc = np.corrcoef(df.RawX, df.RawY)[1,0]
        BCEA_68 = k_68 * np.pi * (std_RawX*std_RawY)* np.sqrt(1-np.power(pmc,2))
        BCEA_95 = k_95 * np.pi * (std_RawX*std_RawY)* np.sqrt(1-np.power(pmc,2))
        logBCEA_68 = np.log(BCEA_68) 
        logBCEA_95 = np.log(BCEA_95)
        
        
        
        # Left Pupil center estimation based on gaussian density kernel center
        xmin_left = df.Left_PupilCenterX.min()
        xmax_left = df.Left_PupilCenterX.max()
        ymin_left = df.Left_PupilCenterY.min()
        ymax_left = df.Left_PupilCenterY.max()
        
        X_left, Y_left = np.mgrid[xmin_left:xmax_left:100j, ymin_left:ymax_left:100j]
        positions_left = np.vstack([X_left.ravel(), Y_left.ravel()])
        values_left = np.vstack([df.Left_PupilCenterX, df.Left_PupilCenterY])
        kernel_left = stats.gaussian_kde(values_left)
        #Z = np.reshape(kernel(positions).T, X.shape)
        
        k_pos_left = kernel_left(positions_left)
        maxdense_pos_left = positions_left.T[np.argmax(k_pos_left)]

        # Right Pupil center estimation based on gaussian density kernel center
        xmin_right = df.Right_PupilCenterX.min()
        xmax_right = df.Right_PupilCenterX.max()
        ymin_right = df.Right_PupilCenterY.min()
        ymax_right = df.Right_PupilCenterY.max()
        
        X_right, Y_right = np.mgrid[xmin_right:xmax_right:100j, ymin_right:ymax_right:100j]
        positions_right = np.vstack([X_right.ravel(), Y_right.ravel()])
        values_right = np.vstack([df.Right_PupilCenterX, df.Right_PupilCenterY])
        kernel_right = stats.gaussian_kde(values_right)
        #Z = np.reshape(kernel(positions).T, X.shape)
        
        k_pos_right = kernel_right(positions_right)
        maxdense_pos_right = positions_right.T[np.argmax(k_pos_right)]

        maxdense_dist_2D = np.sqrt((maxdense_pos_right[0]-maxdense_pos_left[0])**2 + 
                            (maxdense_pos_right[1]-maxdense_pos_left[1])**2)
        
        
        # create series to later concatenate to other parameters
        parameter_add_names = ['Reg_slope_Left_PupilSize',
                               'Reg_slope_Right_PupilSize',
                               'Reg_slope_Pupil_Rollmean_Left',
                               'Reg_slope_Pupil_Rollmean_Right',
                               'Sum_Left_PupilSize',
                               'Sum_Right_PupilSize',
                               'Sum_Pupil_Rollmean_Left',
                               'Sum_Pupil_Rollmean_Right',
                               'Corr_LR_Pupilsize',
                               'Corr_LR_Pupilsize_Roll',
                               'BCEA_68', 'BCEA_95',
                               'logBCEA_68', 'logBCEA_95',
                               'Max_Dense_PupilDist_2D'] 
        
        # create series to later concatenate to other parameters
        parameter_add_values = pd.Series([lreg_leftpupil_slope, 
                                          lreg_rightpupil_slope,
                                          lreg_leftpupil_roll_slope,
                                          lreg_rightpupil_roll_slope,
                                          sum_leftpupil, sum_rightpupil,
                                          sum_rollmean_leftpupil,
                                          sum_rollmean_rightpupil,
                                          lr_corr_pupilsize,
                                          lr_corr_pupilsize_roll,
                                          BCEA_68, BCEA_95,
                                          logBCEA_68, logBCEA_95,
                                          maxdense_dist_2D])
        
    elif (df.Note.iloc[1]=='Pre Baseline'):
        
        # calculate means, stds, median, min, max of all relevant features
        param_list = ['Left_PupilSize','Right_PupilSize',
                          'Pupil_Rollmean_Right','Pupil_Rollmean_Left',
                          'Pupil_Slope_Rollmean_Right',
                          'Pupil_Slope_Rollmean_Left', 'Pupil_Distance_2D',
                          'Pupil_Distance_3D', 'RawX', 'RawY']
        # calculate additional parameters
        # slope of linear regression pupil change over time
        from scipy.stats import linregress
        #linear regression slopes raw data
        lreg_leftpupil = linregress(df.TimeStamp, df.Left_PupilSize)
        lreg_rightpupil = linregress(df.TimeStamp, df.Right_PupilSize)
        lreg_leftpupil_slope = lreg_leftpupil.slope
        lreg_rightpupil_slope = lreg_rightpupil.slope
        
        #linear regression slopes roll mean
        # hier müssen noch die NAs bereinigt werden
        Left_Clean = df[['TimeStamp', 'Pupil_Rollmean_Left']].dropna()
        Right_Clean = df[['TimeStamp','Pupil_Rollmean_Right']].dropna()
        
        
        lreg_leftpupil = linregress(Left_Clean)
        lreg_rightpupil = linregress(Right_Clean)
        lreg_leftpupil_roll_slope = lreg_leftpupil.slope
        lreg_rightpupil_roll_slope = lreg_rightpupil.slope
        
        
        # Correlation between left and Right Pupil Size
        lr_corr_pupilsize = np.corrcoef(df.Left_PupilSize.values, 
                                        df.Right_PupilSize.values)[0][1]
        # Correlation between left and Right Pupil Size roll means
        lr_corr_pupilsize_roll = np.corrcoef(df.Pupil_Rollmean_Left.values, 
                                        df.Pupil_Rollmean_Right.values)[0][1]
        
        # Fläche unter der Pupillen veränderungen
        sum_leftpupil = df.Left_PupilSize.sum()
        sum_rightpupil = df.Right_PupilSize.sum()
        sum_rollmean_leftpupil = df.Pupil_Rollmean_Left.sum()
        sum_rollmean_rightpupil = df.Pupil_Rollmean_Right.sum()
        
        #calculate bivariate contour elipse Area as measure of fixation stability nach vikesdal2016
        # (makes most sense in the brightness change condition)
        # 1. calc standard deviation of RawY and RawY (dont save as it is calculated anyways later)
        std_RawX = np.std(df.RawX)
        std_RawY = np.std(df.RawY)
        # 2. BCEA
        k_68 = 2.28  
        k_95 = 5.98
        # (product-moment correlation of the two position components)
        # product_moment_correlation 
        pmc = np.corrcoef(df.RawX, df.RawY)[1,0]
        BCEA_68 = k_68 * np.pi * (std_RawX*std_RawY)* np.sqrt(1-np.power(pmc,2))
        BCEA_95 = k_95 * np.pi * (std_RawX*std_RawY)* np.sqrt(1-np.power(pmc,2))
        logBCEA_68 = np.log(BCEA_68) 
        logBCEA_95 = np.log(BCEA_95)
        # create series to later concatenate to other parameters
        parameter_add_values = pd.Series([lreg_leftpupil_slope, 
                                          lreg_rightpupil_slope,
                                          lreg_leftpupil_roll_slope,
                                          lreg_rightpupil_roll_slope,
                                          sum_leftpupil, sum_rightpupil,
                                          sum_rollmean_leftpupil,
                                          sum_rollmean_rightpupil,
                                          lr_corr_pupilsize,
                                          lr_corr_pupilsize_roll,
                                          BCEA_68, BCEA_95,
                                          logBCEA_68, logBCEA_95])
        
        # create series to later concatenate to other parameters
        parameter_add_names = ['Reg_slope_Left_PupilSize',
                               'Reg_slope_Right_PupilSize',
                               'Reg_slope_Pupil_Rollmean_Left',
                               'Reg_slope_Pupil_Rollmean_Right',
                               'Sum_Left_PupilSize',
                               'Sum_Right_PupilSize',
                               'Sum_Pupil_Rollmean_Left',
                               'Sum_Pupil_Rollmean_Right',
                               'Corr_LR_Pupilsize',
                               'Corr_LR_Pupilsize_Roll',
                               'BCEA_68', 'BCEA_95',
                               'logBCEA_68', 'logBCEA_95']  
        
    elif (df.Note.iloc[1]=='Post Baseline'):
        
        # calculate means, stds, median, min, max of all relevant features
        param_list = ['Left_PupilSize','Right_PupilSize',
                          'Pupil_Rollmean_Right','Pupil_Rollmean_Left',
                          'Pupil_Slope_Rollmean_Right',
                          'Pupil_Slope_Rollmean_Left', 'Pupil_Distance_2D',
                          'Pupil_Distance_3D', 'RawX', 'RawY']
        # calculate additional parameters
        # slope of linear regression pupil change over time
        from scipy.stats import linregress

        #linear regression slopes raw data
        lreg_leftpupil = linregress(df.TimeStamp, df.Left_PupilSize)
        lreg_rightpupil = linregress(df.TimeStamp, df.Right_PupilSize)
        lreg_leftpupil_slope = lreg_leftpupil.slope
        lreg_rightpupil_slope = lreg_rightpupil.slope
        
        #linear regression slopes roll mean
        # hier müssen noch die NAs bereinigt werden
        Left_Clean = df[['TimeStamp', 'Pupil_Rollmean_Left']].dropna()
        Right_Clean = df[['TimeStamp','Pupil_Rollmean_Right']].dropna()
        
        
        lreg_leftpupil = linregress(Left_Clean)
        lreg_rightpupil = linregress(Right_Clean)
        lreg_leftpupil_roll_slope = lreg_leftpupil.slope
        lreg_rightpupil_roll_slope = lreg_rightpupil.slope
        
        
        # Correlation between left and Right Pupil Size
        lr_corr_pupilsize = np.corrcoef(df.Left_PupilSize.values, 
                                        df.Right_PupilSize.values)[0][1]
        # Correlation between left and Right Pupil Size roll means
        lr_corr_pupilsize_roll = np.corrcoef(df.Pupil_Rollmean_Left.values, 
                                        df.Pupil_Rollmean_Right.values)[0][1]
        
        # Fläche unter der Pupillen veränderungen
        sum_leftpupil = df.Left_PupilSize.sum()
        sum_rightpupil = df.Right_PupilSize.sum()
        sum_rollmean_leftpupil = df.Pupil_Rollmean_Left.sum()
        sum_rollmean_rightpupil = df.Pupil_Rollmean_Right.sum()
        
        #calculate bivariate contour elipse Area as measure of fixation stability nach vikesdal2016
        # (makes most sense in the brightness change condition)
        # 1. calc standard deviation of RawY and RawY (dont save as it is calculated anyways later)
        std_RawX = np.std(df.RawX)
        std_RawY = np.std(df.RawY)
        # 2. BCEA
        k_68 = 2.28  
        k_95 = 5.98
        # (product-moment correlation of the two position components)
        # product_moment_correlation 
        pmc = np.corrcoef(df.RawX, df.RawY)[1,0]
        BCEA_68 = k_68 * np.pi * (std_RawX*std_RawY)* np.sqrt(1-np.power(pmc,2))
        BCEA_95 = k_95 * np.pi * (std_RawX*std_RawY)* np.sqrt(1-np.power(pmc,2))
        logBCEA_68 = np.log(BCEA_68) 
        logBCEA_95 = np.log(BCEA_95)
        # create series to later concatenate to other parameters
        parameter_add_values = pd.Series([lreg_leftpupil_slope, 
                                          lreg_rightpupil_slope,
                                          lreg_leftpupil_roll_slope,
                                          lreg_rightpupil_roll_slope,
                                          sum_leftpupil, sum_rightpupil,
                                          sum_rollmean_leftpupil,
                                          sum_rollmean_rightpupil,
                                          lr_corr_pupilsize,
                                          lr_corr_pupilsize_roll,
                                          BCEA_68, BCEA_95,
                                          logBCEA_68, logBCEA_95])
        
        # create series to later concatenate to other parameters
        parameter_add_names = ['Reg_slope_Left_PupilSize',
                               'Reg_slope_Right_PupilSize',
                               'Reg_slope_Pupil_Rollmean_Left',
                               'Reg_slope_Pupil_Rollmean_Right',
                               'Sum_Left_PupilSize',
                               'Sum_Right_PupilSize',
                               'Sum_Pupil_Rollmean_Left',
                               'Sum_Pupil_Rollmean_Right',
                               'Corr_LR_Pupilsize',
                               'Corr_LR_Pupilsize_Roll',
                               'BCEA_68', 'BCEA_95',
                               'logBCEA_68', 'logBCEA_95']  
        
    
    elif (df.Note.iloc[1]=='Movement 1' or df.Note.iloc[1]=='Movement 2'):
        
        # calculate means, stds, median, min, max of all relevant features
        param_list = [#'Target_Number',
                      'Left_PupilSize', 
                      'Right_PupilSize', 
                      #'Left_PupilCenterX',
                      #'Left_PupilCenterY', 
                      #'Left_PupilCenterZ',
                      #'Right_PupilCenterX',
                      #'Right_PupilCenterY', 
                      #'Right_PupilCenterZ',
                      #'CorrectedX',
                      #'CorrectedY', 
                      #'TargetPosX', 
                      #'TargetPosY',
                      #'RawX', 
                      #'RawY',
                      #'RawX_roll', 
                      #'RawY_roll',
                      'Target_Eye_Distance',
                      'Target_Eye_Distance_Diff',
                      'Target_Eye_Distance_CumDiff',
                      'Target_velocity_px_s',
                      'Target_velocity_roll_px_s',
                      'Eye_velocity_px_s',
                      'Eye_velocity_roll_px_s',
                      'Eye_velocity_savgol_px_s',
                      'Pursuit_Gain',
                      'Pursuit_Gain_Roll',
                      'Pursuit_Gain_Savgol']
        # calculate
        from scipy.stats import linregress
        from statsmodels.tools.eval_measures import rmse
        
        #TODO: break down the first movement into two parts Beginning/ End
        # in the beginning there should be initial catch-up saccades to the target and in the end
        # the eye data should be much more alligned with target vector
        
        # total distance eye moved
        # difference between the eye total movement to the target(overall)

        # total distance traveled of the eye in comparison to distance traveled of target in x and y direction
        # Note: use absolute values of differences
        dist_x_target = df.TargetPosX.diff().abs().sum()
        dist_y_target = df.TargetPosY.diff().abs().sum()
        dist_x_eye = df.RawX.diff().abs().sum()
        dist_y_eye = df.RawY.diff().abs().sum()
        # difference of traveled overall distance between eye and target 
        diff_x_eye_target = dist_x_eye - dist_x_target
        diff_y_eye_target = dist_y_eye - dist_y_target
        
        
        #copy me
        # Linearregression params
        slope, intercept, r_value, p_value, std_err = linregress(df.RawX.dropna(), df.RawY.dropna())
        slope_roll, intercept_roll, r_value_roll, p_value_roll, std_err_roll =  linregress(df.RawX_roll.dropna(), df.RawY_roll.dropna())

        # Mean pursuit gain eye_velocity/target velocity with saccades removed
        
        # Number / amplitude / velocity of saccades during smooth pursuits
        
        # RMSE between target and eye position with saccades included
        # BUT this is highly dependent on calibration accuracy
        # 1. Raw data RMSE X and Y

        #copy me
        #at the office aith stefan.
        '''
        rmse_x = rmse(df.RawX.dropna(), df.TargetPosX)
        rmse_y = rmse(df.RawY.dropna(), df.TargetPosY)
        # 2. Roll data RMSE X and Y 
        #TODO: hier passt die Anzahl der Sample nicht durch die Mittelung sind NAs drin
        #rmse_x_roll = 0
        rmse_x_roll = rmse(df.RawX_roll.dropna(), df.TargetPosX[~np.isnan(df.RawX_roll)])
        #rmse_y_roll = 0
        rmse_y_roll = rmse(df.RawY_roll.dropna(), df.TargetPosY[~np.isnan(df.RawY_roll)])        
        
        #copy!
        # median shift target number vector to median xy of RawX RawY
        # calculate median x and y values of target and eye data
        median_diff_x = df.TargetPosX.median() - df.RawX.dropna().median()
        median_diff_y = df.TargetPosY.median() - df.RawY.dropna().median()
    
        rmse_x_median_shift = rmse(df.RawX.dropna(), df.TargetPosX - median_diff_x)
        rmse_y_median_shift = rmse(df.RawY.dropna(), df.TargetPosY - median_diff_y)
        
        # median shift target number vector to median xy of (RawX RawY) roll
        # calculate median x and y values of target and eye data
        median_diff_x_roll = df.TargetPosX[~np.isnan(df.RawX_roll)].median() - df.RawX_roll.dropna().median()
        median_diff_y_roll = df.TargetPosY[~np.isnan(df.RawY_roll)].median() - df.RawY_roll.dropna().median()
        
        rmse_x_roll_median_shift = rmse(df.RawX_roll.dropna(), df.TargetPosX[~np.isnan(df.RawX_roll)] - median_diff_x_roll)
        rmse_y_roll_median_shift = rmse(df.RawY_roll.dropna(), df.TargetPosY[~np.isnan(df.RawY_roll)] - median_diff_y_roll)
        '''
        #print('Debug RMSE Shift X:', rmse_x_median_shift)
        
        # create series to later concatenate to other parameters
        parameter_add_values = pd.Series([dist_x_eye, dist_y_eye, diff_x_eye_target, diff_y_eye_target,
                                          slope, intercept, r_value, p_value, std_err,
                                          slope_roll, intercept_roll, r_value_roll, 
                                          p_value_roll, std_err_roll,
                                          rmse_x, rmse_y,
                                          rmse_x_roll, rmse_y_roll,
                                          rmse_x_median_shift, rmse_y_median_shift,
                                          rmse_x_roll_median_shift, rmse_y_roll_median_shift])
        # create series to later concatenate to other parameters
        parameter_add_names = ['Distance_Eye_RawX', 'Distance_Eye_RawY',
                               'Eye_Target_DiffDist_RawX', 'Eye_Target_DiffDist_RawY',
                               'Reg_slope_RawXY', 'Reg_intercept_RawXY',
                               'Reg_r_value_RawXY', 'Reg_p_value_RawXY',  
                               'Reg_std_err_RawXY',
                               'Reg_slope_RawXY_roll', 'Reg_intercept_RawXY_roll',
                               'Reg_r_value_RawXY_roll', 'Reg_p_value_RawXY_roll',  
                               'Reg_std_err_RawXY_roll',
                               'RMSE_RawX','RMSE_RawY',
                               'RMSE_RawX_roll','RMSE_RawY_roll',
                               'RMSE_RawX_MedianShift','RMSE_RawY_MedianShift',
                               'RMSE_RawX_roll_MedianShift','RMSE_RawY_roll_MedianShift']  
        
   
    elif (df.Note.iloc[1]=='Transition'):
        
        # calculate means, stds, median, min, max of all relevant features
        param_list = [
                             #'Detected_Number',
                             #'Target_Number',
                             'Left_PupilSize', 
                             'Right_PupilSize', 
                             'Left_PupilCenterX',
                             'Left_PupilCenterY', 
                             'Left_PupilCenterZ',
                             'Right_PupilCenterX',
                             'Right_PupilCenterY', 
                             'Right_PupilCenterZ',
                             'CorrectedX',
                             'CorrectedY', 
                             'TargetPosX', 
                             'TargetPosY',
                             'RawX', 
                             'RawY', 
                             'TimeStamp', 
                             'EyeTime']
        
    elif (df.Note.iloc[1]=='FinalCalculation'):
        
        # calculate means, stds, median, min, max of all relevant features
        param_list = [
                             #'Detected_Number',
                             #'Target_Number',
                             'Left_PupilSize', 
                             'Right_PupilSize', 
                             'Left_PupilCenterX',
                             'Left_PupilCenterY', 
                             'Left_PupilCenterZ',
                             'Right_PupilCenterX',
                             'Right_PupilCenterY', 
                             'Right_PupilCenterZ',
                             'CorrectedX',
                             'CorrectedY', 
                             'TargetPosX', 
                             'TargetPosY',
                             'RawX', 
                             'RawY', 
                             'TimeStamp', 
                             'EyeTime']
    
    elif (df.Note.iloc[1]=='ReturnPhase'):
        
        # calculate means, stds, median, min, max of all relevant features
        param_list = [
                             #'Detected_Number',
                             #'Target_Number',
                             'Left_PupilSize', 
                             'Right_PupilSize', 
                             'Left_PupilCenterX',
                             'Left_PupilCenterY', 
                             'Left_PupilCenterZ',
                             'Right_PupilCenterX',
                             'Right_PupilCenterY', 
                             'Right_PupilCenterZ',
                             'CorrectedX',
                             'CorrectedY', 
                             'TargetPosX', 
                             'TargetPosY',
                             'RawX', 
                             'RawY', 
                             'TimeStamp', 
                             'EyeTime']        
        
    elif (df.Note.iloc[1]=='Wait for Return'):
        # calculate means, stds, median, min, max of all relevant features
        param_list = [
                             #'Detected_Number',
                             #'Target_Number',
                             'Left_PupilSize', 
                             'Right_PupilSize', 
                             'Left_PupilCenterX',
                             'Left_PupilCenterY', 
                             'Left_PupilCenterZ',
                             'Right_PupilCenterX',
                             'Right_PupilCenterY', 
                             'Right_PupilCenterZ',
                             'CorrectedX',
                             'CorrectedY', 
                             'TargetPosX', 
                             'TargetPosY',
                             'RawX', 
                             'RawY', 
                             'TimeStamp', 
                             'EyeTime']
        
    else:
        print('Marker not found')
        return 
    
    # get ID, MoveID, Trial and Note values etc
    if df.Note.iloc[1]=='Movement 1' or df.Note.iloc[1]=='Movement 2':
        df_ID = df[['ID','MoveID','Trial','Note', 'Target_Number']].iloc[0,:]

    else:
        df_ID = df[['ID','MoveID','Trial','Note']].iloc[0,:]
    df_ID = df_ID.to_frame()
    df_ID = df_ID.T
    
    id_param_names = list(df_ID)
    

    # calculate values
    #for the entire df not only movment1 and 2
    mean_value = df[param_list].mean()
    median_value = df[param_list].median()
    std_value = df[param_list].std()
    min_value = df[param_list].min()
    # 10 smallest values mean
    # TODO: order nach column ist irgendwie quatsch aber egal weil gleich der mean gebildet wird
    mean_min_value = df[param_list].nsmallest(10, columns=param_list).mean()
    max_value = df[param_list].max()
    # 10 largest values mean
    mean_max_value = df[param_list].nlargest(10,columns=param_list).mean()
    
    parameter_values = pd.concat([mean_value, median_value, std_value,
                        min_value, mean_min_value, max_value, mean_max_value,
                        parameter_add_values])
    
    # create and transpose series to data frame
    df_feature_val  = parameter_values.to_frame()
    df_feature_val = df_feature_val.T
    
    # concatenate the ID and the Value data frame
    # irgendwie müssen die indexe getdroppt werden sonst funktioniert das concat nicht und die dtypes gehen verloren
    df_feature = pd.concat([df_ID.reset_index(drop=True), 
                            df_feature_val.reset_index(drop=True)], axis=1)
    #, axis = 1, ignore_index=True
    # Define Parameter names
    param_list_mean_val = ['Mean_' + s for s in param_list]
    param_list_median_val = ['Median_' + s for s in param_list]
    param_list_std_val = ['Std_' + s  for s in param_list]
    param_list_min_val = ['Min_' + s  for s in param_list]
    param_list_mean_min_val = ['MeanMin_' + s  for s in param_list]
    param_list_max_val = ['Max_' + s  for s in param_list]
    param_list_mean_max_val = ['MeanMax_' + s  for s in param_list]

    parameter_names = id_param_names + param_list_mean_val + \
    param_list_median_val + param_list_std_val + param_list_min_val +\
    param_list_mean_min_val + param_list_max_val +\
    param_list_mean_max_val + parameter_add_names
    
    # rename data frame columns
    df_feature.columns = parameter_names

    return df_feature 

'''
TODO: Define a function that combines the data frames from the 
different interaction sequences

something like this e.g. for calibration:
    
df_temp = pd.merge(dfcal_pre_b1_feature, dfcal_lum_feature, 
           on=['ID','MoveID', 'Trial'], how='outer', 
           indicator=False,suffixes=('_preBl', '_Lum'))

# relabel names for last data frame for merging
name_param = [s + '_postBl' for s in list(dfcal_post_b1_feature)[4:]] 
name_id = list(dfcal_post_b1_feature)[:4]
name_all = name_id + name_param
dfcal_post_b1_feature.columns =  name_all

# merge existing data frame with post baseline data frame
dfcal_feature_raw = df_temp.merge(dfcal_post_b1_feature , 
               on=['ID','MoveID', 'Trial'] , how='outer', indicator=False)

'''



'''
The purpose of this function is:

    1. Calculate biometric features as a combination from features across 
    different interaction steps. E.g. calculate the difference of a feature 
    between pre baseline and luminance change during calibration
    
    
'''

# Purpose of this function is ...
def calc_final_features(dfcal_feature_raw, interaction_seq):
    
    
    if (interaction_seq == 'Calibration'):
        
        dfcal_feature = dfcal_feature_raw.iloc[:,:3]
        # LIST of all calculated features
        # Pupilsizes baseline corrected (Mean/Min/Max) Raw values
        # Mean Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Mean_Left_PupilSize = 
                             dfcal_feature_raw.Mean_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Mean_Left_PupilSize_preBl.values)
        
        # Mean Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Mean_Right_PupilSize = 
                             dfcal_feature_raw.Mean_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Mean_Right_PupilSize_preBl.values)
        
        # Pupilsizes baseline corrected (Mean/Min/Max)
        # Mean Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Mean_Left_PupilSize = 
                             dfcal_feature_raw.Mean_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Mean_Left_PupilSize_postBl.values)
        
        # Mean Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Mean_Right_PupilSize = 
                             dfcal_feature_raw.Mean_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Mean_Right_PupilSize_postBl.values)
        
       
        # Mean Left pre to lum+post
        dfcal_feature = dfcal_feature.assign(Pre_LumPost_BlCor_Mean_Left_PupilSize = 
                             np.mean([dfcal_feature_raw.Mean_Left_PupilSize_Lum.values,
                                     dfcal_feature_raw.Mean_Left_PupilSize_postBl.values], axis=0) - 
                             dfcal_feature_raw.Mean_Left_PupilSize_preBl.values)
        
        # Mean Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_LumPost_BlCor_Mean_Right_PupilSize = 
                             np.mean([dfcal_feature_raw.Mean_Right_PupilSize_Lum.values,
                                     dfcal_feature_raw.Mean_Right_PupilSize_postBl.values], axis=0)- 
                             dfcal_feature_raw.Mean_Right_PupilSize_preBl.values)
        
     
        
        # Std
        # Std Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Std_Left_PupilSize = 
                             dfcal_feature_raw.Std_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Std_Left_PupilSize_preBl.values)
        
        # Std Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Std_Right_PupilSize = 
                             dfcal_feature_raw.Std_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Std_Right_PupilSize_preBl.values)
        
        # Std Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Std_Left_PupilSize = 
                             dfcal_feature_raw.Std_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Std_Left_PupilSize_postBl.values)
        
        # Std Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Std_Right_PupilSize = 
                             dfcal_feature_raw.Std_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Std_Right_PupilSize_postBl.values)
        
        
        # Median
        # Median Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Median_Left_PupilSize = 
                             dfcal_feature_raw.Median_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Median_Left_PupilSize_preBl.values)
        
        # Median Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Median_Right_PupilSize = 
                             dfcal_feature_raw.Median_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Median_Right_PupilSize_preBl.values)
        
        # Median Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Median_Left_PupilSize = 
                             dfcal_feature_raw.Median_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Median_Left_PupilSize_postBl.values)
        
        # Median Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Median_Right_PupilSize = 
                             dfcal_feature_raw.Median_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Median_Right_PupilSize_postBl.values)
        '''
        # Max
        # Max Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Max_Left_PupilSize = 
                             dfcal_feature_raw.Max_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Max_Left_PupilSize_preBl.values)
        
        # Max Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Max_Right_PupilSize = 
                             dfcal_feature_raw.Max_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Max_Right_PupilSize_preBl.values)
        
        # Max Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Max_Left_PupilSize = 
                             dfcal_feature_raw.Max_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Max_Left_PupilSize_postBl.values)
        
        # Max Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Max_Right_PupilSize = 
                             dfcal_feature_raw.Max_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Max_Right_PupilSize_postBl.values)
        '''
        # MeanMax
        # MeanMax Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_MeanMax_Left_PupilSize = 
                             dfcal_feature_raw.MeanMax_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.MeanMax_Left_PupilSize_preBl.values)
        
        # MeanMax Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_MeanMax_Right_PupilSize = 
                             dfcal_feature_raw.MeanMax_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.MeanMax_Right_PupilSize_preBl.values)
        
        # MeanMax Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_MeanMax_Left_PupilSize = 
                             dfcal_feature_raw.MeanMax_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.MeanMax_Left_PupilSize_postBl.values)
        
        # MeanMax Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_MeanMax_Right_PupilSize = 
                             dfcal_feature_raw.MeanMax_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.MeanMax_Right_PupilSize_postBl.values)
        
         # MeanMax
        # MeanMax Left to pre to lumpost
        dfcal_feature = dfcal_feature.assign(Pre_LumPost_BlCor_MeanMax_Left_PupilSize = 
                             (dfcal_feature_raw.MeanMax_Left_PupilSize_Lum.values +
                              dfcal_feature_raw.MeanMax_Left_PupilSize_postBl.values) - 
                             dfcal_feature_raw.MeanMax_Left_PupilSize_preBl.values)
        
        # MeanMax
        # MeanMax Right to LUmpost
        dfcal_feature = dfcal_feature.assign(Pre_LumPost_BlCor_MeanMax_Right_PupilSize = 
                             (dfcal_feature_raw.MeanMax_Right_PupilSize_Lum.values +
                              dfcal_feature_raw.MeanMax_Right_PupilSize_postBl.values) - 
                             dfcal_feature_raw.MeanMax_Right_PupilSize_preBl.values)
        
         # MeanMax
        # MeanMax Left to pre to post
        dfcal_feature = dfcal_feature.assign(Pre_Post_BlCor_MeanMax_Left_PupilSize = 
                             dfcal_feature_raw.MeanMax_Left_PupilSize_postBl.values - 
                             dfcal_feature_raw.MeanMax_Left_PupilSize_preBl.values)
        
        # MeanMax
        # MeanMax Right to  pre to post
        dfcal_feature = dfcal_feature.assign(Pre_Post_BlCor_MeanMax_Right_PupilSize = 
                             dfcal_feature_raw.MeanMax_Right_PupilSize_postBl.values - 
                             dfcal_feature_raw.MeanMax_Right_PupilSize_preBl.values)
        
        
        
        
        '''
        # Min
        # Min Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Min_Left_PupilSize = 
                             dfcal_feature_raw.Min_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Min_Left_PupilSize_preBl.values)
        
        # Min Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Min_Right_PupilSize = 
                             dfcal_feature_raw.Min_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Min_Right_PupilSize_preBl.values)
        
        # Min Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Min_Left_PupilSize = 
                             dfcal_feature_raw.Min_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Min_Left_PupilSize_postBl.values)
        
        # Min Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Min_Right_PupilSize = 
                             dfcal_feature_raw.Min_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Min_Right_PupilSize_postBl.values)
        '''
        # MeanMin
        # MeanMeanMin Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_MeanMin_Left_PupilSize = 
                             dfcal_feature_raw.MeanMin_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.MeanMin_Left_PupilSize_preBl.values)
        
        # MeanMin Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_MeanMin_Right_PupilSize = 
                             dfcal_feature_raw.MeanMin_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.MeanMin_Right_PupilSize_preBl.values)
        
        # MeanMin Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_MeanMin_Left_PupilSize = 
                             dfcal_feature_raw.MeanMin_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.MeanMin_Left_PupilSize_postBl.values)
        
        # MeanMin Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_MeanMin_Right_PupilSize = 
                             dfcal_feature_raw.MeanMin_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.MeanMin_Right_PupilSize_postBl.values)
        
        
        #MeanMin to MeanMax Right Lum to pre
        dfcal_feature = dfcal_feature.assign(PretoLum_BlCor_MaxtoMin_Right_PupilSize = 
                             dfcal_feature_raw.MeanMax_Right_PupilSize_Lum.values/ 
                             dfcal_feature_raw.MeanMin_Right_PupilSize_preBl.values)
        #MeanMin to MeanMax Right post to pre
        dfcal_feature = dfcal_feature.assign(PretoPost_BlCor_MaxtoMin_Right_PupilSize = 
                             dfcal_feature_raw.MeanMax_Right_PupilSize_postBl.values/ 
                             dfcal_feature_raw.MeanMin_Right_PupilSize_preBl.values)
             
        #MeanMin to MeanMax Left Lum to pre
        dfcal_feature = dfcal_feature.assign(PretoLum_BlCor_MaxtoMin_Left_PupilSize = 
                             dfcal_feature_raw.MeanMax_Left_PupilSize_Lum.values/ 
                             dfcal_feature_raw.MeanMin_Left_PupilSize_preBl.values)
        #MeanMin to MeanMax Left post to pre
        dfcal_feature = dfcal_feature.assign(PretoPost_BlCor_MaxtoMin_Left_PupilSize = 
                             dfcal_feature_raw.MeanMax_Left_PupilSize_postBl.values/ 
                             dfcal_feature_raw.MeanMin_Left_PupilSize_preBl.values)
        
       
        
        
        
        # Sum
        # Sum Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Sum_Left_PupilSize = 
                             dfcal_feature_raw.Sum_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Sum_Left_PupilSize_preBl.values)
        
        # Sum Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Sum_Right_PupilSize = 
                             dfcal_feature_raw.Sum_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Sum_Right_PupilSize_preBl.values)
        
        # Sum Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Sum_Left_PupilSize = 
                             dfcal_feature_raw.Sum_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Sum_Left_PupilSize_postBl.values)
        
        # Sum Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Sum_Right_PupilSize = 
                             dfcal_feature_raw.Sum_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Sum_Right_PupilSize_postBl.values)
        
        # Sum (pre vs. lum+post)
        # Sum Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_LumPost_BlCor_Sum_Left_PupilSize = 
                             (dfcal_feature_raw.Sum_Left_PupilSize_Lum.values +
                              dfcal_feature_raw.Sum_Left_PupilSize_postBl.values) - 
                             dfcal_feature_raw.Sum_Left_PupilSize_preBl.values)
        
        # Sum Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_LumPost_BlCor_Sum_Right_PupilSize = 
                             (dfcal_feature_raw.Sum_Right_PupilSize_Lum.values +
                             dfcal_feature_raw.Sum_Right_PupilSize_postBl.values) - 
                             dfcal_feature_raw.Sum_Right_PupilSize_preBl.values)
        
   
        
        # Regression Slopes
        # Reg_slope Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Reg_slope_Left_PupilSize = 
                             dfcal_feature_raw.Reg_slope_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Reg_slope_Left_PupilSize_preBl.values)
        
        
        # Reg_slope Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Reg_slope_Right_PupilSize = 
                             dfcal_feature_raw.Reg_slope_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Reg_slope_Right_PupilSize_preBl.values)
        
        # Reg_slope Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Reg_slope_Left_PupilSize = 
                             dfcal_feature_raw.Reg_slope_Left_PupilSize_Lum.values - 
                             dfcal_feature_raw.Reg_slope_Left_PupilSize_postBl.values)
        
        # Reg_slope Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Reg_slope_Right_PupilSize = 
                             dfcal_feature_raw.Reg_slope_Right_PupilSize_Lum.values - 
                             dfcal_feature_raw.Reg_slope_Right_PupilSize_postBl.values)
        
        
        
        # Correlations Left and Right Pupilsize
        # Reg_slope Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Corr_LR_Pupilsize = 
                             dfcal_feature_raw.Corr_LR_Pupilsize_Lum.values - 
                             dfcal_feature_raw.Corr_LR_Pupilsize_preBl.values)
        
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Corr_LR_Pupilsize = 
                             dfcal_feature_raw.Corr_LR_Pupilsize_Lum.values - 
                             dfcal_feature_raw.Corr_LR_Pupilsize_postBl.values)

        
        
        ##############################################################################
        # Pupilsizes baseline corrected (Mean/Min/Max) Geglättete Kurven
        
        '''
        # Mean Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Mean_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Mean_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Mean_Pupil_Rollmean_Left_preBl.values)
        
        # Mean Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Mean_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Mean_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Mean_Pupil_Rollmean_Right_preBl.values)
        
        # Pupilsizes baseline corrected (Mean/Min/Max)
        # Mean Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Mean_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Mean_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Mean_Pupil_Rollmean_Left_postBl.values)
        
        # Mean Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Mean_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Mean_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Mean_Pupil_Rollmean_Right_postBl.values)
        
        # Std
        # Std Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Std_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Std_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Std_Pupil_Rollmean_Left_preBl.values)
        
        # Std Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Std_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Std_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Std_Pupil_Rollmean_Right_preBl.values)
        
        # Std Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Std_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Std_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Std_Pupil_Rollmean_Left_postBl.values)
        
        # Std Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Std_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Std_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Std_Pupil_Rollmean_Right_postBl.values)
        
        
        # Median
        # Median Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Median_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Median_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Median_Pupil_Rollmean_Left_preBl.values)
        
        # Median Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Median_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Median_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Median_Pupil_Rollmean_Right_preBl.values)
        
        # Median Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Median_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Median_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Median_Pupil_Rollmean_Left_postBl.values)
        
        # Median Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Median_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Median_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Median_Pupil_Rollmean_Right_postBl.values)
        '''
        '''
        # Max
        # Max Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Max_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Max_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Max_Pupil_Rollmean_Left_preBl.values)
        
        # Max Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Max_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Max_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Max_Pupil_Rollmean_Right_preBl.values)
        
        # Max Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Max_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Max_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Max_Pupil_Rollmean_Left_postBl.values)
        
        # Max Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Max_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Max_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Max_Pupil_Rollmean_Right_postBl.values)
        '''
        '''
        # MeanMax
        # MeanMax Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_MeanMax_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.MeanMax_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.MeanMax_Pupil_Rollmean_Left_preBl.values)
        
        # MeanMax Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_MeanMax_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.MeanMax_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.MeanMax_Pupil_Rollmean_Right_preBl.values)
        
        # MeanMax Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_MeanMax_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.MeanMax_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.MeanMax_Pupil_Rollmean_Left_postBl.values)
        
        # MeanMax Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_MeanMax_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.MeanMax_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.MeanMax_Pupil_Rollmean_Right_postBl.values)
        '''
        '''
        # Min
        # Min Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Min_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Min_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Min_Pupil_Rollmean_Left_preBl.values)
        
        # Min Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Min_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Min_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Min_Pupil_Rollmean_Right_preBl.values)
        
        # Min Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Min_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Min_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Min_Pupil_Rollmean_Left_postBl.values)
        
        # Min Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Min_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Min_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Min_Pupil_Rollmean_Right_postBl.values)
        '''
        '''
        # MeanMin
        # MeanMeanMin Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_MeanMin_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.MeanMin_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.MeanMin_Pupil_Rollmean_Left_preBl.values)
        
        # MeanMin Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_MeanMin_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.MeanMin_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.MeanMin_Pupil_Rollmean_Right_preBl.values)
        
        # MeanMin Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_MeanMin_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.MeanMin_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.MeanMin_Pupil_Rollmean_Left_postBl.values)
        
        # MeanMin Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_MeanMin_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.MeanMin_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.MeanMin_Pupil_Rollmean_Right_postBl.values)
        '''
        '''
        # Sum
        # Sum Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Sum_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Sum_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Sum_Pupil_Rollmean_Left_preBl.values)
        
        # Sum Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Sum_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Sum_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Sum_Pupil_Rollmean_Right_preBl.values)
        
        # Sum Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Sum_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Sum_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Sum_Pupil_Rollmean_Left_postBl.values)
        
        # Sum Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Sum_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Sum_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Sum_Pupil_Rollmean_Right_postBl.values)
        
        '''
        '''
        # Regression Slopes
        # Reg_slope Left to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Reg_slope_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Reg_slope_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Reg_slope_Pupil_Rollmean_Left_preBl.values)
        
        
        # Reg_slope Right to pre
        dfcal_feature = dfcal_feature.assign(Pre_BlCor_Reg_slope_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Reg_slope_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Reg_slope_Pupil_Rollmean_Right_preBl.values)
        
        # Reg_slope Left to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Reg_slope_Pupil_Rollmean_Left = 
                             dfcal_feature_raw.Reg_slope_Pupil_Rollmean_Left_Lum.values - 
                             dfcal_feature_raw.Reg_slope_Pupil_Rollmean_Left_postBl.values)
        
        # Reg_slope Right to post
        dfcal_feature = dfcal_feature.assign(Post_BlCor_Reg_slope_Pupil_Rollmean_Right = 
                             dfcal_feature_raw.Reg_slope_Pupil_Rollmean_Right_Lum.values - 
                             dfcal_feature_raw.Reg_slope_Pupil_Rollmean_Right_postBl.values)
        '''
        #########################################################################################
        #Absolute Values
        # Mean Pupillendistanz 
        dfcal_feature = dfcal_feature.assign(Mean_PupilDist2D = 
                                             dfcal_feature_raw.Mean_Pupil_Distance_2D_Lum.values)
        dfcal_feature = dfcal_feature.assign(Median_PupilDist2D = 
                                             dfcal_feature_raw.Median_Pupil_Distance_2D_Lum.values)
        dfcal_feature = dfcal_feature.assign(Std_PupilDist2D = 
                                             dfcal_feature_raw.Std_Pupil_Distance_2D_Lum.values)
        
        dfcal_feature = dfcal_feature.assign(Mean_PupilDist3D = 
                                             dfcal_feature_raw.Mean_Pupil_Distance_3D_Lum.values)
        dfcal_feature = dfcal_feature.assign(Median_PupilDist3D = 
                                             dfcal_feature_raw.Median_Pupil_Distance_3D_Lum.values)
        dfcal_feature = dfcal_feature.assign(Std_PupilDist3D = 
                                             dfcal_feature_raw.Std_Pupil_Distance_3D_Lum.values)
        
        # Calculate Pupildistance as center of density for Pupil Center positions
        dfcal_feature = dfcal_feature.assign(Max_Dense_PupilDist2D = 
                                             dfcal_feature_raw.Max_Dense_PupilDist_2D.values)
        
        
        
        # BCEA_68 and BCEA_95 Bivariate contour ellipse area 
        dfcal_feature = dfcal_feature.assign(BCEA_68_Lum = 
                                             dfcal_feature_raw.BCEA_68_Lum.values)
        dfcal_feature = dfcal_feature.assign(BCEA_95_Lum = 
                                             dfcal_feature_raw.BCEA_95_Lum.values)
        dfcal_feature = dfcal_feature.assign(logBCEA_68_Lum = 
                                             dfcal_feature_raw.logBCEA_68_Lum.values)
        dfcal_feature = dfcal_feature.assign(logBCEA_95_Lum = 
                                             dfcal_feature_raw.logBCEA_95_Lum.values)
        # BCEA_68 and BCEA_95 Bivariate contour ellipse area 
        dfcal_feature = dfcal_feature.assign(BCEA_68_preBl = 
                                             dfcal_feature_raw.BCEA_68_preBl.values)
        dfcal_feature = dfcal_feature.assign(BCEA_95_preBl = 
                                             dfcal_feature_raw.BCEA_95_preBl.values)
        dfcal_feature = dfcal_feature.assign(logBCEA_68_preBl = 
                                             dfcal_feature_raw.logBCEA_68_preBl.values)
        dfcal_feature = dfcal_feature.assign(logBCEA_95_preBl = 
                                             dfcal_feature_raw.logBCEA_95_preBl.values)
        # BCEA_68 and BCEA_95 Bivariate contour ellipse area 
        dfcal_feature = dfcal_feature.assign(BCEA_68_postBl = 
                                             dfcal_feature_raw.BCEA_68_postBl.values)
        dfcal_feature = dfcal_feature.assign(BCEA_95_postBl = 
                                             dfcal_feature_raw.BCEA_95_postBl.values)
        dfcal_feature = dfcal_feature.assign(logBCEA_68_postBl = 
                                             dfcal_feature_raw.logBCEA_68_postBl.values)
        dfcal_feature = dfcal_feature.assign(logBCEA_95_postBl = 
                                             dfcal_feature_raw.logBCEA_95_postBl.values)
        '''
        # Mean Pupillensize L/R preBL
        dfcal_feature = dfcal_feature.assign(Mean_Left_PupilSize_preBl = 
                                            dfcal_feature_raw.Mean_Left_PupilSize_preBl.values)
        dfcal_feature = dfcal_feature.assign(Mean_Right_PupilSize_preBl = 
                                            dfcal_feature_raw.Mean_Right_PupilSize_preBl.values)
        
        # Mean Pupillensize L/R Luminance change
        dfcal_feature = dfcal_feature.assign(Mean_Left_PupilSize_Lum = 
                                            dfcal_feature_raw.Mean_Left_PupilSize_Lum.values)
        dfcal_feature = dfcal_feature.assign(Mean_Right_PupilSize_Lum = 
                                            dfcal_feature_raw.Mean_Right_PupilSize_Lum.values)
        # Mean Pupillensize L/R postBl
        dfcal_feature = dfcal_feature.assign(Mean_Left_PupilSize_postBl = 
                                            dfcal_feature_raw.Mean_Left_PupilSize_postBl.values)
        dfcal_feature = dfcal_feature.assign(Mean_Right_PupilSize_postBl = 
                                            dfcal_feature_raw.Mean_Right_PupilSize_postBl.values)
        '''
        # remove all rows where more than 50% of columns values are NA 
        dfcal_feature = dfcal_feature.dropna(thresh = round((dfcal_feature.shape[1])*0.50))
        
    else:
        print('No valid Sequence name')
            
    
    return dfcal_feature


# Some plotting functions that I probably won't need anymore
'''

# Purpose of this function is ...

# Define Functions for Printing
# 1. Lineplots over time
# Input: df - data frame
#        groupvar - group the data by which variable
#        x - x axis variable
#        y - y axis variable
def plot_lines_byfactor(df, groupvar, x , y, lstyle='.--'):
        
    
    grouped = df.groupby(groupvar)
    fig, ax = plt.subplots(figsize=(10, 5), dpi=100)
    #cmap = plt.get_cmap(name='Paired')
    
    for key, group in grouped:
            
        group.plot(x, y, label=key, ax=ax, style=lstyle)

    plt.title((group.Note.iloc[0] + ' grouped by ' + '  '.join(groupvar)))    
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=2)
    # ensure that always all data is shown
    plt.ylim((df[y].min()-np.abs(0.10*df[y].max()),df[y].max()+ np.abs(0.10*df[y].max())))
    plt.xlim((df[x].min()-np.abs(0.10*df[x].max()),df[x].max()+ np.abs(0.10*df[x].max())))
    plt.xlabel(x)
    plt.ylabel(y)
    #plt.show()

    return fig

# 2. Lineplots over time all variables
# Input: df - data frame
#        groupvar - group the data by which variable
#        x - x axis variable
# Purpose of this function is ...

def plot_lines_by_factor_all(df, groupvar, x, lstyle ='.--'):
    # get only numeric columns
    param_list = list(df.select_dtypes(include=[np.number]))
    # Remove Time Variable
    param_list.remove(x)
    print(param_list)
    
    from matplotlib.backends.backend_pdf import PdfPages
    pp = PdfPages(df.Note.iloc[0] + '.pdf')

    for param in param_list:
        fg = plot_lines_byfactor(df = df, 
                            groupvar = groupvar, x = x,
                            y = param, lstyle = lstyle)
        
        #plt.show(fg)
        pp.savefig(fg)
    pp.close()

    return



def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
'''
