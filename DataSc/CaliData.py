from DataSc.Data import data
import pandas as pd
import numpy as np
from scipy.signal import savgol_filter
from scipy.stats import linregress
from scipy import stats
from DataSc import Log
from colorama import *
from Configuration import ParameterManger as PM
import os
import matplotlib.pyplot as plt

class caliData(data):
    def __init__(self,MyData, Device):
        Log.AddToLog('Cali was choosen')
        super().__init__()
        self.CaliSequence = ['Pre Baseline','Brightness Change','Post Baseline']
        self.SeperationFlag = True
        self.StopFalg=False
        # that might not be beautiful but efficient to remove all necessary sequences in the data during initialization
        # remove all sequences that are not of interest for the current revision
        self.MyData = MyData[MyData.Note.isin(self.CaliSequence)]

        self.revision = PM.ReturnParameter('self.revision')     #need to be syncronize with the reales number at Data_Sc GIT. This should always be
                                # changed when adding/removing features, also Preprocessingmatrix has to be updated
        self.device = Device
        self.rollwindowlength = PM.ReturnParameter('self.rollwindowlength')   # time in seconds
    def runner(self):
        Log.AddToLog('runner.caliData started')
        try:
            # if filter dont work
            self.FilterBy='Note'
            if not self.Filter(self.FilterBy):
                return False
            # TODO: This only works if we group the data frame, when only one group is available this will fail!!!!!!!!!!!!
            # TODO: implement check
            '''--------------------------clean dada--------------------------------------------------------'''
            print('Filter ended ,starting to clean')
            self.CleanThreshold=PM.ReturnParameter('self.CleanThreshold')
            #self.CleanedData = self.FilteredData.apply(lambda x: self.CleanData(x, CleaningThreshold = CleanThreshold))
            self.CleanedData = self.LambdaReplace( self.FilteredData,1)
            if isinstance(self.CleanedData, bool): return False
            # only go on with calculation if all subsequences of calibration passed Cleaning process
            if self.CleanedData.Clean.all():
                Log.AddToLog('Cleaning occured and worked,start calculation '.format(self.CleanThreshold))

                '''--------------------------CalculateAdditionalParameters--------------------------------------------------------'''
                # define the number of samples for rolling mean window
                # TODO: should be changed to a fixed time and not samples

                #self.CleanedAddCalcData = self.CleanedData.reset_index(drop=True).groupby(self.FilterBy).apply(lambda x: self.CalculateAdditionalParameters(x,rollwindowsize))
                self.CleanedAddCalcData = self.LambdaReplace(self.CleanedData,2)
                if isinstance(self.CleanedAddCalcData, bool): return False

                '''--------------------------CalculateRawFeatures--------------------------------------------------------'''
                #self.RawFeatureData = self.CleanedAddCalcData.reset_index(drop=True).groupby(self.FilterBy).apply(lambda x: self.CalculateRawFeatures(x))
                self.RawFeatureData = self.LambdaReplace(self.CleanedAddCalcData , 3)
                if isinstance(self.RawFeatureData, bool): return False

                '''--------------------------FinalFeatures--------------------------------------------------------'''
                #TODO:FUTURE This is only needed when data frames are grouped and not calculated over the whole calibration sequence
                self.FinalFeatures = self.FuseCalibrationFeatures(self.RawFeatureData)
                if isinstance(self.FinalFeatures, bool): return False
                self.FinalFeatures = self.FinalFeatures.assign(Revision = self.revision)


                print(Fore.GREEN + 'DataSc: Creating Feature vector succeeded , Returning True' + Fore.RESET)
                return True
            else:
                # say to alon, I need new data because
                print(Fore.RED + 'Data is insufficent according to threshold  ,need to restart collecting data, returning False' + Fore.RESET)
                return False
        except Exception as e:
            print(Fore.RED + 'returning False, exaption' + Fore.RESET)
            Log.AddToLog("exception in runner.caliData :  {}".format(e), 2)
            return False

    def LambdaReplace(self, DataToFilter,flag):
        Log.AddToLog('LambdaReplace started flag equal = {}'.format(flag))
        try:
            if flag!=1:
                Data = DataToFilter.groupby(self.FilterBy)
            else:
                Data=DataToFilter
            DataFrameToReturn = pd.DataFrame()
            for keys, df in Data:
                if flag==1:
                    tempDf = self.CleanData(df, CleaningThreshold=self.CleanThreshold)
                    #print ('could not succeed cleaning')
                elif flag==2:
                    tempDf = self.CalculateAdditionalParameters(df , self.rollwindowlength)
                elif flag == 3:
                    tempDf = self.CalculateRawFeatures(df)
                if isinstance(tempDf, bool):
                    Log.AddToLog('LambdaReplace key = {} and flag = {} return False'.format(keys,flag))
                    print((Fore.RED + 'key = {} and flag = {} return False' + Fore.RESET).format(keys,flag))
                    return False
                else:
                    DataFrameToReturn = pd.concat([DataFrameToReturn, tempDf], axis=0)
            return DataFrameToReturn
        except Exception as e:
            Log.AddToLog("exception in LambdaReplace :  {}".format(e), 2)
            return False


    # Function to groupe data by Note column
    #self.FilterData is a group obkect meaning its a dict that the keys are the nots and the valuse in the corresponding df
    def Filter(self, Filter = None):
        Log.AddToLog('Filter started')
        try:
            DataToClean = self.MyData.copy()
            if Filter is None:
                self.FilteredData = DataToClean
            else:
                KeysName=[]
                DataToClean = DataToClean.groupby(Filter)
                #if DataToClean is an empty Group
                checkIfNotEmpty=(list(DataToClean))
                if not checkIfNotEmpty :
                    print(Fore.RED + 'DataToClean after group by is an empty DataFrame!!' + Fore.RESET)
                    Log.AddToLog('DataToClean after group by is a  empty DataFrame!!')
                    return False
                Log.AddToLog('Data Been split to the following groups: {}'.format(DataToClean.groups.keys()))
                print ('Data Been split to the following groups: {}'.format(DataToClean.groups.keys()))
                self.FilteredData = DataToClean
            return True
        except Exception as e:
            Log.AddToLog("exception in Filter :  {}".format(e), 2)
            return False




    def CleanData(self, DfToClean, CleaningThreshold):
        Log.AddToLog('CleanData started group : {} '.format(DfToClean[self.FilterBy].values[0]))
        try:
            #File Columns:
            '''
            TimeStamp, EyeTime, RawX, RawY, AverageX, AverageY, Left_RawX, Left_RawY, Left_PupilSize, Left_PupilCenterX, \
            Left_PupilCenterY, Left_PupilCenterZ, Right_RawX, Right_RawY, Right_PupilSize, Right_PupilCenterX, \
            Right_PupilCenterY, Right_PupilCenterZ, CorrectedX, CorrectedY, Note,
            '''
            # define columns that should be checked for outliers
            drop_names = ['RawX','RawY',
                          'Left_PupilSize','Right_PupilSize',
                          'Left_PupilCenterX','Left_PupilCenterY',
                          'Left_PupilCenterZ','Right_PupilCenterX',
                          'Right_PupilCenterY','Right_PupilCenterZ']

            raw_shape = DfToClean.shape
            # Remove duplicate rows ... introduced when Data Collection request is faster than the tracker
            DfToClean = DfToClean[~DfToClean.duplicated(subset='EyeTime')]
            row_miss_eyetime = ((raw_shape[0] - DfToClean.shape[0]) / raw_shape[0]) * 100
            Log.AddToLog('Percentage of duplicate EyeTime Rows : {} '.format(row_miss_eyetime))
            print(Fore.RED + 'Duplicates: {: 0.2f}% EyeTime of initial data shape'.format(
                row_miss_eyetime)+ Fore.RESET)

            # get initial data frame dimensions
            shape_initial = DfToClean.shape

            # Find 4 consecutive zeros in raw data and remove them from dataframe to avoid single eyetracking
            flagBool, DfToClean = self.get_consecutive_zeros(DfToClean, 4)
            if flagBool == False:
                return False
            # remove rows with for zeros


            # Remove outliers in all columns of interest
            DfToClean = DfToClean[DfToClean[drop_names].apply(lambda x: self.RemoveOutlier(x)).all(axis=1)]
            # Remove invalid pyhysiological values
            # remove pupil size < 1mm und > 9mm for left and right pupil
            # return all rows where left pupil size is between 1 and 9
            DfToClean = DfToClean[(DfToClean.Left_PupilSize > 1) & (DfToClean.Left_PupilSize < 9)]
            DfToClean.assign(Clean = pd.Series(np.nan))
            DfToClean.assign(Comment = pd.Series(np.nan))

            if DfToClean.empty:
                Log.AddToLog('No valid left pupil size data left, DfToClean.empty')
                return False
            DfToClean = DfToClean[(DfToClean.Right_PupilSize > 1) & (DfToClean.Right_PupilSize < 9)]
            if DfToClean.empty:
                Log.AddToLog('No valid right pupil size data left, DfToClean.empty')
                return False
            # remove all rows with NAs
            DfToClean = DfToClean.dropna(subset = drop_names)
            Log.AddToLog('CleanData main is finished, entering  IsDataAboveThreshold')
            return(self.IsDataAboveThreshold(DfToClean, CleaningThreshold,shape_initial))

        except Exception as e:
            Log.AddToLog("exception in CleanData :  {}".format(e), 2)
            return False



    def CalculateAdditionalParameters(self, dftocalculate, rollwindowlength):
        '''only if pupil values are available'''
        Log.AddToLog('CalculateAdditionalParameters Group name is : {}'.format(dftocalculate[self.FilterBy].values[0]))
        try:
            # Pupil Slopes Left and Right Eye
            slope_left = pd.Series(np.gradient(dftocalculate.Left_PupilSize),
                                   dftocalculate.TimeStamp, name='slope')
            slope_right = pd.Series(np.gradient(dftocalculate.Right_PupilSize),
                                    dftocalculate.TimeStamp, name='slope')
            # data validity check
            if slope_left.isnull().all() or slope_right.isnull().all() or slope_left.abs().sum() == 0 or slope_right.abs().sum() == 0:
                print(Fore.RED + 'Error in CalculateAdditionalParameters: invalid slope values introduced' + Fore.RESET)
                Log.AddToLog('Error in CalculateAdditionalParameters: invalid slope values introduced')
                return False
            else:
                print(Fore.GREEN + 'Check i.O. : slope l/r values in CalculateAdditionalParameters' + Fore.RESET)
                Log.AddToLog('Check i.O.: slope l/r values in CalculateAdditionalParameters')


            dftocalculate = dftocalculate.assign(Pupil_Slope_Right = slope_right.values)
            dftocalculate = dftocalculate.assign(Pupil_Slope_Left = slope_left.values)

            # rollmeans left centred Pupil Left and Right Eye
            rollwindowsize = np.round((dftocalculate.shape[0] * self.rollwindowlength)/dftocalculate.TimeStamp.diff().sum()).astype(np.int)


            s = rollwindowsize - 1
            roll_mean_left = pd.Series(dftocalculate.Left_PupilSize.rolling(window=rollwindowsize, axis=0).mean().shift(-s))
            roll_mean_right = pd.Series(dftocalculate.Right_PupilSize.rolling(window=rollwindowsize, axis=0).mean().shift(-s))
            dftocalculate = dftocalculate.assign(Pupil_Rollmean_Right=roll_mean_right.values)
            dftocalculate = dftocalculate.assign(Pupil_Rollmean_Left=roll_mean_left.values)

            '''
            # plots for debug
            plt.plot(dftocalculate.index.values, dftocalculate.Left_PupilSize)
            plt.plot(dftocalculate.index.values, roll_mean_left)
            plt.title(dftocalculate.Note.iloc[0])
            plt.ylim(2,6)
            plt.show()
            '''

            nanRatioLeft = dftocalculate.Pupil_Rollmean_Left.isna().sum()/len(dftocalculate.Pupil_Rollmean_Left)
            nanRatioRight = dftocalculate.Pupil_Rollmean_Right.isna().sum()/len(dftocalculate.Pupil_Rollmean_Right)

            # check validity of the data
            maxNATreshRatio = .5
            if (nanRatioLeft >= maxNATreshRatio) or (nanRatioRight >= maxNATreshRatio):
                print(Fore.RED + 'Error in CalculateAdditionalParameters: More than 50% of NAs introduced by Rollmean Pupilsize.'
                                 'Does rollmeanwindowsize match the Device type?' + Fore.RESET)
                Log.AddToLog('Error in CalculateAdditionalParameters: More than 50% of NAs introduced by Rollmean Pupilsize'
                             'Does rollmeanwindowsize match the Device type?')
                return False
            else:
                print(
                    Fore.GREEN + 'Check i.O. : NA ratio in CalculateAdditionalParameters' + Fore.RESET)
                Log.AddToLog('Check i.O.: NA roll ratio in CalculateAdditionalParameters')

            # check if to many NAs are produced by the rollmean window size
            # Pupil Slopes Left and Right Eye over Rollmean pupil values
            slope_rollmean_left = pd.Series(np.gradient(dftocalculate.Pupil_Rollmean_Left),
                                            dftocalculate.TimeStamp, name='slope')
            slope_rollmean_right = pd.Series(np.gradient(dftocalculate.Pupil_Rollmean_Right),
                                             dftocalculate.TimeStamp, name='slope')
            # data validity check
            if slope_rollmean_left.isnull().all() or slope_rollmean_right.isnull().all() or \
                    slope_rollmean_left.abs().sum() == 0 or slope_rollmean_right.abs().sum() == 0:
                print(Fore.RED + 'Error in CalculateAdditionalParameters: invalid rollmean slope values introduced' + Fore.RESET)
                Log.AddToLog('Error in CalculateAdditionalParameters: invalid rollmean slope values introduced')
                return False
            else:
                print(Fore.GREEN + 'Check i.O. : rollmean slope l/r values in CalculateAdditionalParameters' + Fore.RESET)
                Log.AddToLog('Check i.O.: rollmean slope l/r values in CalculateAdditionalParameters')

            dftocalculate = dftocalculate.assign(Pupil_Slope_Rollmean_Right=slope_rollmean_right.values)
            dftocalculate = dftocalculate.assign(Pupil_Slope_Rollmean_Left=slope_rollmean_left.values)


            ''' Should be available for all tracking systems '''
            # calculate 2D pupil distance
            try:
                pupdist2D = np.sqrt(((dftocalculate.Left_PupilCenterX - dftocalculate.Right_PupilCenterX) ** 2) +
                                    ((dftocalculate.Left_PupilCenterY - dftocalculate.Right_PupilCenterY) ** 2))
            except:
                pupdist2D=np.random.randint(1000,7000,len(dftocalculate.Left_PupilCenterX))

            dftocalculate = dftocalculate.assign(Pupil_Distance_2D=pupdist2D)

            # calculate 3D pupil distance
            try:
                pupdist3D = np.sqrt(((dftocalculate.Left_PupilCenterX -
                                      dftocalculate.Right_PupilCenterX) ** 2) +
                                    ((dftocalculate.Left_PupilCenterY -
                                      dftocalculate.Right_PupilCenterY) ** 2) +
                                    ((dftocalculate.Left_PupilCenterZ -
                                      dftocalculate.Right_PupilCenterZ) ** 2))
            except:
                pupdist3D = np.random.randint(1000, 7000, len(dftocalculate.Left_PupilCenterX))

            dftocalculate = dftocalculate.assign(Pupil_Distance_3D = pupdist3D)
            return dftocalculate
        except Exception as e:
            Log.AddToLog("exception in CalculateAdditionalParameters :  {}".format(e), 2)
            return False


    def CalculateRawFeatures(self, df):
        Log.AddToLog('CalculateRawFeatures Group name is : {} started'.format(df[self.FilterBy].values[0]))
        try:
            # calculate means, stds, median, min, max of all relevant features
            param_list = ['Left_PupilSize', 'Right_PupilSize',
                          'Pupil_Rollmean_Right', 'Pupil_Rollmean_Left',
                          'Pupil_Slope_Rollmean_Right',
                          'Pupil_Slope_Rollmean_Left', 'Pupil_Distance_2D',
                          'Pupil_Distance_3D', 'RawX', 'RawY']


            '''Calculate special parameters'''
            # slope of linear regression pupil change over time
            # linear regression slopes raw data
            lreg_leftpupil = linregress(df.TimeStamp, df.Left_PupilSize)
            lreg_rightpupil = linregress(df.TimeStamp, df.Right_PupilSize)
            lreg_leftpupil_slope = lreg_leftpupil.slope
            lreg_rightpupil_slope = lreg_rightpupil.slope
            # data validity check
            if np.isnan(lreg_leftpupil_slope) or np.isnan(lreg_rightpupil_slope) or \
                    np.sum(np.absolute(lreg_leftpupil_slope)) == 0 or np.sum(np.absolute(lreg_rightpupil_slope)) == 0:
                print(Fore.RED + 'Error in CalculateRawFeatures: invalid regression slope values introduced' + Fore.RESET)
                Log.AddToLog('Error in CalculateRawFeatures: invalid regression slope values introduced')
                return False
            else:
                print(Fore.GREEN + 'Check i.O. : regression slope values in CalculateRawFeatures' + Fore.RESET)
                Log.AddToLog('Check i.O. : regression slope values in CalculateRawFeatures')

            # linear regression slopes roll mean
            # hier müssen noch die NAs bereinigt werden
            Left_Clean = df[['TimeStamp', 'Pupil_Rollmean_Left']].dropna()
            Right_Clean = df[['TimeStamp', 'Pupil_Rollmean_Right']].dropna()

            lreg_leftpupil = linregress(Left_Clean)
            lreg_rightpupil = linregress(Right_Clean)
            lreg_leftpupil_roll_slope = lreg_leftpupil.slope
            lreg_rightpupil_roll_slope = lreg_rightpupil.slope

            # data validity check
            if np.isnan(lreg_leftpupil_roll_slope) or np.isnan(lreg_rightpupil_roll_slope) or \
                    np.sum(np.absolute(lreg_leftpupil_roll_slope)) == 0 or np.sum(np.absolute(lreg_rightpupil_roll_slope)) == 0:
                print(
                    Fore.RED + 'Error in CalculateRawFeatures: invalid rollmean regression slope values introduced' + Fore.RESET)
                Log.AddToLog('Error in CalculateRawFeatures: invalid rollmean regression slope values introduced')
                return False
            else:
                print(Fore.GREEN + 'Check i.O. : rollmean regression slope values in CalculateRawFeatures' + Fore.RESET)
                Log.AddToLog('Check i.O. : rollmean regression slope values in CalculateRawFeatures')

            # Correlation between left and Right Pupil Size
            lr_corr_pupilsize = np.ma.corrcoef(df.Left_PupilSize.values,
                                            df.Right_PupilSize.values)[0][1]

            # Correlation between left and Right Pupil Size roll means
            # remove NA values in rollmeans first. should be in the same places for both sides introduces by rollmean shift
            lr_corr_pupilsize_roll = np.corrcoef(df.Pupil_Rollmean_Left.dropna().values,
                                                 df.Pupil_Rollmean_Right.dropna().values)[0][1]

            # data validity check
            if np.isnan(lr_corr_pupilsize) or np.isnan(lr_corr_pupilsize_roll) or \
                    np.sum(np.absolute(lr_corr_pupilsize)) == 0 or np.sum(np.absolute(lr_corr_pupilsize_roll)) == 0:
                print(
                    Fore.RED + 'Error in CalculateRawFeatures: invalid l/r pupil corr values introduced' + Fore.RESET)
                Log.AddToLog('Error in CalculateRawFeatures: invalid l/r pupil corr values introduced')
                return False
            else:
                print(Fore.GREEN + 'Check i.O. : l/r pupil corr values in CalculateRawFeatures' + Fore.RESET)
                Log.AddToLog('Check i.O. : l/r pupil corr values in CalculateRawFeatures')

            # Fläche unter der Pupillen veränderungen
            sum_leftpupil = df.Left_PupilSize.sum()
            sum_rightpupil = df.Right_PupilSize.sum()
            sum_rollmean_leftpupil = df.Pupil_Rollmean_Left.sum()
            sum_rollmean_rightpupil = df.Pupil_Rollmean_Right.sum()




            # 2. Calculate BCEA
            flagBool, BCEA_68, BCEA_95, logBCEA_68, logBCEA_95 = self.calcBCEA(df)
            if flagBool == False:
                return False


            # Pupil Distance based on density
            flagBool, maxdense_dist_2D = self.calcPupDistfromDensity(df)
            if flagBool == False:
                return False


            # create series to later concatenate to other parameters
            parameter_add_values = pd.Series([lreg_leftpupil_slope,
                                              lreg_rightpupil_slope,
                                              lreg_leftpupil_roll_slope,
                                              lreg_rightpupil_roll_slope,
                                              sum_leftpupil, sum_rightpupil,
                                              sum_rollmean_leftpupil,
                                              sum_rollmean_rightpupil,
                                              lr_corr_pupilsize,
                                              lr_corr_pupilsize_roll,
                                              BCEA_68, BCEA_95,
                                              logBCEA_68, logBCEA_95,
                                              maxdense_dist_2D])

            # create series to later concatenate to other parameters
            parameter_add_names = ['Reg_slope_Left_PupilSize',
                                   'Reg_slope_Right_PupilSize',
                                   'Reg_slope_Pupil_Rollmean_Left',
                                   'Reg_slope_Pupil_Rollmean_Right',
                                   'Sum_Left_PupilSize',
                                   'Sum_Right_PupilSize',
                                   'Sum_Pupil_Rollmean_Left',
                                   'Sum_Pupil_Rollmean_Right',
                                   'Corr_LR_Pupilsize',
                                   'Corr_LR_Pupilsize_Roll',
                                   'BCEA_68', 'BCEA_95',
                                   'logBCEA_68', 'logBCEA_95',
                                   'Max_Dense_PupilDist_2D']


            '''Aggregate Features in param_list'''
            # calculate values
            mean_value = df[param_list].mean()
            median_value = df[param_list].median()
            std_value = df[param_list].std()
            min_value = df[param_list].min()

            # 10% of smallest values mean
            # TODO: order nach column ist irgendwie quatsch aber egal weil gleich der mean gebildet wird
            mean_min_value = df[param_list].dropna().nsmallest(np.round(df.shape[0]*.1).astype(np.int), columns=param_list).mean()
            max_value = df[param_list].max()

            # 10% of largest values mean
            mean_max_value = df[param_list].dropna().nlargest(np.round(df.shape[0]*.1).astype(np.int), columns=param_list).mean()

            parameter_values = pd.concat([mean_value, median_value, std_value,
                                          min_value, mean_min_value, max_value, mean_max_value,
                                          parameter_add_values])

            # create and transpose series to data frame
            df_feature_val = parameter_values.to_frame()
            df_feature_val = df_feature_val.T

            # concatenate the ID and the Value data frame
            # irgendwie müssen die indexe getdroppt werden sonst funktioniert das concat nicht und die dtypes gehen verloren
            df_feature = pd.DataFrame(df_feature_val)

            # , axis = 1, ignore_index=True
            # Define Parameter names
            param_list_mean_val=[]
            param_list_median_val=[]
            param_list_std_val=[]
            param_list_min_val=[]
            param_list_mean_min_val=[]
            param_list_max_val=[]
            param_list_mean_max_val=[]
            for s in param_list:
                param_list_mean_val.append('Mean_' + s)
                param_list_median_val.append('Median_' + s)
                param_list_std_val.append('Std_' + s)
                param_list_min_val.append('Min_' + s)
                param_list_mean_min_val.append('MeanMin_' + s)
                param_list_max_val.append('Max_' + s)
                param_list_mean_max_val.append('MeanMax_' + s)

            parameter_names = param_list_mean_val + \
                              param_list_median_val + param_list_std_val + param_list_min_val + \
                              param_list_mean_min_val + param_list_max_val + \
                              param_list_mean_max_val + parameter_add_names

            # rename data frame columns
            df_feature.columns = parameter_names
            df_feature = df_feature.assign(Note = df[self.FilterBy].values[0])
            # validate final feature data frame
            if df_feature.isna().values.any():
                print(Fore.RED + 'Error in CalculateRawFeatures : Calculation introduced NA feature values {}'.
                      format(df_feature.columns[df_feature.isna().any()].tolist())   + Fore.RESET)
                Log.AddToLog('Error in CalculateRawFeatures : Calculation introduced NA feature values')
                return False
            else:
                Log.AddToLog('CalculateRawFeatures Group name is : {} finished successfully'.format(df[self.FilterBy].values[0]))
                return df_feature
        except Exception as e:
            Log.AddToLog("exception in CalculateRawFeatures :  {}".format(e), 2)
            return False

    # Helper Functions:
    def ConcatAbsoluteValues(self, df, feature_list, seq_index):
        Log.AddToLog('ConcatAbsoluteValues started')
        try:
            feature_vals = []
            feature_names = []
            for feature in feature_list:
                feature_vals.append(df[df[self.FilterBy]== seq_index][feature])
                #print(type(df[feature].loc[seq_index]))
            return feature_vals
        except Exception as e:
            Log.AddToLog("exception in ConcatAbsoluteValues :  {}".format(e), 2)
            return False

    def ConcatDiffValues(self, df, feature_list, seq_index_main, seq_index_sub):
        Log.AddToLog('ConcatDiffValues started')
        try:
            feature_vals = []
            for feature in feature_list:
                feature_diff = df[df[self.FilterBy]== seq_index_main][feature] - df[df[self.FilterBy]== seq_index_sub][feature]
                feature_vals.append(feature_diff)
            return feature_vals
        except Exception as e:
            Log.AddToLog("exception in ConcatDiffValues :  {}".format(e), 2)
            return False

    def ConcatDiffValuesCombined(self, df, feature_list, seq_index_main1, seq_index_main2 ,seq_index_sub):
        Log.AddToLog('ConcatDiffValuesCombined started')
        try:
            feature_vals = []
            for feature in feature_list:
                feature_diff = ((df[df[self.FilterBy]== seq_index_main1][feature] + df[df[self.FilterBy]== seq_index_main2][feature])/2) - \
                               df[df[self.FilterBy] == seq_index_sub][feature]
                feature_vals.append(feature_diff)
            return feature_vals
        except Exception as e:
            Log.AddToLog("exception in ConcatDiffValuesCombined :  {}".format(e), 2)
            return False

    # TODO: Question to Alon:
    # Either only the current features used are calculated or all possible features are calculated and later selected
    def FuseCalibrationFeatures(self, dftofuse):
        Log.AddToLog('FuseCalibrationFeatures started ')
        try:
            # all available features right now
            all_features_list = dftofuse.columns


            # Absolute features

            lum_feature_list = ['Mean_Pupil_Distance_2D',
                                'Median_Pupil_Distance_2D',
                                'Std_Pupil_Distance_2D',
                                'Mean_Pupil_Distance_3D',
                                'Median_Pupil_Distance_3D',
                                'Std_Pupil_Distance_3D',
                                'Max_Dense_PupilDist_2D',
                                'BCEA_68',
                                'BCEA_95',
                                'logBCEA_68',
                                'logBCEA_95']

            pre_feature_list = ['BCEA_68',
                                'BCEA_95',
                                'logBCEA_68',
                                'logBCEA_95']

            post_feature_list = ['BCEA_68',
                                'BCEA_95',
                                'logBCEA_68',
                                'logBCEA_95']
            #Debug


            #Feature Values
            absLum_feature_list_val = self.ConcatAbsoluteValues(dftofuse, feature_list = lum_feature_list , seq_index = 'Brightness Change')
            # Feature_Names
            absLum_feature_list_names = ['AbsLum_' + s for s in lum_feature_list]

            absPre_feature_list_val = self.ConcatAbsoluteValues(dftofuse, feature_list=pre_feature_list,
                                                             seq_index='Pre Baseline')
            # Feature_Names
            absPre_feature_list_names = ['AbsPreBl_' + s for s in pre_feature_list]

            absPost_feature_list_val = self.ConcatAbsoluteValues(dftofuse, feature_list=post_feature_list,
                                                                seq_index='Post Baseline')
            # Feature_Names
            absPost_feature_list_names = ['AbsPostBl_' + s for s in post_feature_list]

            ''''''
            #Dummy List
            lum_to_pre_diff_feature_list = ['Mean_Left_PupilSize',
                                            'Mean_Right_PupilSize',
                                            'Std_Left_PupilSize',
                                            'Std_Right_PupilSize',
                                            'Median_Left_PupilSize',
                                            'Median_Right_PupilSize',
                                            'MeanMax_Left_PupilSize',
                                            'MeanMax_Right_PupilSize',
                                            'MeanMin_Left_PupilSize',
                                            'MeanMin_Right_PupilSize',
                                            'Sum_Left_PupilSize',
                                            'Sum_Right_PupilSize',
                                            'Reg_slope_Left_PupilSize',
                                            'Reg_slope_Right_PupilSize',
                                            'Corr_LR_Pupilsize',
                                            'Mean_Pupil_Rollmean_Right',
                                            'Mean_Pupil_Rollmean_Left',
                                            'Mean_Pupil_Slope_Rollmean_Right',
                                            'Mean_Pupil_Slope_Rollmean_Left',
                                            'Std_Pupil_Rollmean_Right',
                                            'Std_Pupil_Rollmean_Left',
                                            'Std_Pupil_Slope_Rollmean_Right',
                                            'Std_Pupil_Slope_Rollmean_Left',
                                            'Median_Pupil_Rollmean_Right',
                                            'Median_Pupil_Rollmean_Left',
                                            'Median_Pupil_Slope_Rollmean_Right',
                                            'Median_Pupil_Slope_Rollmean_Left',
                                            'MeanMin_Pupil_Rollmean_Right',
                                            'MeanMin_Pupil_Rollmean_Left',
                                            'MeanMin_Pupil_Slope_Rollmean_Right',
                                            'MeanMin_Pupil_Slope_Rollmean_Left',
                                            'MeanMax_Pupil_Rollmean_Right',
                                            'MeanMax_Pupil_Rollmean_Left',
                                            'MeanMax_Pupil_Slope_Rollmean_Right',
                                            'MeanMax_Pupil_Slope_Rollmean_Left',
                                            'Reg_slope_Pupil_Rollmean_Left',
                                            'Reg_slope_Pupil_Rollmean_Right',
                                            'Sum_Pupil_Rollmean_Left',
                                            'Sum_Pupil_Rollmean_Right',
                                            'Corr_LR_Pupilsize_Roll']
            # Feature Values
            diff_Lum_to_Pre_feature_list_val = self.ConcatDiffValues(dftofuse, feature_list = lum_to_pre_diff_feature_list,
                                                          seq_index_main = 'Brightness Change', seq_index_sub = 'Pre Baseline')
            # Feature_Names
            diff_Lum_to_Pre_feature_list_names = ['DiffLumToPre_' + s for s in lum_to_pre_diff_feature_list]

            ''''''
            #Dummy List
            post_to_pre_diff_feature_list = ['Mean_Left_PupilSize',
                                            'Mean_Right_PupilSize',
                                            'Std_Left_PupilSize',
                                            'Std_Right_PupilSize',
                                            'Median_Left_PupilSize',
                                            'Median_Right_PupilSize',
                                            'MeanMax_Left_PupilSize',
                                            'MeanMax_Right_PupilSize',
                                            'MeanMin_Left_PupilSize',
                                            'MeanMin_Right_PupilSize',
                                            'Sum_Left_PupilSize',
                                            'Sum_Right_PupilSize',
                                            'Reg_slope_Left_PupilSize',
                                            'Reg_slope_Right_PupilSize',
                                            'Corr_LR_Pupilsize',
                                            'Mean_Pupil_Rollmean_Right',
                                            'Mean_Pupil_Rollmean_Left',
                                            'Mean_Pupil_Slope_Rollmean_Right',
                                            'Mean_Pupil_Slope_Rollmean_Left',
                                            'Std_Pupil_Rollmean_Right',
                                            'Std_Pupil_Rollmean_Left',
                                            'Std_Pupil_Slope_Rollmean_Right',
                                            'Std_Pupil_Slope_Rollmean_Left',
                                            'Median_Pupil_Rollmean_Right',
                                            'Median_Pupil_Rollmean_Left',
                                            'Median_Pupil_Slope_Rollmean_Right',
                                            'Median_Pupil_Slope_Rollmean_Left',
                                            'MeanMin_Pupil_Rollmean_Right',
                                            'MeanMin_Pupil_Rollmean_Left',
                                            'MeanMin_Pupil_Slope_Rollmean_Right',
                                            'MeanMin_Pupil_Slope_Rollmean_Left',
                                            'MeanMax_Pupil_Rollmean_Right',
                                            'MeanMax_Pupil_Rollmean_Left',
                                            'MeanMax_Pupil_Slope_Rollmean_Right',
                                            'MeanMax_Pupil_Slope_Rollmean_Left',
                                            'Reg_slope_Pupil_Rollmean_Left',
                                            'Reg_slope_Pupil_Rollmean_Right',
                                            'Sum_Pupil_Rollmean_Left',
                                            'Sum_Pupil_Rollmean_Right',
                                            'Corr_LR_Pupilsize_Roll']
            # Feature Values
            diff_Post_to_Pre_feature_list_val = self.ConcatDiffValues(dftofuse, feature_list = post_to_pre_diff_feature_list,
                                                          seq_index_main = 'Post Baseline', seq_index_sub = 'Pre Baseline')
            # Feature_Names
            diff_Post_to_Pre_feature_list_names = ['DiffPostToPre_' + s for s in post_to_pre_diff_feature_list]

            ''''''
            # Dummy list
            lum_to_post_diff_feature_list = ['Mean_Left_PupilSize',
                                            'Mean_Right_PupilSize',
                                            'Std_Left_PupilSize',
                                            'Std_Right_PupilSize',
                                            'Median_Left_PupilSize',
                                            'Median_Right_PupilSize',
                                            'MeanMax_Left_PupilSize',
                                            'MeanMax_Right_PupilSize',
                                            'MeanMin_Left_PupilSize',
                                            'MeanMin_Right_PupilSize',
                                            'Sum_Left_PupilSize',
                                            'Sum_Right_PupilSize',
                                            'Reg_slope_Left_PupilSize',
                                            'Reg_slope_Right_PupilSize',
                                            'Corr_LR_Pupilsize',
                                            'Mean_Pupil_Rollmean_Right',
                                            'Mean_Pupil_Rollmean_Left',
                                            'Mean_Pupil_Slope_Rollmean_Right',
                                            'Mean_Pupil_Slope_Rollmean_Left',
                                            'Std_Pupil_Rollmean_Right',
                                            'Std_Pupil_Rollmean_Left',
                                            'Std_Pupil_Slope_Rollmean_Right',
                                            'Std_Pupil_Slope_Rollmean_Left',
                                            'Median_Pupil_Rollmean_Right',
                                            'Median_Pupil_Rollmean_Left',
                                            'Median_Pupil_Slope_Rollmean_Right',
                                            'Median_Pupil_Slope_Rollmean_Left',
                                            'MeanMin_Pupil_Rollmean_Right',
                                            'MeanMin_Pupil_Rollmean_Left',
                                            'MeanMin_Pupil_Slope_Rollmean_Right',
                                            'MeanMin_Pupil_Slope_Rollmean_Left',
                                            'MeanMax_Pupil_Rollmean_Right',
                                            'MeanMax_Pupil_Rollmean_Left',
                                            'MeanMax_Pupil_Slope_Rollmean_Right',
                                            'MeanMax_Pupil_Slope_Rollmean_Left',
                                            'Reg_slope_Pupil_Rollmean_Left',
                                            'Reg_slope_Pupil_Rollmean_Right',
                                            'Sum_Pupil_Rollmean_Left',
                                            'Sum_Pupil_Rollmean_Right',
                                            'Corr_LR_Pupilsize_Roll']

            # Feature Values
            diff_Lum_to_Post_feature_list_val = self.ConcatDiffValues(dftofuse, feature_list=lum_to_post_diff_feature_list,
                                                          seq_index_main='Brightness Change', seq_index_sub='Post Baseline')
            # Feature_Names
            diff_Lum_to_Post_feature_list_names = ['DiffLumToPost_' + s for s in lum_to_post_diff_feature_list]

            ''''''
            # Dummy list
            lumpost_to_pre_diff_feature_list = ['Mean_Left_PupilSize',
                                            'Mean_Right_PupilSize',
                                            'Std_Left_PupilSize',
                                            'Std_Right_PupilSize',
                                            'Median_Left_PupilSize',
                                            'Median_Right_PupilSize',
                                            'MeanMax_Left_PupilSize',
                                            'MeanMax_Right_PupilSize',
                                            'MeanMin_Left_PupilSize',
                                            'MeanMin_Right_PupilSize',
                                            'Sum_Left_PupilSize',
                                            'Sum_Right_PupilSize',
                                            'Reg_slope_Left_PupilSize',
                                            'Reg_slope_Right_PupilSize',
                                            'Corr_LR_Pupilsize',
                                            'Mean_Pupil_Rollmean_Right',
                                            'Mean_Pupil_Rollmean_Left',
                                            'Mean_Pupil_Slope_Rollmean_Right',
                                            'Mean_Pupil_Slope_Rollmean_Left',
                                            'Std_Pupil_Rollmean_Right',
                                            'Std_Pupil_Rollmean_Left',
                                            'Std_Pupil_Slope_Rollmean_Right',
                                            'Std_Pupil_Slope_Rollmean_Left',
                                            'Median_Pupil_Rollmean_Right',
                                            'Median_Pupil_Rollmean_Left',
                                            'Median_Pupil_Slope_Rollmean_Right',
                                            'Median_Pupil_Slope_Rollmean_Left',
                                            'MeanMin_Pupil_Rollmean_Right',
                                            'MeanMin_Pupil_Rollmean_Left',
                                            'MeanMin_Pupil_Slope_Rollmean_Right',
                                            'MeanMin_Pupil_Slope_Rollmean_Left',
                                            'MeanMax_Pupil_Rollmean_Right',
                                            'MeanMax_Pupil_Rollmean_Left',
                                            'MeanMax_Pupil_Slope_Rollmean_Right',
                                            'MeanMax_Pupil_Slope_Rollmean_Left',
                                            'Reg_slope_Pupil_Rollmean_Left',
                                            'Reg_slope_Pupil_Rollmean_Right',
                                            'Sum_Pupil_Rollmean_Left',
                                            'Sum_Pupil_Rollmean_Right',
                                            'Corr_LR_Pupilsize_Roll']
            # Feature Values
            diff_LumPost_to_Pre_feature_list_val = self.ConcatDiffValuesCombined(df = dftofuse,
                                                                            feature_list = lumpost_to_pre_diff_feature_list,
                                                                            seq_index_main1 = 'Brightness Change',
                                                                            seq_index_main2 = 'Post Baseline',
                                                                            seq_index_sub = 'Pre Baseline')
            # Feature_Names
            diff_LumPost_to_Pre_feature_list_names = ['DiffLumPostToPre_' + s for s in lumpost_to_pre_diff_feature_list]

            ''''''
            lumpre_to_post_diff_feature_list = ['Mean_Left_PupilSize',
                                            'Mean_Right_PupilSize',
                                            'Std_Left_PupilSize',
                                            'Std_Right_PupilSize',
                                            'Median_Left_PupilSize',
                                            'Median_Right_PupilSize',
                                            'MeanMax_Left_PupilSize',
                                            'MeanMax_Right_PupilSize',
                                            'MeanMin_Left_PupilSize',
                                            'MeanMin_Right_PupilSize',
                                            'Sum_Left_PupilSize',
                                            'Sum_Right_PupilSize',
                                            'Reg_slope_Left_PupilSize',
                                            'Reg_slope_Right_PupilSize',
                                            'Corr_LR_Pupilsize',
                                            'Mean_Pupil_Rollmean_Right',
                                            'Mean_Pupil_Rollmean_Left',
                                            'Mean_Pupil_Slope_Rollmean_Right',
                                            'Mean_Pupil_Slope_Rollmean_Left',
                                            'Std_Pupil_Rollmean_Right',
                                            'Std_Pupil_Rollmean_Left',
                                            'Std_Pupil_Slope_Rollmean_Right',
                                            'Std_Pupil_Slope_Rollmean_Left',
                                            'Median_Pupil_Rollmean_Right',
                                            'Median_Pupil_Rollmean_Left',
                                            'Median_Pupil_Slope_Rollmean_Right',
                                            'Median_Pupil_Slope_Rollmean_Left',
                                            'MeanMin_Pupil_Rollmean_Right',
                                            'MeanMin_Pupil_Rollmean_Left',
                                            'MeanMin_Pupil_Slope_Rollmean_Right',
                                            'MeanMin_Pupil_Slope_Rollmean_Left',
                                            'MeanMax_Pupil_Rollmean_Right',
                                            'MeanMax_Pupil_Rollmean_Left',
                                            'MeanMax_Pupil_Slope_Rollmean_Right',
                                            'MeanMax_Pupil_Slope_Rollmean_Left',
                                            'Reg_slope_Pupil_Rollmean_Left',
                                            'Reg_slope_Pupil_Rollmean_Right',
                                            'Sum_Pupil_Rollmean_Left',
                                            'Sum_Pupil_Rollmean_Right',
                                            'Corr_LR_Pupilsize_Roll']
            # Feature Values
            diff_LumPre_to_Post_feature_list_val = self.ConcatDiffValuesCombined(df=dftofuse,
                                                                                 feature_list=lumpost_to_pre_diff_feature_list,
                                                                                 seq_index_main1='Brightness Change',
                                                                                 seq_index_main2='Pre Baseline',
                                                                                 seq_index_sub='Post Baseline')
            # Feature_Names
            diff_LumPre_to_Post_feature_list_names = ['DiffLumPreToPost_' + s for s in lumpre_to_post_diff_feature_list]

            # combine all Features
            # Values
            final_feature_list_vals = absLum_feature_list_val + \
                                      absPre_feature_list_val +\
                                      absPost_feature_list_val + \
                                      diff_Lum_to_Pre_feature_list_val + \
                                      diff_Lum_to_Post_feature_list_val + \
                                      diff_Post_to_Pre_feature_list_val +\
                                      diff_LumPost_to_Pre_feature_list_val + \
                                      diff_LumPre_to_Post_feature_list_val
            # Names
            final_feature_list_names = absLum_feature_list_names +\
                                    absPre_feature_list_names + \
                                    absPost_feature_list_names + \
                                    diff_Lum_to_Pre_feature_list_names +\
                                    diff_Lum_to_Post_feature_list_names + \
                                    diff_Post_to_Pre_feature_list_names +\
                                    diff_LumPost_to_Pre_feature_list_names + \
                                    diff_LumPre_to_Post_feature_list_names
            
            df_final_feature = pd.DataFrame(final_feature_list_vals).T
            df_final_feature.columns = final_feature_list_names
            if df_final_feature.isna().values.any():
                print(Fore.RED + 'Error in FuseCalibrationFeatures : '
                                 'Calculation introduced NA feature values {}'
                      .format(df_final_feature.columns[df_final_feature.isna().any()].tolist()) + Fore.RESET)
                Log.AddToLog('Error in FuseCalibrationFeatures : Calculation introduced NA feature values')
                return False
            else:
                Log.AddToLog('FuseCalibrationFeatures finished successfully')
                return df_final_feature
        except Exception as e:
            Log.AddToLog("exception in FuseCalibrationFeatures :  {}".format(e), 2)
            return False



    def ReplacingLambda(self):
        df_list=[]
        drop_names = ['RawX','RawY',
                      'Left_PupilSize','Right_PupilSize',
                      'Left_PupilCenterX','Left_PupilCenterY',
                      'Left_PupilCenterZ','Right_PupilCenterX',
                      'Right_PupilCenterY','Right_PupilCenterZ']
        for Note,NoteDf in self.FilteredData:
            OneGroupAsDf=self.CleanData(NoteDf,0.7)
            if OneGroupAsDf['Clean']:
                df_list.append(OneGroupAsDf)
            else:
                return False
        MergeDF=pd.merge(df_list[0],df_list[1],df_list[2], on=drop_names)
        return MergeDF


    def calcBCEA(self, df):
        Log.AddToLog('calcBCEA started')
        try:
            ''' calculate bivariate contour elipse Area as measure of fixation stability nach vikesdal2016'''
            # (makes most sense in the brightness change condition)
            # 1. calc standard deviation of RawY and RawY (dont save as it is calculated anyways later)
            std_RawX = np.std(df.RawX)
            std_RawY = np.std(df.RawY)
            #std_RawX = 0
            #std_RawY = 0

            # data validity check
            if np.isnan(std_RawX) or np.isnan(std_RawY) or std_RawX == 0 or std_RawY == 0:
                print(Fore.RED + 'Error in CalculateRawFeatures: std of gaze x/y coordinates is {}/{}'
                      .format(std_RawX, std_RawY) + Fore.RESET)
                Log.AddToLog(
                    'Error in CalculateRawFeatures: std of gaze x/y coordinates is {}/{}'.format(std_RawX, std_RawY))
                return False, 0, 0, 0, 0

            k_68 = 2.28
            k_95 = 5.98
            # (product-moment correlation of the two position components)
            # product_moment_correlation
            pmc = np.corrcoef(df.RawX, df.RawY)[1, 0]

            BCEA_68 = k_68 * np.pi * (std_RawX * std_RawY) * np.sqrt(1 - np.power(pmc, 2))
            BCEA_95 = k_95 * np.pi * (std_RawX * std_RawY) * np.sqrt(1 - np.power(pmc, 2))
            logBCEA_68 = np.log(BCEA_68)
            logBCEA_95 = np.log(BCEA_95)

            return True, BCEA_68, BCEA_95, logBCEA_68, logBCEA_95

        except Exception as e:
            Log.AddToLog("exception in calcBCEA :  {}".format(e), 2)
            return False, 0, 0, 0, 0

    def calcPupDistfromDensity(self, df):
        Log.AddToLog('calcPupDistfromDensity started')
        try:
            #Left Pupil center estimation based on gaussian density kernel center
            xmin_left = df.Left_PupilCenterX.min()
            xmax_left = df.Left_PupilCenterX.max()
            ymin_left = df.Left_PupilCenterY.min()
            ymax_left = df.Left_PupilCenterY.max()

            X_left, Y_left = np.mgrid[xmin_left:xmax_left:100j, ymin_left:ymax_left:100j]
            positions_left = np.vstack([X_left.ravel(), Y_left.ravel()])
            values_left = np.vstack([df.Left_PupilCenterX, df.Left_PupilCenterY])
            kernel_left = stats.gaussian_kde(values_left)
            k_pos_left = kernel_left(positions_left)
            maxdense_pos_left = positions_left.T[np.argmax(k_pos_left)]

            # Right Pupil center estimation based on gaussian density kernel center
            xmin_right = df.Right_PupilCenterX.min()
            xmax_right = df.Right_PupilCenterX.max()
            ymin_right = df.Right_PupilCenterY.min()
            ymax_right = df.Right_PupilCenterY.max()

            X_right, Y_right = np.mgrid[xmin_right:xmax_right:100j, ymin_right:ymax_right:100j]
            positions_right = np.vstack([X_right.ravel(), Y_right.ravel()])
            values_right = np.vstack([df.Right_PupilCenterX, df.Right_PupilCenterY])
            kernel_right = stats.gaussian_kde(values_right)
            k_pos_right = kernel_right(positions_right)
            maxdense_pos_right = positions_right.T[np.argmax(k_pos_right)]

            maxdense_dist_2D = np.sqrt((maxdense_pos_right[0] - maxdense_pos_left[0]) ** 2 +
                                       (maxdense_pos_right[1] - maxdense_pos_left[1]) ** 2)


            return True, maxdense_dist_2D

        except Exception as e:
            Log.AddToLog("exception in calcPupDistfromDensity :  {}".format(e), 2)
            return False, 0


