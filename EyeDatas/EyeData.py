# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"
#EDITED: Alon Cohn 07.02.18




import platform
import time
from Utils import Log
import collections
import numpy as np
import pandas as pd

try:
    import win32api
except ImportError:
    try:
        from Quartz.CoreGraphics import *
    except ImportError:
        Log.AddToLog("Mouse control is not possible.")
        print("Mouse control not possible.")


from Configuration import ParameterManger as PM

class eyeData:
    def __init__(self,TrackerName,MoudleName,id,logstatus = "a"):
        Log.AddToLog("Init EyeData started")
        try:
            self.trackerFrameRate = PM.ReturnParameter("self.trackerFrameRate")
            self.SampleNumber = 0
            self.NewRow = []
            self.os_system = platform.system()
            self.time = time.time()
            self.Note = np.nan
            self.log=None    #logger1(TrackerName,MoudleName,id,logstatus)
            self.trackerRunning = False
            self.name = TrackerName
            self.XCordinate = 0
            self.YCordinate = 0
            self.eyeData = {}
            self.SampleNumber = 0
            self.EndWriting=False
            self.eyeDataOrderDict = collections.OrderedDict(
                {'EyeTime': 0,
                 'RawX': 0,
                 'RawY': 0,
                 'Left_RawX': 0,
                 'Left_RawY': 0,
                 'Left_PupilSize': 0,
                 'Left_PupilCenterX': 0,
                 'Left_PupilCenterY': 0,
                 'Left_PupilCenterZ': 0,
                 'Right_RawX': 0,
                 'Right_RawY': 0,
                 'Right_PupilSize': 0,
                 'Right_PupilCenterX': 0,
                 'Right_PupilCenterY': 0,
                 'Right_PupilCenterZ': 0})
            self.XList = np.array([])
            self.YList = np.array([])
            self.TimeList= np.array([])
            self.mycolumns = ['TimeStamp','EyeTime', 'RawX','RawY','Left_RawX','Left_RawY','Left_PupilSize','Left_PupilCenterX',
                 'Left_PupilCenterY','Left_PupilCenterZ','Right_RawX','Right_RawY','Right_PupilSize','Right_PupilCenterX',
                'Right_PupilCenterY', 'Right_PupilCenterZ','Note']
            self.EyeDataDF = pd.DataFrame(columns=self.mycolumns)
            self.raw_data_df = pd.DataFrame(columns=self.mycolumns)
        except Exception as e:
            Log.AddToLog("exception in __init__ EyeData Class :  {}".format(e), 2)

    def StartTracking(self,thread=None):
        Log.AddToLog("Thread-StartTracking.")
        try:
            self.trackerRunning = True
            while self.trackerRunning:
                start = time.perf_counter()
                if not self.PushData():   # Using a property of the son at the father might be risky.
                    self.stopTracking()
                    Log.AddToLog("Push data failed , stopping the thread self.trackerRunning = False ")
                if self.EndWriting:
                    self.stopTracking()
                runtime = time.perf_counter()-start
                if runtime < 1/self.trackerFrameRate:
                    time.sleep(1/self.trackerFrameRate-runtime)
        except Exception as e:
            Log.AddToLog("exception in StartTracking :  {}, changing tracker sample to False".format(e), 2)


    def stopTracking(self):
        Log.AddToLog("stopTracking activated.")
        self.trackerRunning = False

    def PushData(self):
        Log.AddToLog("PushData in eye data activated.")
        print("wring place ")
        Log.AddToLog("wrong place, push data was activated in EyeData ")
        pass








