__author__ = "Alon Cohn"
'''12.02
create an object call smi with parent Eyedata'''

from EyeDatas.EyeData import eyeData
from Utils import Log

# importing global varibale from the SMI
from EyeTrackerUtils.useTobii_minimalAW17 import g as gaze
# gaze[0]=The X cordinate on the scree which my right eye looking at; the dimension is in pixels!
# gaze[1]=The Y cordinate on the scree which my right eye looking at; the dimension is in pixels!
# gaze[2]=The X cordinate on the scree which my LEFT eye looking at; the dimension is in pixels!
# gaze[3]=The Y cordinate on the scree which my LEFT eye looking at; the dimension is in pixels!
from EyeTrackerUtils.useTobii_minimalAW17 import tstamp as timeStampe
from EyeTrackerUtils.useTobii_minimalAW17 import PupilDiam
from EyeTrackerUtils.useTobii_minimalAW17 import RightEyePos
from EyeTrackerUtils.useTobii_minimalAW17 import LeftEyePos

class tobi(eyeData):
    def __init__(self,name):
        super().__init__(name)
        self.name='TOBII'
        self.XCordinate=0
        self.YCordinate=0
        self.eyeData={}


    def ReadDataFromTobi(self):
        Log.AddToLog("ReadDataFromTobi started",1)
        try:
            self.XCordinate, self.YCordinate, self.eyeTimeStamp = (gaze[0] + gaze[2]) / 2, (
                        gaze[1] + gaze[3]) / 2, timeStampe
            self.rx, self.ry, self.lx, self.ly = gaze[0], gaze[1], gaze[2], gaze[3]
            self.rightPupilSize, self.leftPupilSize = PupilDiam[0], PupilDiam[1]
            self.PositionRightX, self.PositionRightY, self.PositionRightZ = RightEyePos[0], RightEyePos[1], RightEyePos[2]
            self.PositionLeftX, self.PositionLeftY, self.PositionLeftZ = LeftEyePos[0], LeftEyePos[1], LeftEyePos[2]
        except Exception as e:
            Log.AddToLog("exception in ReadDataFromTobi :  {}".format(e), 2)

    def PushData (self):
        Log.AddToLog("TOBII PushData started",1)
        try:
            self.ReadDataFromTobi()
            self.eyeData = {'EyeTime': str(self.eyeTimeStamp)[1:-1],
                            'RawX': self.XCordinate, 'RawY': self.YCordinate, 'AvarageX': 0, 'AvarageY': 0, 'Left_RawX': self.lx,
                            'Left_RawY': self.ly, 'Left_PupilSize': self.leftPupilSize, 'Left_PupilCenterX': self.PositionLeftX,
                            'Left_PupilCenterY': self.PositionLeftY, 'Left_PupilCenterZ': self.PositionLeftZ, 'Right_RawX': self.rx,
                            'Right_RawY': self.ry, 'Right_PupilSize': self.rightPupilSize,
                            'Right_PupilCenterX': self.PositionRightX, 'Right_PupilCenterY': self.PositionRightY, 'Right_PupilCenterZ': self.PositionRightZ}
            Log.AddToLog("Finished PushData  successfully eyeData= {}".format(self.eyeData), 1)
        except Exception as e:
            Log.AddToLog("exception in Tobii PushData :  {}".format(e), 2)