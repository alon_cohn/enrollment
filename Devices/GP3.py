__author__ = "Alon Cohn"




import numpy as np
from colorama import *
import pandas as pd
import sys
import time
import multiprocessing

from EyeDatas.EyeData import eyeData
from Utils import Log
from Configuration import ParameterManger as PM
from Configuration import Globales


path = PM.ReturnParameter("root_path")
sys.path.append(path + "gp3")
from gp3.ClientSocket import ClientGaze


class gp3(eyeData):
    def __init__(self,ChoosenTracker,moudle,id):
        super().__init__(ChoosenTracker,moudle,id)
        self.data_list = []
        self.gui_data_list = []
        self.trackerFrameRate= PM.ReturnParameter("self.trackerFrameRate")
        self.new_rows =[]
        self.LeftPupilSize = np.array([])
        self.RightPupilSize = np.array([])
        self.mycolumns = self.mycolumns + ['Fixation_X', 'Fixation_Y', 'Fixation_duration', 'Fixation_valid',
                                           'Right_raw_valid', 'Left_raw_valid', 'RawX_valid',
                                           'Right_pupil_valid', 'Left_pupil_valid',
                                           'Blink_id', 'Blink_duration', 'Blink_amount']
        self.EyeDataDF = pd.DataFrame(columns=self.mycolumns)
        self.raw_data_df = pd.DataFrame(columns=self.mycolumns)
        self.TimeUniqueness = 0
        self.thread_lock = None
        self.gettrace = getattr(sys, 'gettrace', None)
        self.gp3_process_activation()

        #self.answer_queue = multiprocessing.Queue()
        #self.connect()
        #tracking_thread_manager = Thread(target=self.is_EXIT)
        #tracking_thread_manager.start()

    def gp3_process_activation(self):
        """
        activating gp3 process!!!!!
        :return:
        """
        Log.AddToLog("gp3_connect started")
        try:
            manager = multiprocessing.Manager()
            self.value = multiprocessing.Value("i", 1)
            self.lock = multiprocessing.Lock()
            self.shared_list = manager.list([None])
            print(self.shared_list)
            tracking_process = multiprocessing.Process(target=ClientGaze, args=(self.shared_list, self.lock, '127.0.0.1', 4242))
            tracking_process.start()
            Globales.RUNNING_PROCESS.append(tracking_process)
            # wait until the process starts running.
            gp3_timeout = PM.ReturnParameter("gp3")["timeout"]
            delta = 0
            t0 =  time.time()
            while not self.shared_list[0]:
                if delta > gp3_timeout:
                    print("time out: no data enter the gp3 after {} please check the gp3.".format(gp3_timeout))
                    Log.AddToLog("time out: no data enter the gp3 after {} please check the gp3.")
                    Globales.EXIT = True
                    break
                time.sleep(0.001)
                delta = time.time() - t0
            Log.AddToLog("shard_list exist, is {} after {} sec".format(self.shared_list,delta))
        except Exception as e:
            Log.AddToLog("exception inz connect :  {}".format(e), 2)
            return False



    def StartTracking(self, thread_lock=None):
        Log.AddToLog("gp3 Thread StartTracking")
        try:
            self.trackerRunning = True
            while self.trackerRunning:
                start = time.perf_counter()
                if not self.PushData(thread_lock):   # Using a property of the son at the father might be risky.
                    self.stopTracking(thread_lock)
                    Log.AddToLog("Push data failed , stopping the thread self.trackerRunning = False ")
                if self.EndWriting:
                    self.stopTracking(thread_lock)
                runtime = time.perf_counter()-start
                if runtime < 1/self.trackerFrameRate:
                    time.sleep(1/self.trackerFrameRate-runtime)
        except Exception as e:
            Log.AddToLog("exception in StartTracking :  {}, changing tracker sample to False".format(e), 2)


    def stopTracking(self, thread_lock=None):
        Log.AddToLog("stopTracking activated.")
        try:
            if not (thread_lock is None):
                thread_lock.acquire()

            self.raw_data_df = pd.DataFrame(self.data_list, columns=self.mycolumns)
            self.EyeDataDF = pd.DataFrame(self.gui_data_list, columns=self.mycolumns)
            from tabulate import tabulate
            print(tabulate( self.EyeDataDF, headers='keys', tablefmt='psql'))
            #print(tabulate(self.raw_data_df, headers='keys', tablefmt='psql'))
            self.trackerRunning = False

            if not (thread_lock is None):
                thread_lock.release()
        except Exception as e:
            Log.AddToLog("exception in StopTracking :  {}".format(e), 2)
            if not (thread_lock is None):
                thread_lock.release()

    def PushData (self, thread_lock=None):
        """
        push data is activating read the data feom the share list and assign the answer
        into self.eyedata -  get a dict ( for gui neeeds)
        into self.      data that will be transfer to Data science needs
        :param thread_lock: mainly for validation, create a synchronizations between the collected data to the GUI
        :return:
        """
        Log.AddToLog("PushData started cycle number {}".format(self.SampleNumber))
        try:
            if not (thread_lock is None):
                thread_lock.acquire()

            t0 = time.time()
            #collecting data from the beackground process
            self.lock.acquire()
            gp3_answer = self.shared_list[0]
            self.lock.release()

            Log.AddToLog("to get answer from gp3( including geting the lock ) took : {}".format(time.time() - t0))
            Log.AddToLog("gp3answer:{}".format(gp3_answer))

            try:
                gp3_answer[0].append(self.Note)
                self.data_list.append(gp3_answer[0])
                #self.EyeDataDF.loc[len(self.EyeDataDF)] = gp3_answer[0]
            except:
                Log.AddToLog("gp3 could not obtin data from list, gp3 answer:{}".format(gp3_answer))
                print("problome at gp3")
            try:
                self.eyeData = gp3_answer[1]
                self.eyeData['Note'] = self.Note
                self.gui_data_list.append(self.eyeData)
                Log.AddToLog("self.eyeData has been update".format(self.eyeData))
            except:
                Log.AddToLog("gp3 could not obtin data from dict, gp3 answer:{}".format(gp3_answer))
                print("problome at gp3")

            if not (thread_lock is None):
                thread_lock.release()
            self.SampleNumber = self.SampleNumber + 1
            print("sample_number = {} ".format(self.SampleNumber))
            return True

        except Exception as e:
            if not (thread_lock is None):
                thread_lock.release()
            self.lock.release()
            Log.AddToLog("exception in PushData :  {}".format(e), 2)
            return False

    def WriteMyData(self):
        Log.AddToLog("WriteMyData started", 1)
        try:
            self.SampleNumber=self.SampleNumber+1
            self.log.writeTimeStamp()
            for keys in self.eyeDataOrderDict:
                self.log.writeLog(self.eyeData[keys])
            self.log.writeLog(self.Note)
            self.log.writeNewLine()
        except Exception as e:
            Log.AddToLog("exception in WriteMyData :  {}".format(e), 2)
            return False

    def IsDataValid(self, Width, Height):
        return True

    def IsDataValid1(self, Width, Height):
        '''the terms for valis Data:
        coordiantes are not ZERO
        Coordinates are in screen size
        mean pupil site are not Zero
        mean pupil size in range
        if df is to short

        '''
        Log.AddToLog('IsDataValid started')
        try:
            self.MinPupilSize=1
            self.MaxPupilSize = 10
            TimeThreshold = PM.ReturnParameter('TimeThreshold')
            ScreenThreshold=PM.ReturnParameter('ScreenThreshold')
            ErrCount=0

            #if data fram dont contain answers
            if self.EyeDataDF.shape[0] < 2 :
                print(Fore.RED + "Dataframe is empty!!!" + Fore.RESET)
                return False
            if  ((self.EyeDataDF.shape[0] < 5) and (not (self.gettrace()))) :
                print(Fore.RED + 'to little movement1 samples, something is wrong returning FALSE' + Fore.RESET)
                Log.AddToLog('to little movement1 samples, something is wrong returning FALSE')
                return False

            '''if 1-(self.TimeUniqueness/self.SampleNumber) <= TimeThreshold:
                print(Fore.RED + 'data is not valid,time is not unique' + Fore.RESET)
                Log.AddToLog("to much data is missing : {} data points from {} data points are missing thats :{} which is less than "
                             "the threshold of{}!".format(self.TimeUniqueness,self.SampleNumber,1-(self.TimeUniqueness/self.SampleNumber),TimeThreshold), 2)
                return False'''
            # if all values are zero
            if not np.any(self.XList) and not np.any(self.YList):
                print(Fore.RED + 'data is not valid,coordinate issue' + Fore.RESET)
                Log.AddToLog("the cordiante from the Tracker are all zero!", 2)
                return False
            # if all values are zero
            if not np.any(self.LeftPupilSize) and not np.any(self.RightPupilSize):
                print(Fore.RED + 'data is not valid,Pupil size is zero' + Fore.RESET)
                Log.AddToLog("the pupil size from the Tracker are all zero!", 2)
                return False
            #Coordinates are in screen size
            for cor in self.XList:
                if 0 > cor or cor > Width*1.5:
                    ErrCount=ErrCount+1
                    Log.AddToLog(
                        '{} Obtain coordiante outside the screen range, widthCor is: {} have to be between 0-{}'.format(
                            self.name, cor, Width), 2)

            if ErrCount > len(self.XList) * (1 - ScreenThreshold):
                print((Fore.RED + 'data is not valid,out of width coordinate range, amount of errors : {}' + Fore.RESET).format(
                    ErrCount))
                return False
            Log.AddToLog("The amount of width coor out of range is : {}".format(ErrCount), 2)
            ErrCount = 0

            for cor in self.YList:
                if 0 > cor or cor > Height*1.5:
                    ErrCount = ErrCount + 1
                    Log.AddToLog(
                        '{} Obtain coordiante outside the screen range, HeightCor is: {} have to be between 0-{}'.format(
                            self.name, cor, Height), 2)
            if ErrCount > len(self.YList) * (1 - ScreenThreshold):
                print((Fore.RED + 'data is not valid,out of hight coordinate range,amount of errors : {}' + Fore.RESET).format(
                    ErrCount))
                return False
            ErrCount = 0
            Log.AddToLog("The amount of hight coor out of range is : {}".format(ErrCount), 2)

            #mean pupil size in range
            MeanL=np.mean(self.LeftPupilSize)
            MeanR=np.mean(self.RightPupilSize)
            if self.MinPupilSize> MeanL or self.MaxPupilSize<MeanL or self.MinPupilSize> MeanR or self.MaxPupilSize<MeanR:
                print(Fore.RED + 'data is not valid,pupil size not in range' + Fore.RESET)
                Log.AddToLog('pupil mean size outside the allowed range, have to be between {}-{}, size is L:{} R:{}'.
                             format(self.MinPupilSize,self.MaxPupilSize,MeanL,MeanR), 2)
                return False
            Log.AddToLog('Data is Valid! returning True')
            return True
        except Exception as e:
            Log.AddToLog(" exception in IsDataValid :  {}".format(e), 2)
            return False

    def Reset(self):
        Log.AddToLog('Reset started')
        try:
            self.SampleNumber=0
            self.TimeUniqueness = 0
            self.XList = np.array([])
            self.YList = np.array([])
            #self.eyeData.fromkeys(eyeData, 0)
            self.EndWriting = False
            self.LeftPupilSize = np.array([])
            self.RightPupilSize = np.array([])
            self.data_list = []
            self.gui_data_list = []
            self.TimeList = np.array([])
            self.Note = np.nan
            del self.raw_data_df
            del self.EyeDataDF
            self.EyeDataDF = pd.DataFrame(columns=self.mycolumns)
            self.raw_data_df = pd.DataFrame(columns=self.mycolumns)

        except Exception as e:
            Log.AddToLog(" exception in Reset :  {}".format(e), 2)









if __name__ == '__main__':
    g = gp3("gp3","cali","22")
    t0 = time.time()
    delta = 0
    time.sleep(0.1)
    while delta<4 :
        g.PushData()
        delta = time.time() - t0
    g.gp3.stop_tracking()


"""


    def tracking_thread_mangment(self):
        
        Log.AddToLog("ready_for_activation started")
        try:
            i = 0
            already_tracking = False
            self.event.wait()
            while self.event.is_set():
                if self.trackerRunning and not already_tracking:
                    print("try")
                    gp3_start_tracking = Thread(target=self.gp3.start_tracking)
                    gp3_start_tracking.start()
                    already_tracking = True
                    print('start tracking')
                elif not self.trackerRunning and already_tracking:
                    self.gp3.stop_tracking()
                    already_tracking = False
                    is_alive = True
                    while is_alive:
                        is_alive = gp3_start_tracking.isAlive()
                        print(is_alive)
                        time.sleep(0.01)
                    del gp3_start_tracking
                    gp3_start_tracking =  None
                    print("stop tracking, ran in tracking_thread_mangment for {} loop cycle".format(i))
                    Log.AddToLog("stop tracking, ran in tracking_thread_mangment for {} loop cycle".format(i))
                i = i+1
                time.sleep(0.01)
            print("thread tracking_thread_mangment is being close")

        except Exception as e:
            Log.AddToLog("exception in ready_for_activation :  {}".format(e), 2)
            return False
            
        # NewRow = [time.time(), data['TIME_TICK'],data['BPOGX'], data['BPOGY'],
                #                data['LPOGX'], data['LPOGY'],data['LPD'], data['LEYEX'], data['LEYEY'], data['LEYEZ'],
                #                data['RPOGX'], data['RPOGY'], data['RPD'], data['REYEX'], data['REYEY'],data['REYEZ'],
                #                self.Note]
                
                
    ----------------------------------------------------------------------------------------------------            
        def ReadData(self):
            Log.AddToLog("ReadData started sampleNumber = {}".format(self.SampleNumber),1)

            # Legend:
            #     POG - position of gaze as a fraction of the screen size
            #     BPOG_X/Y : BPOGY - the best POG data which is avarge of the left eye and the right eye.if avg is not
            #     available last result as a fraction of the screen size
            #     L/R_POG_X/Y : LPOGX - the POGdata for the user L/R eye as a fraction of the screen size
            #     L/R_PD:LPD - the diameter of the left eye pupil in pixels
            #     R_EYE_X : REYEX - the x y z corrdiantes of the right eye with 3d space with respect tot the camera focal point in meters!
            #
            # "as a fraction of the screen size" -  it is precentage (0-1) of the screen size
            # "3d space with respect tot the camera focal point in meters!" - some where in the camera lies the focol point

            try:
                new_rows = []
                while True:
                    if not self.answer_queue.empty():
                        break
    
                rows_of_data = self.get_data_from_gp3()
                if isinstance(rows_of_data, bool):
                    return False
                for info in rows_of_data:
                    if not info:
                        continue
                    data = self.transfer_data_to_dict(info)
                    if isinstance(data, bool):
                        continue
                    data_NewRow = self.insert_data(data)
                    if isinstance(data_NewRow, bool):
                        continue
                    new_rows.append(data_NewRow)
                return new_rows
            except Exception as e:
                Log.AddToLog("exception in ReadData :  {}".format(e), 2)
                return False

    def get_data_from_gp3(self):
        
        # collect the data
        # check if the data is not an empty list
        # in case there is more then 3 errors will return false@!
        return: dat in a list, each raw is a gp3 smaple
        
        Log.AddToLog("get_data_from_gp3 started", 1)
        try:
            err = 0
            rows_of_data = None
            while err < 3:
                gp3_answer = self.answer_queue.get()
                if not gp3_answer:
                    print("data is empty! err number {}".format(err))
                    Log.AddToLog("data is empty! err number {}".format(err))
                    err += 1
                    if err > 2:
                        Log.AddToLog("3 errors in data collection form gp3 ! return false".format(err))
                        return False
                else:
                    rows_of_data = gp3_answer.split("\n")
                    break
            return rows_of_data
        except Exception as e:
            Log.AddToLog("exception in get_data_from_gp3 :  {}".format(e), 2)
            return False

    def transfer_data_to_dict(self, info):
        
        transfer the data from string to doctionary
        
        data = {}
        Log.AddToLog("transfer_data_to_dict started", 1)
        try:
            values = info.split(" ")
            if values[0] == "<ACK" or values[-1] != "/>\r":
                # print("no end ot start, the line {} is being ignore".format(info))
                Log.AddToLog("no end or no start to the string information  =  {}, it is being ignore".format(info))
                return False
            else:
                values.pop(0)
                values.pop(-1)
                for value in values:
                    key, item = value.split("=")
                    try:
                        data[key] = float(item.replace('"', ''))
                    except ValueError:
                        #print("encouter value problom at:{}".format(value))
                        Log.AddToLog("encouter value problom at  {} ".format(value))
                        data[key] = 0.0
            return data
        except Exception as e:
            Log.AddToLog("exception in transfer_data_to_dict :  {}".format(e), 2)
            return False

    def insert_data(self, data):
        Log.AddToLog("insert_data started", 1)
        try:
            self.eyeTimeStamp = data['TIME_TICK']
            #if not self.IfTimeUniqe():
            #    return False

            self.XCordinate, self.YCordinate = float(data['BPOGX']), float(data['BPOGY'])
            self.rx, self.ry = float(data['RPOGX']), float(data['RPOGY'])
            self.lx, self.ly = float(data['LPOGX']), float(data['LPOGY'])
            self.rightPupilSize, self.leftPupilSize = float(data['RPD']), float(data['LPD'])
            self.PositionRightX, self.PositionRightY, self.PositionRightZ = float(data['REYEX']), \
                                                                            float(data['REYEY']), \
                                                                            float(data['REYEZ'])
            self.PositionLeftX, self.PositionLeftY, self.PositionLeftZ = float(data['LEYEX']), \
                                                                         float(data['LEYEY']), \
                                                                         float(data['LEYEZ'])

            NewRow = [time.time(), self.eyeTimeStamp, self.XCordinate, self.YCordinate, self.lx, self.ly,
                      self.leftPupilSize, self.PositionLeftX, self.PositionLeftY, self.PositionLeftZ,
                      self.rx, self.ry, self.rightPupilSize, self.PositionRightX, self.PositionRightY,
                      self.PositionRightZ, self.Note]
            return NewRow
        except Exception as e:
            Log.AddToLog("exception in insert_data :  {}".format(e), 2)
            return False
                
    
    def is_EXIT(self):
        
        start tracking
        :return:
        try:
            in_use = 0
            while True:
                if Globales.EXIT:
                    try:
                        tracking_thread.terminate()
                    except:
                        print("exit, not thread to stop")
                    Log.AddToLog("EXIT = TRUE, stoping thread")
                    print("EXIT = TRUE, stoping thread")
                    break
                if self.trackerRunning and not in_use:
                    print("startTracking = True strating to collect data from GP3")
                    tracking_thread = multiprocessing.Process(target=ClientGaze, args=(self.answer_queue, self.lock, '127.0.0.1',4242))
                    tracking_thread.start()
                    in_use = 1
                elif not self.trackerRunning and in_use:
                    in_use = 0
                    tracking_thread.terminate()
                    i=0
                    while not self.answer_queue.empty():
                        print(self.answer_queue.get())
                        print(i)
                        i+=1
                    print("stoped thread")
                    Log.AddToLog("stoped thread")
                time.sleep(0.5)
            print("thread is_exit is being close")
            Log.AddToLog("thread is_exit is being close")
        except Exception as e:
            Log.AddToLog("exception in ready_for_activation :  {}".format(e), 2)
            return False
                
                
                
                
"""







