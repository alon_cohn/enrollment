__author__ = "Alon Cohn"
'''12.02
create an object call smi with parent Eyedata'''
import collections
import numpy as np
from colorama import *
import pandas as pd
import sys
import time


from EyeDatas.EyeData import eyeData
from Utils import Log
# importing global varibale from the SMI
from EyeTrackerUtils.useTobii_minimal import g as gaze
# gaze[0]=The X cordinate on the scree which my right eye looking at; the dimension is in pixels!
# gaze[1]=The Y cordinate on the scree which my right eye looking at; the dimension is in pixels!
# gaze[2]=The X cordinate on the scree which my LEFT eye looking at; the dimension is in pixels!
# gaze[3]=The Y cordinate on the scree which my LEFT eye looking at; the dimension is in pixels!
from EyeTrackerUtils.useTobii_minimal import tstamp as timeStampe
from EyeTrackerUtils.useTobii_minimal import PupilDiam
from EyeTrackerUtils.useTobii_minimal import RightEyePos
from EyeTrackerUtils.useTobii_minimal import LeftEyePos
from Configuration import ParameterManger as PM
from EyeTrackerUtils import useTobii_minimal
#know if we are in debud mode




class smi(eyeData):
    def __init__(self,ChoosenTracker,moudle,id):
        super().__init__(ChoosenTracker,moudle,id)
        self.trackerFrameRate=60
        self.pupilLeft = 3.5
        self.pupilRight = 3.5
        self.LeftPupilSize = np.array([])
        self.RightPupilSize = np.array([])
        self.TimeUniqueness = 0
        self.gettrace = getattr(sys, 'gettrace', None)
        self.connect()

    def connect(self):
        """
        connecting to the smi using the tobi minimal
        :return:
        """
        Log.AddToLog("smi_connect started")
        try:
            SMIConnected = useTobii_minimal.IsSMIConected()  # Check if there is a SMI DEVICE
            if SMIConnected:
                Log.AddToLog("SMIConnected={}, SMI was chosen".format(SMIConnected), 1)
        except Exception as e:
            Log.AddToLog("exception inz ReadDataFromSmi :  {}".format(e), 2)
            return False


    def ReadDataFromSmi(self):
        Log.AddToLog("ReadDataFromSmi started sampleNumber = {}".format(self.SampleNumber),1)
        try:
            #in case the smi is not in order it will give the same time stam
            #chekcing the time time stamp is not repeating itself.
            self.eyeTimeStamp=timeStampe[0]
            self.IfTimeUniqe()

            self.XCordinate, self.YCordinate = (gaze[0] + gaze[2]) / 2, (
                        gaze[1] + gaze[3]) / 2
            self.rx, self.ry, self.lx, self.ly = gaze[0], gaze[1], gaze[2], gaze[3]
            self.rightPupilSize, self.leftPupilSize = PupilDiam[0], PupilDiam[1]
            self.PositionRightX, self.PositionRightY, self.PositionRightZ = RightEyePos[0], RightEyePos[1], RightEyePos[2]
            self.PositionLeftX, self.PositionLeftY, self.PositionLeftZ = LeftEyePos[0], LeftEyePos[1], LeftEyePos[2]

            self.NewRow = [time.time(), self.eyeTimeStamp, self.XCordinate, self.YCordinate, self.lx, self.ly,
                           self.leftPupilSize, self.PositionLeftX, self.PositionLeftY, self.PositionLeftZ,
                           self.rx, self.ry, self.rightPupilSize, self.PositionRightX, self.PositionRightY,
                           self.PositionRightZ, self.Note]

            return self.NewRow
        except Exception as e:
            Log.AddToLog("exception in ReadDataFromSmi :  {}".format(e), 2)
            return False

    def PushData (self):
        Log.AddToLog("SmiPushData started",1)
        try:
            #start = time.perf_counter()
            newRow =  self.ReadDataFromSmi()
            if newRow == False:
                raise Exception
            self.EyeDataDF.loc[len(self.EyeDataDF)] = newRow
            #stop = time.perf_counter() - start
            #print("Time to write data to df = {}".format(stop))

            self.XList = np.append(self.XList, float(self.XCordinate))
            self.YList = np.append(self.YList, float(self.YCordinate))
            self.LeftPupilSize = np.append(self.LeftPupilSize,float(self.leftPupilSize ))
            self.RightPupilSize = np.append(self.RightPupilSize,float(self.rightPupilSize))
            self.eyeData = {'EyeTime': str(self.eyeTimeStamp)[1:-1],
                            'RawX': self.XCordinate, 'RawY': self.YCordinate,'Left_RawX': self.lx,
                            'Left_RawY': self.ly, 'Left_PupilSize': self.leftPupilSize, 'Left_PupilCenterX': self.PositionLeftX,
                            'Left_PupilCenterY': self.PositionLeftY, 'Left_PupilCenterZ': self.PositionLeftZ, 'Right_RawX': self.rx,
                            'Right_RawY': self.ry, 'Right_PupilSize': self.rightPupilSize,
                            'Right_PupilCenterX': self.PositionRightX, 'Right_PupilCenterY': self.PositionRightY, 'Right_PupilCenterZ': self.PositionRightZ}

            self.SampleNumber = self.SampleNumber + 1

            return True
        except Exception as e:
            Log.AddToLog("exception in SmiPushData :  {}".format(e), 2)
            return False

    def WriteMyData(self):
        Log.AddToLog("WriteMyData started", 1)
        try:
            self.SampleNumber=self.SampleNumber+1
            self.log.writeTimeStamp()
            for keys in self.eyeDataOrderDict:
                self.log.writeLog(self.eyeData[keys])
            self.log.writeLog(self.Note)
            self.log.writeNewLine()
        except Exception as e:
            Log.AddToLog("exception in WriteMyData :  {}".format(e), 2)
            return False

    def IsDataValid(self, Width, Height):
        '''the terms for valis Data:
        coordiantes are not ZERO
        Coordinates are in screen size
        mean pupil site are not Zero
        mean pupil size in range
        if df is to short

        '''
        try:
            Log.AddToLog('IsDataValid started')
            self.MinPupilSize=1
            self.MaxPupilSize = 10
            TimeThreshold = PM.ReturnParameter('TimeThreshold')
            ScreenThreshold=PM.ReturnParameter('ScreenThreshold')
            ErrCount=0
            if self.EyeDataDF.shape[0] < 2 :
                print(Fore.RED + "Dataframe is empty!!!" + Fore.RESET)
                return False
            if  ((self.EyeDataDF.shape[0] < 5) and (not (self.gettrace()))) :
                print(Fore.RED + 'to little movement1 samples, something is wrong returning FALSE' + Fore.RESET)
                Log.AddToLog('to little movement1 samples, something is wrong returning FALSE')
                return False

            '''if 1-(self.TimeUniqueness/self.SampleNumber) <= TimeThreshold:
                print(Fore.RED + 'data is not valid,time is not unique' + Fore.RESET)
                Log.AddToLog("to much data is missing : {} data points from {} data points are missing thats :{} which is less than "
                             "the threshold of{}!".format(self.TimeUniqueness,self.SampleNumber,1-(self.TimeUniqueness/self.SampleNumber),TimeThreshold), 2)
                return False'''
            if not np.any(self.XList) and not np.any(self.YList):  # if all values are zero
                print(Fore.RED + 'data is not valid,coordinate issue' + Fore.RESET)
                Log.AddToLog("the cordiante from the Tracker are all zero!", 2)
                return False
            if not np.any(self.LeftPupilSize) and not np.any(self.RightPupilSize):  # if all values are zero
                print(Fore.RED + 'data is not valid,Pupil size is zero' + Fore.RESET)
                Log.AddToLog("the pupil size from the Tracker are all zero!", 2)
                return False
            for cor in self.XList:
                if 0 > cor or cor > Width*1.5:
                    ErrCount=ErrCount+1
                    Log.AddToLog(
                        '{} Obtain coordiante outside the screen range, widthCor is: {} have to be between 0-{}'.format(
                            self.name, cor, Width), 2)
            if ErrCount > len(self.XList) * (1 - ScreenThreshold):
                print((Fore.RED + 'data is not valid,out of width coordinate range, amount of errors : {}' + Fore.RESET).format(
                    ErrCount))
                return False
            Log.AddToLog("The amount of width coor out of range is : {}".format(ErrCount), 2)
            ErrCount = 0
            for cor in self.YList:
                if 0 > cor or cor > Height*1.5:
                    ErrCount = ErrCount + 1
                    Log.AddToLog(
                        '{} Obtain coordiante outside the screen range, HeightCor is: {} have to be between 0-{}'.format(
                            self.name, cor, Height), 2)
            if ErrCount > len(self.YList) * (1 - ScreenThreshold):
                print((Fore.RED + 'data is not valid,out of hight coordinate range,amount of errors : {}' + Fore.RESET).format(
                    ErrCount))
                return False
            ErrCount = 0
            Log.AddToLog("The amount of hight coor out of range is : {}".format(ErrCount), 2)
            MeanL=np.mean(self.LeftPupilSize)
            MeanR=np.mean(self.RightPupilSize)
            if self.MinPupilSize> MeanL or self.MaxPupilSize<MeanL or self.MinPupilSize> MeanR or self.MaxPupilSize<MeanR:
                print(Fore.RED + 'data is not valid,pupil size not in range' + Fore.RESET)
                Log.AddToLog('pupil mean size outside the allowed range, have to be between {}-{}, size is L:{} R:{}'.
                             format(self.MinPupilSize,self.MaxPupilSize,MeanL,MeanR), 2)
                return False
            Log.AddToLog('Data is Valid! returning True')
            return True
        except Exception as e:
            Log.AddToLog(" exception in IsDataValid :  {}".format(e), 2)
            return False

    def Reset(self):
        Log.AddToLog('Reset started')
        try:
            self.SampleNumber=0
            self.TimeUniqueness = 0
            self.XList = np.array([])
            self.YList = np.array([])
            #self.eyeData.fromkeys(eyeData, 0)
            self.EndWriting = False
            self.LeftPupilSize = np.array([])
            self.RightPupilSize = np.array([])
            self.TimeList = np.array([])

            del self.EyeDataDF
            self.EyeDataDF = pd.DataFrame(columns=self.mycolumns)

        except Exception as e:
            Log.AddToLog(" exception in Reset :  {}".format(e), 2)


    def IfTimeUniqe(self):

        Log.AddToLog("IfTimeUniqe started", 1)
        try:
            if self.eyeTimeStamp in self.TimeList:
                Log.AddToLog("Timestamp : {} is allready exist SMI send False data, sample number= {}".format(
                    self.eyeTimeStamp,self.SampleNumber), 2)
                self.TimeUniqueness+=1
            else:
                self.TimeList = np.append(self.TimeList, float(self.eyeTimeStamp))
        except Exception as e:
            Log.AddToLog("exception in IfTimeUniqe :  {}".format(e), 2)
            return False

