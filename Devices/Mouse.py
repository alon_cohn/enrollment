__author__ = "Alon Cohn"
'''09.02
create an object call mouse with parent Eyedata'''

import win32api
import pyautogui
from EyeDatas.EyeData import eyeData
import datetime
from Utils import Log
from numpy.random import random_integers as Irand
from numpy.random import random_sample as frand
import numpy as np
import collections
from random import uniform as rand
import pandas as pd
import time
import multiprocessing

class mouse(eyeData):
    def __init__(self,ChoosenTracker,moudle,id):
        super().__init__(ChoosenTracker,moudle,id)
        self.trackerFrameRate=60
        self.lock = multiprocessing.Lock()

    def GetMousePosition(self):
        Log.AddToLog("GetMousePosition started", 1)
        try:
            if self.os_system == "Windows":

                self.XCordinate,self.YCordinate = win32api.GetCursorPos()
                EyeTime = datetime.datetime.now()

                Left_RawX= rand(600,900) ; Left_RawY= rand(150,250) ; Left_PupilSize= 7*frand()
                Left_PupilCenterX= rand(-50, -25) ; Left_PupilCenterY= rand(25, 50) ;  Left_PupilCenterZ= rand(450, 650)
                Right_RawX= rand(600,900); Right_RawY= rand( 150, 250); Right_PupilSize= 7*frand()
                Right_PupilCenterX= rand(25, 50); Right_PupilCenterY= rand(25, 50); Right_PupilCenterZ= rand(450, 650)

                self.NewRow=[time.time(),EyeTime,  frand()*self.XCordinate,   frand()*self.YCordinate, Left_RawX,Left_RawY,
                                   Left_PupilSize,Left_PupilCenterX,Left_PupilCenterY, Left_PupilCenterZ,
                Right_RawX, Right_RawY,Right_PupilSize,Right_PupilCenterX, Right_PupilCenterY, Right_PupilCenterZ,self.Note]

                self.XList = np.append(self.XList, float(self.XCordinate))
                self.YList = np.append(self.YList, float(self.YCordinate))
            return True
        except Exception as e:
            Log.AddToLog("exception in GetMousePosition :  {}".format(e), 2)
            return False

    def PushData (self):
        #IF the Data is not coming from a thread WhereDataComeFrom should NOT be None
        Log.AddToLog("PushData started, sampleNumber = {}".format(self.SampleNumber),1)
        try:
            if not self.GetMousePosition():
                Log.AddToLog("GetMousePosition return False")
                return False
            self.EyeDataDF.loc[len(self.EyeDataDF)] = self.NewRow

            self.eyeData = {
                'EyeTime': str(datetime.datetime.now()).replace(":", "").replace(" ", "").replace(".", "").replace("-", ""),
                'RawX': rand(1,5)*self.XCordinate, 'RawY': rand(1,5)*self.YCordinate,'Left_RawX': rand(1,9),
                'Left_RawY': rand(1,9), 'Left_PupilSize': rand(1,9), 'Left_PupilCenterX': rand(-30,-20),
                'Left_PupilCenterY': 2, 'Left_PupilCenterZ': rand(1,9), 'Right_RawX': rand(1,9), 'Right_RawY': rand(1,9), 'Right_PupilSize': rand(1,9),
                'Right_PupilCenterX': rand(20,30), 'Right_PupilCenterY': 2, 'Right_PupilCenterZ': rand(400,650)}
            self.SampleNumber = self.SampleNumber + 1
            #prinitng the Valus to Data
            #self.WriteMyData()
            return True
        except Exception as e:
            Log.AddToLog(" exception in PushData :  {}".format(e), 2)
            return False

    def WriteMyData (self):
        Log.AddToLog("WriteMyData started", 1)
        try:
            self.SampleNumber = self.SampleNumber + 1
            self.log.writeTimeStamp()
            for keys in self.eyeDataOrderDict:
                self.log.writeLog(self.eyeData[keys])
            self.log.writeLog(self.Note)
            self.log.writeNewLine()
        except Exception as e:
            Log.AddToLog("exception in WriteMyData :  {}".format(e), 2)
            return False

    def IsDataValid(self,Width,Height):
        try:
            Log.AddToLog('IsDataValid started')
            if not any(self.XList) or not any(self.YList):      # if all values are zero
                print('data is not valid, coordinate issue')
                Log.AddToLog("the cordiante from the Tracker are all zero!",2)
                return False
            for cor in self.XList:
                if 0>cor or cor>Width:
                    print('data is not valid, coordinate issue')
                    Log.AddToLog('{} Obtain coordiante outside the screen range, widthCor is: {} have to be between 0,{}'.format(self.name,cor,Width),2)
                    return False
            for cor in self.YList:
                if 0 > cor or cor > Height:
                    print('data is not valid, coordinate issue')
                    Log.AddToLog('{} Obtain coordiante outside the screen range, HeightCor is: {} have to be between 0,{}'.format(
                        self.name, cor, Height),2)
                    return False
            Log.AddToLog('Data is Valid! returning true')
            return True
        except Exception as e:
            Log.AddToLog(" exception in IsDataValid :  {}".format(e), 2)
            return False

    def Reset(self):
        Log.AddToLog('Reset started')
        try:
            self.SampleNumber=0
            self.XList = np.array([])
            self.YList = np.array([])
            del self.EyeDataDF
            self.EyeDataDF = pd.DataFrame(columns=self.mycolumns)
            self.EndWriting = False
        except Exception as e:
            Log.AddToLog(" exception in Reset :  {}".format(e), 2)
            return False