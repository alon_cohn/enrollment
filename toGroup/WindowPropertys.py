# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"

import pygame
import ctypes

try:
    from AppKit import NSScreen
except ImportError:
    from win32api import GetSystemMetrics


class windowPropertys():
    # Set the pygame window propertys
    def __init__(self):

        self.frameRate = 60
        # variable needs to be set for the used display
        # AW17 = 0.384, Dell 19" = 0.34, Dell 24" = 55.1
        self.screen_width_m = 0.384
        self.constant_factor_for_resizing = (self.screen_width_m / 2560)
        self.bg_color = (0, 0, 0)
        # set the size of the window to the maximum size of the screen
        # TODO: Fix the bug with the dpiAware setting. This one only works if in the windows display setting the magnification is set >= 150%
        # else its not working
        self.dpiAware = False

    def getScreenWithBlackBackground(self):
        # initializes a black full screen window
        pygame.init()
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((self.width, self.height), pygame.FULLSCREEN)
        self.background = pygame.Surface((self.width, self.height))
        self.drawBackground(self.bg_color)

    def drawBackground(self, color):
        # draw the background window
        self.background.fill(color)
        self.background = self.background.convert()

    def setScreenSize(self):
        # set screen size to maximum display size, if dpiAware = True the windows magnification must be higher 150%
        self.infoObject = pygame.display.Info()
        self.width_true = self.infoObject.current_w
        self.height_true = self.infoObject.current_h

        self.width, self.height = (GetSystemMetrics(0), GetSystemMetrics(1))
        if self.width_true / self.width >= 1.45:
            ctypes.windll.user32.SetProcessDPIAware()

            self.true_res = (ctypes.windll.user32.GetSystemMetrics(0), ctypes.windll.user32.GetSystemMetrics(1))
            self.width = self.true_res[0]
            self.height = self.true_res[1]
            print('true resolution active...')
            self.dpiAware = True

        # calculate pixel pitch and resize factor for the given size
        self.pixelPitch = self.screen_width_m / self.width
        self.factor = self.constant_factor_for_resizing / self.pixelPitch
        print('Resize factor = {}'.format(self.factor))
        print("Set screen size to {}x{} Pixel".format(self.width, self.height))

    def graphicsResizer(self, graphic):
        # resizing graphics to same apperance on all display sizes
        # graphics schould be load with pygame.image.load
        # the grapics are made for a resolution of 2560x1440 on 17" with a pixel pitch of 0.15 mm
        if self.dpiAware == False:
            graphic = pygame.transform.rotozoom(graphic, 0.0, self.factor)
        return graphic

    def valueResizer(self, value):
        # resizing values to same amopunt on all display sizes
        # the values are made for a resolution of 2560x1440 on 17" with a pixel pitch of 0.15 mm
        if self.dpiAware == False:
            return int(value * self.factor)
        else:
            return value


if __name__ == '__main__':
    window = windowPropertys()
    window.setScreenSize()