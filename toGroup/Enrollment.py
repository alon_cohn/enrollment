import time
import os
import re
import win32api
from colorama import *
import datetime
import pandas as pd
from threading import Thread
import platform

from toGroup.Device import device
from DataSc.Module import module
from toGroup.AcquisitioDataWay import acquisitioDataWav
from ValidationProcess.Validation import validation
from DataSc import Log
from GUI.ShowEyes import showEyes
from Configuration import ParameterManger as PM


#{'CALI': cali,'PIN':pin}

'''Enrollment is a class for the first interaction between the user and the software
the only input the user supply its his ID
Enrollment object contain information about
Choosed Device
choosing acqusetion data way 
start ruing the gui
'''


#plese looke the enrollment chart.
class enrollment ():
    def __init__(self):     #,ID,PathToSaveGoodData):
        pass

        #self.Valid.runner(self.Device.Tracker, self.AcquisitioDataWay.Acqu.window.width,
                          #self.AcquisitioDataWay.Acqu.window.height, self.showEyes, self.logpath)

#-------------------data stefan-------------------------------------------------------------

    def init(self, ID, PathToSaveGoodData, pin):
        self.id = str(ID)  # Only using for log name
        self.FileLocationPath = PathToSaveGoodData
        self.Valid = validation(ID)
        self.moudle = PM.ReturnParameter('self.moudle')  # How Do I know enrollment moudle
        self.AcquisitioDataWay = acquisitioDataWav(self.moudle)
        self.Device = device(self.moudle, self.id,
                             self.Valid.DeviceName)  # Checkign with which device the user will make the enrollment system
        # you  have to know first whats the device you are using for
        # mouse cannot work with CALI-> NEED TO BE DECIDED
        self.FindMeFile = PM.ReturnParameter('self.FindMeFile')
        self.MakeDirectory()  # creating 2 directory one for raw data and one for feature vectores.
        self.MakePathToLogFilesFolder()  # creating slef.logpath
        self.showEyes = showEyes()
        self.pin = pin

    def EnrollRunner(self,Screen,TargetFeatureVectore):
        Log.AddToLog('EnrollRunner started')
        try:
            AmountOfFeatureVecore = 0
            now = datetime.datetime.now()
            TimeToWorkInMin=PM.ReturnParameter('TimeToWorkInMin')
            NowPluse5 = now + datetime.timedelta(minutes=TimeToWorkInMin)
            tries = 0
            while AmountOfFeatureVecore < TargetFeatureVectore or now > NowPluse5:
                print('Number of Feature Vectors = {}'.format(AmountOfFeatureVecore))
                self.FileName = str(AmountOfFeatureVecore) + "_" + self.id + "_" + self.Device.Tracker.name + "_" + self.moudle + "_" + str(int(time.time()))
                print("Data file name for this ID ={} is : {}".format(self.id, self.FileName))
                Log.AddToLog("Data file name for this ID ={} is : {}".format(self.id, self.FileName))
                #resetign all the devices to intial conditaions.
                self.Device.Tracker.Reset()
                self.AcquisitioDataWay.Acqu.reset()
                digit = int(str(self.pin)[AmountOfFeatureVecore])
                FeatureVector = self.AcquisitioDataWay.Acqu.RunGUI(self.Device.Tracker, Screen, AmountOfFeatureVecore, digit)
                if FeatureVector:
                    self.DataToAnlasys = module(self.Device.Tracker.EyeDataDF, self.FileName)  # creatin an object - DataSc package
                    # true if successeded to create FV
                    if self.DataToAnlasys.Aqcu.runner():
                        #saving FV and Raw at Qsync
                        self.SaveFeatureVector(self.DataToAnlasys.FileName, self.DataToAnlasys.Aqcu.FinalFeatures)
                        self.SaveRawData(self.DataToAnlasys.FileName , self.Device.Tracker.EyeDataDF)
                        print('FeatureVector and Raw Data were saved')
                        print((Fore.GREEN + '"Enrollment process: Feature Vectore Number {} was created"' + Fore.RESET).format(AmountOfFeatureVecore))
                        AmountOfFeatureVecore = AmountOfFeatureVecore + 1
                        Screen.FeatureVecroteSucsses(AmountOfFeatureVecore,TargetFeatureVectore)
                    else:
                        Log.AddToLog("the decision is that data is not valid, changing log name cycle = {}".format(
                            AmountOfFeatureVecore))
                        print((Fore.RED + 'the decision is that data is not valid' + Fore.RESET))
                        #saving raw data in logfiles
                        self.Device.Tracker.EyeDataDF.to_csv(os.path.join(self.logpath , self.FileName+'_'+'False'+'_Decision.txt'), index=False)
                        Screen.FeatureVecroteFailed()
                else:
                    Log.AddToLog(
                        "Could not sucseed to make data for creating feature vector, changing log name cycle = {}".format(
                            AmountOfFeatureVecore))
                    print((Fore.RED + 'Could not sucseed to creat data for creating feature vector' + Fore.RESET))
                    self.Device.Tracker.EyeDataDF.to_csv(os.path.join(self.logpath, self.FileName + '_' + 'False.txt'),
                                                         index=False)
                    Screen.FeatureVecroteFailed()
                now = datetime.datetime.now()
                print('Tries = ',tries)
                tries = tries + 1
            WaitWhileProcessing=Thread(target=Screen.FiveCollected)
            WaitWhileProcessing.start()
            return True
        except Exception as e:
            Log.AddToLog("exception in EnrollRunner :  {}".format(e), 2)
            return False

    def SaveFeatureVector(self,FileName,FeatureVector):
        Log.AddToLog('SaveFeatureVector started')
        try:
            FileName=FileName + '.csv'
            FeatureVector.to_csv(os.path.join(self.FeatureVectorPath , FileName), index=False)
        except Exception as e:
            Log.AddToLog("exception in SaveFeatureVector :  {}".format(e), 2)

    def SaveRawData(self,FileName,RawData):
        Log.AddToLog('SaveRawData started')
        try:
            FileName = FileName + '.txt'
            RawData.to_csv(os.path.join(self.RawDataPath, FileName), index=False)
        except Exception as e:
            Log.AddToLog("exception in SaveRawData :  {}".format(e), 2)

    def MakeDirectory(self):
        Log.AddToLog('MakeDirectory started')
        try:
            Now = int(time.time())
            NowAsStr = str(Now)
            self.SaveFilePath = self.FileLocationPath + "\\" + self.id + "\\" + self.Device.Tracker.name + "\\" + self.moudle + "\\" + NowAsStr
            self.FeatureVectorPath=self.SaveFilePath + "\\" + 'FeatureVector'
            self.RawDataPath = self.SaveFilePath +  "\\" + 'RawData'
            os.makedirs(self.FeatureVectorPath)
            os.makedirs(self.RawDataPath)
            print('MakeDirectory Created 2 dir FeatureVectorPath : {} and RawDataPath : {} '.format(self.FeatureVectorPath,self.RawDataPath))
            Log.AddToLog('MakeDirectory Created 2 dir FeatureVectorPath : {} and RawDataPath : {} '.format(self.FeatureVectorPath,self.RawDataPath))
        except Exception as e:
            Log.AddToLog("exception in MakeDirectory :  {}".format(e), 2)
            exit()

    def FindFile(self,root_folder, rex):
        Log.AddToLog('find_file started')
        try:
            for root, dirs, files in os.walk(root_folder):
                if  'Qsync' in dirs:
                    return self.EasyFind(root_folder, rex)
                if '$Recycle.Bin' in dirs :
                    dirs.remove('$Recycle.Bin')
                    continue
                for f in files:
                    result = rex.search(f)
                    if result:
                        print('Found the file the location is : ',os.path.join(root, f))
                        self.FileLocationPath = root    # the path to wehre you can find the Feature veactors.
                        Log.AddToLog('File was found FeatureVectorPath is : {}'.format(self.FileLocationPath))
                        return True
            return False

        except Exception as e:
            Log.AddToLog("exception in FindFileInAllDrives :  {}".format(e), 2)

    def FindFileInAllDrives(self,file_name):
        Log.AddToLog('FindFileInAllDrives started')
        try:
            # create a regular expression for the file
            rex = re.compile(file_name)
            for drive in win32api.GetLogicalDriveStrings().split('\000')[:-1]:
                if self.FindFile(drive, rex):
                    return
                print('Following  drive : {} , file is not exist'.format(drive))
                Log.AddToLog('Following  drive : {} , file is not exist'.format(drive))
            print(Fore.BLUE + 'File was not traced , do not know where to save feature vectors, exiting the program' + Fore.RESET)
            Log.AddToLog('File was not traced , do not know where to save feature vectors, exiting the program')
            exit()
        except Exception as e:
            Log.AddToLog("exception in FindFileInAllDrives :  {}".format(e), 2)
            exit()

    def EasyFind(self,root_folder,rex):
        for ro, dir, file in os.walk(root_folder + 'Qsync' + '\\' + 'Temp_Data'):
            for f in file:
                result = rex.search(f)
                if result:
                    print('Found the file the location is : ', os.path.join(ro, f))
                    self.FileLocationPath = ro  # the path to wehre you can find the Feature veactors.
                    Log.AddToLog('File was found FeatureVectorPath is : {}'.format(self.FileLocationPath))
                    return True

    def MakePathToLogFilesFolder(self):
        try:
            Log.AddToLog("Validation. makeDirectory started")
            self.logpath = os.getcwd()
            os_system = platform.system()
            if os_system == "Windows":
                self.logpath = self.logpath + "\LogFiles"
            else:
                self.logpath = self.logpath + "/LogFiles"
            if not os.path.exists(self.logpath):
                print("LogFiles folder created")
                os.makedirs(self.logpath)
        except Exception as e:
            Log.AddToLog("Validation.makeDirectory :  {}".format(e), 2)
            exit()


    # wnated to create a way to enter the matrix the new feature metrix.
    #not activated
    def InsertNewFvToMatrix(self,FVasDF, Matrixpath,MoveID):
        #NewMoveID need to ne either 100 or 99
        #99 : those are the ones that the model was creathed with
        #100: this FV came from TryMe
        Log.AddToLog('InsertNewFvToMatrix started',1)
        try:
            if (self.moudle=='CALI' and self.Device.ChosenTracker=='SMI'):
                MatrixToUpdate = PM.ReturnParameter('MatrixToUpdate')
                MatrixFullPath = Matrixpath + '//' + MatrixToUpdate +'.csv' # 28163
                MatrixDF = pd.read_csv(MatrixFullPath)
                FVasDF = FVasDF.assign(ID=self.id)
                FVasDF = FVasDF.assign(MoveID=MoveID)
                print('Adding new Feature matrix to the MainMatrix')
                Log.AddToLog('Adding new Feature matrix to the MainMatrix the FV created during {} MatrixShape Before={}'.format(MoveID, MatrixDF.shape), 1)
                MatrixDF = pd.concat([MatrixDF, FVasDF], axis=0)
                Log.AddToLog('MatrixShape After = {}'.format(MatrixDF.shape), 1)
                with open(MatrixFullPath, 'a') as NewMatrixDF:
                    MatrixDF.to_csv(NewMatrixDF, index=False)
            else:
                return
        except Exception as e:
            Log.AddToLog("InsertNewFvToMatrix :  {}".format(e), 2)
            print('matrix was not update')
'''
source = FilePathInLogFiles
            dest = self.RawDataPath
            shutil.copy2(source,dest)
'''
