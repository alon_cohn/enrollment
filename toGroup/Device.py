__author__ = "Alon Cohn,11.02"

'''The class runner check which device is connected and create a child class to Eyedata name after the device '''

import sys
from colorama import *
import os

from Utils import Log
from Configuration import ParameterManger as PM
from Devices.Mouse import mouse
from Devices.Smi import smi
from Devices.Tobi import tobi
from Devices.GP3 import gp3





class device:
    def __init__(self,moudle,id,process):
        """
        :param moudle: which module to activate
        :param id: user id
        :param process: which process is runing on the machine, Validation.py check it
        """
        self.moudle = moudle
        self.id=id
        self.DeviceDict = {'MOUSE': mouse,'SMI':smi,'TOBI':tobi, "gp3":gp3}
        self.ChosenTracker = process
        self.Tracker = self.InitTracker()


    def InitTracker(self):
        Log.AddToLog("InitTracker started working", 1)
        try:
            if self.ChosenTracker not in self.DeviceDict:
                Log.AddToLog("The Choosen Tracker is not in the list of allowed trackers", 1)
                raise RuntimeError
            else:
                print((Fore.GREEN + 'was able to connect to: {} ' + Fore.RESET).format(self.ChosenTracker))
                return self.DeviceDict[self.ChosenTracker](self.ChosenTracker,self.moudle,self.id)
        except Exception as e:
            Log.AddToLog("exception in InitTracker :  {}".format(e), 2)



    # def WhichTrackerIsActivated(self):
    #     #Alon Cohn 07.12.18
    #     # the function will try to connect to the TOBII or SMI otherwise will use the MOUSE as defalt
    #     Log.AddToLog("WhichTrackerIsActivated started working",1)
    #     try:
    #         SMIConnected = useTobii_minimal.IsSMIConected()     #Check if there is a SMI DEVICE
    #         if SMIConnected:
    #             Log.AddToLog("SMIConnected={}, SMI was chosen".format(SMIConnected), 1)
    #             return('SMI')
    #
    #         from EyeTrackerUtils import useTobii_minimalAW17
    #         TobiConnected = useTobii_minimalAW17.IsTobiiConected()  # Check if there is a TOBI DEVICE
    #         if TobiConnected:
    #             print("SMI tracker not found, attempt to connect to Tobii")
    #             Log.AddToLog("TobiConnected={}, TOBI is connected".format(TobiConnected), 1)
    #             return('TOBI')
    #         # Alon added for be able to run the code from a computer without TOBI or SMI
    #         else:
    #             Log.AddToLog("TobiConnected={} and SMIConnected={}, returning MOUSE was chosen".format(TobiConnected,SMIConnected), 1)
    #             return('MOUSE')
    #     except Exception as e:
    #         Log.AddToLog("exception in AddToLog :  {}".format(e), 2)