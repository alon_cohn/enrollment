from AcquisitioDataWays.Calibration import  cali
from AcquisitioDataWays.Pin import pin
from AcquisitioDataWays.CaliAndPin import caliPin
from Utils import Log


class acquisitioDataWav:
    def __init__(self,aquName):   #System name can be only names from values in self.AcquisitioDict
        self.AcquisitioDict = {'CALI': cali,'PIN':pin,'CALI+PIN':caliPin}
        self.acqName=aquName
        self.Acqu=self.InitAcqu()


    def InitAcqu(self):
        Log.AddToLog("InitAcqu started working", 1)
        try:
            if self.acqName not in self.AcquisitioDict:
                raise RuntimeError
            else:
                return self.AcquisitioDict[self.acqName](self.acqName)
        except Exception as e:
            Log.AddToLog("exception in InitTracker :  {}".format(e), 2)

    def WhichAcquisitioWay(self):
        # Alon Cohn 16.02
        # the function will try to get form the user which opreating data acqusition have been chosen
        Log.AddToLog("WhichTrackerIsActivated started working", 1)
