from Utils import  Log
import pandas as pd


class TryModel():
    def __init__(self,ModelFeaturesList,Screens,Enroll,FileLocationPath):
        self.Eer = ModelFeaturesList[0]
        self.Threshold = ModelFeaturesList[1]
        self.Model = ModelFeaturesList[2]
        self.Screens=Screens
        self.Enroll=Enroll
        self.FileLocationPath=FileLocationPath


    def ProtoTypeRunner(self):
        Log.AddToLog("ProtoTypeRunner started ")
        try:
            WhereIsFile=self.Enroll.FeatureVectorPath + '\\' + self.Enroll.FileName + '.csv'
            self.OneFeatureVector = pd.read_csv(self.Enroll.FeatureVectorPath + '\\' + self.Enroll.FileName + '.csv')
            print(self.Enroll.FileName)
            Log.AddToLog('WhereIsFile: {}'.format(WhereIsFile),1)
            # Patch!!!!
            # should delete Note column in the Data sience part
            if self.Enroll.moudle == 'PIN':
                self.OneFeatureVector = self.OneFeatureVector.drop(columns=['Revision','Note','Target_velocity_px_s_Movement1','Target_velocity_px_s_Movement2'])
            else:
                self.OneFeatureVector = self.OneFeatureVector.drop(columns=['Revision'])
            Log.AddToLog('User (.5 thresh): {}'.format(self.Model.predict(self.OneFeatureVector)),1)
            Log.AddToLog('Prediction probas: {}'.format(self.Model.predict_proba(self.OneFeatureVector)), 1)

            probUser = self.Model.predict_proba(self.OneFeatureVector)[0][1]
            Log.AddToLog('The ProbUser ={} Threshold = {} and the Eer = {}'.format(probUser,self.Threshold,self.Eer),1)
            print('The ProbUser ={} Threshold = {} and the Eer = {}'.format(probUser,self.Threshold,self.Eer))
            if probUser >= self.Threshold:
                return True
            else:
                return False
        except Exception as e:
            Log.AddToLog("exception in ProtoTypeRunner :  {}".format(e), 2)
            return False

    def MakeOneFeatureVector(self):
        Log.AddToLog("MakeOneFeatureVector started ")
        try:
            ContinueRuning=True
            while ContinueRuning:
                self.Screens.ExplainCalibration()
                TargetFeatureVectore = 1
                FeatureVectors = self.Enroll.EnrollRunner(self.Screens, TargetFeatureVectore)
                if FeatureVectors:
                    #self.Enroll.InsertNewFvToMatrix(self.Enroll.DataToAnlasys.Aqcu.FinalFeatures , self.FileLocationPath, 100)
                    if self.ProtoTypeRunner():
                        self.Screens.ItsMe(self.Enroll.id)
                    else:
                        self.Screens.ItsNotMe()
        except Exception as e:
            Log.AddToLog("exception in MakeOneFeatureVector :  {}".format(e), 2)
            return False

  