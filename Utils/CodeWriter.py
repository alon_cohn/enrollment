import pickle
import hashlib, binascii
import os
from Utils import Log

def storePasswordHash(name = 'none',pwd = 'none'):
    filename = "C:/Qsync/Temp_Data/NameDict.txt"
    run = True
    if os.path.isfile(filename):
        output = open(filename,"rb")
        dictionary = pickle.load(output)
        output.close()
    else:
        dictionary = {}
    output = open(filename,"wb")
    if name == 'none':
        username = input("Enter Username\n")
    else:
        username = name
    if pwd == 'none':
        while run:
            password = input("Enter Password\n")
            validation = input("Repeat Password\n")
            if password == validation:
                run = False
            else:
                Log.AddToLog("Passwords not matching")
    else:
        password = pwd
    hashpwd = binascii.hexlify(hashlib.pbkdf2_hmac('sha256',password.encode(),b'lulilala',100000))
    if username not in dictionary.values():
        dictionary[hashpwd] = username
    else:
        Log.AddToLog('could not save password: user {} already exists'.format(username))
    pickle.dump(dictionary, output, pickle.HIGHEST_PROTOCOL)
    output.close()

if __name__ == '__main__':
    storePasswordHash()