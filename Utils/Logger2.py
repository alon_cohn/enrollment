# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"
import os
import platform
import time
from Utils import Log
import datetime


#class logger1():
def init(TrackerName, MoudleName,id,logstatus):
    '''Write a log message to a file, use txt format'''
    logstatus = logstatus
    logHeader = ( "EyeTime,RawX,RawY,AverageX,AverageY,Left_RawX,Left_RawY,Left_PupilSize,Left_PupilCenterX,Left_PupilCenterY,Left_PupilCenterZ,Right_RawX,Right_RawY,Right_PupilSize,Right_PupilCenterX,Right_PupilCenterY,Right_PupilCenterZ,CorrectedX,CorrectedY")
    logpath=makeDirectory()
    logname=CreatingSelfLogName(id,TrackerName,MoudleName)
    openLogFile(logname,logpath,logstatus)
    CreatingSelfHeaderNeeded(logstatus)  # 12.02 eddit fir not having an if in init

def writeHeader(self):
    self.writeLog("TimeStamp,MoveID,"+self.logHeader+",Note")
    self.writeNewLine()

def CreatingSelfHeaderNeeded(self,logstatus):
    try:
        if os.path.isfile("LogFiles/"+self.logname+".txt"):
            self.headerNeeded = True
        else:
            self.headerNeeded = False
        if self.headerNeeded == True or logstatus == "w":
            print("Header written") #DevLog.AddToLog("header written") need to be in the function
            self.writeHeader()
    except Exception as e:
        Log.AddToLog("exception in CreatingSelfHeaderNeeded :  {}".format(e), 2)
        return False
def CreatingSelfLogName(self,id,TrackerName,MoudleName):
    try:
        logname = id+"_"+TrackerName+"_"+MoudleName+"_"+str(int(time.time()))
        print(logname)
        return logname
    except Exception as e:
        Log.AddToLog("exception in CreatingSelfLogName :  {}".format(e), 2)
        return False

def makeDirectory():
    logpath = os.getcwd()
    os_system = platform.system()
    if os_system == "Windows":
        logpath = logpath + "\LogFiles"
    else:
        logpath = logpath + "/LogFiles"
    if not os.path.exists(logpath):
        print("LogFiles folder created")
        os.makedirs(logpath)

def openLogFile(logname,logpath,logstatus):
    try:
        logfilename = os.path.join(logpath, logname + ".txt")
        log=open(logfilename,logstatus)
        return log
    except Exception as e:
        Log.AddToLog("exception in openLogFileName :  {}".format(e), 2)
        return False

def closeLogFile(log):
    log.close()

def writeLog(log,logMessage):
    log.write(str(logMessage) + ",")
    closeLogFile(log)
    log=open(logfilename, self.logstatus)


def writeLastLineEntry(log, logMessage):
    log.write(logMessage)

def writeNewLine(log):
    log.write("\n")
    writeTimeStamp()

def writeTimeStamp(log):
    timestamp = str(time.time())  # .replace(":","").replace(" ","").replace(".","").replace("-","")
    log.write(self.timestamp + ",")
