# -*- coding: utf-8 -*-

__author__ = "Klaas Filler"
import os
import platform
import time
from Utils import Log
import datetime


class cSVcreator:
    def __init__(self, TrackerName, MoudleName,id,logstatus,Cycle=None):
        '''Write a log message to a file, use txt format'''
        self.logstatus = logstatus
        self.logHeader = ( "EyeTime,RawX,RawY,Left_RawX,Left_RawY,Left_PupilSize,Left_PupilCenterX,Left_PupilCenterY,Left_PupilCenterZ,Right_RawX,Right_RawY,Right_PupilSize,Right_PupilCenterX,Right_PupilCenterY,Right_PupilCenterZ")
        self.makeDirectory()
        self.CreatingSelfLogName(id,TrackerName,MoudleName,Cycle)
        self.openLogFile()
        self.CreatingSelfHeaderNeeded(logstatus)  # 12.02 eddit fir not having an if in init
        Log.AddToLog('Finsih creating Log data file name is=  {}'.format(self.logname), 1)


    def writeHeader(self):
        self.writeLog("TimeStamp,"+self.logHeader+",Note")
        self.writeNewLine()

    def CreatingSelfHeaderNeeded(self,logstatus):
        try:
            if os.path.isfile("LogFiles/"+self.logname+".txt"):
                self.headerNeeded = True
            else:
                self.headerNeeded = False
            if self.headerNeeded == True or logstatus == "w":
                self.writeHeader()
        except Exception as e:
            Log.AddToLog("exception in CreatingSelfHeaderNeeded :  {}".format(e), 2)
            return False
    def CreatingSelfLogName(self,id,TrackerName,MoudleName,Cycle=None):
        try:
            self.logname = str(Cycle)+"_"+str(id)+"_"+TrackerName+"_"+MoudleName+"_"+str(int(time.time()))
            print("Data file name for this ID ={} is : {}".format(id,self.logname))
        except Exception as e:
            Log.AddToLog("exception in CreatingSelfLogName :  {}".format(e), 2)
            return False

    def makeDirectory(self):
        self.logpath = os.getcwd()
        os_system = platform.system()
        if os_system == "Windows":
            self.logpath = self.logpath + "\LogFiles"
        else:
            self.logpath = self.logpath + "/LogFiles"
        if not os.path.exists(self.logpath):
            print("LogFiles folder created")
            os.makedirs(self.logpath)

    def openLogFile(self):
        try:
            self.logfilename = os.path.join(self.logpath, self.logname + ".txt")
            self.log=open(self.logfilename, self.logstatus)
        except Exception as e:
            Log.AddToLog("exception in openLogFileName :  {}".format(e), 2)
            return False

    def closeLogFile(self):
        #self.log.close()
        pass

    def writeLog(self, logMessage):
        try:
            self.log.write(str(logMessage) + ",")
            self.closeLogFile()
            self.log=open(self.logfilename, self.logstatus)
        except Exception as e:
            Log.AddToLog("exception in writeLog :  {}".format(e), 2)
            return False

    def writeLastLineEntry(self, logMessage):
        self.log.write(logMessage)

    def writeNewLine(self):
        self.log.write("\n")


    def writeTimeStamp(self):
        self.timestamp = str(time.time())  # .replace(":","").replace(" ","").replace(".","").replace("-","")
        self.log.write(self.timestamp + ",")

    def ChangeLogName(self,UserNewName):
        self.closeLogFile()
        old_file = os.path.join(self.logpath,self.logname+'.txt')
        new_file = os.path.join(self.logpath,UserNewName+'.txt')
        os.rename(old_file, new_file)

def Write(Message):
    os.write(Message)
