from EyeTrackerUtils import tobii
import win32api
import math
import tkinter as tk
from PIL import Image, ImageTk

width_mm = 3467
height_mm = 1950
width_pixel = win32api.GetSystemMetrics(0)
height_pixel = win32api.GetSystemMetrics(1)

#root = tk.Tk()
#img = ImageTk.PhotoImage(Image.open("python.jpg"))
#panel = tk.Label(root, image = img)
#panel.pack(side = "bottom", fill = "both", expand = "yes")

distance = 0

def gazeReceived(x,y,timestamp):
	print ("Gaze: ", x, " ", y, " " , timestamp)
	#win32api.SetCursorPos(((d['Gaze']['x'],(d['Gaze']['y']))
	if distance > 0:
		x_mm = (x/width_pixel-0.5) * width_mm
		y_mm = (y/height_pixel-0.5) * height_mm
		len = math.sqrt(x_mm * x_mm + y_mm * y_mm)
		angle = math.degrees( math.atan(len/distance) )
		angle_x = math.degrees( math.atan(x_mm/distance) )
		angle_y = math.degrees( math.atan(y_mm/distance) )
		print ("angle: ", angle, " degrees, angle x: ", angle_x, " degrees, angle y: ", angle_y, " degrees")
	
def imageReceived(img):
	print ("got an image")
	#panel.configure(image = img)
	#panel.image = img
#	cv2.imshow('frame',img)
	img.show()
	
def positionReceived(lx,ly,lz,rx,ry,rz,timestamp):
	global distance
	print ("Position: ", lx, " ", ly, " ", lz, " ", rx, " ", ry, " ", rz, " " , timestamp)
	distance = max(lz,rz)
	print ("distance: ", distance, "mm")
	
	
#tobii.addGazeCallback(gazeReceived)
#tobii.addImageCallback(imageReceived)
tobii.addPositionCallback(positionReceived)
tobii.connect()
tobii.enableLogImage("images_test/")
print ("hello")

#root.mainloop()