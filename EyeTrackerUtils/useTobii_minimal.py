from EyeTrackerUtils import tobii
import math
from PIL import Image, ImageTk
from datetime import datetime
import tkinter as tk
import platform

os_system = platform.system()
if os_system == "Windows":
    import win32api

    width_pixel = win32api.GetSystemMetrics(0)
    height_pixel = win32api.GetSystemMetrics(1)
'''
else:
	import tkinter as tk
	root = tk.Tk() 
	root.withdraw()
	width_pixel = root.winfo_screenwidth()
	height_pixel = root.winfo_screenheight()
'''

width_mm = 3467
height_mm = 1950
# root = tk.Tk()
# img = ImageTk.PhotoImage(Image.open("python.jpg"))
# panel = tk.Label(root, image = img)
# panel.pack(side = "bottom", fill = "both", expand = "yes")


dis = [0]
tstamp = [0]
ang_x = [0]
ang_y = [0]
xcoord_mm = [0]
ycoord_mm = [0]

g = [0, 0, 0, 0]
LeftEyePos = [0, 0, 0]
RightEyePos = [0, 0, 0]
PupilDiam = [0, 0]
distance = 0


def gazeReceived(Rx, Ry, Lx, Ly, timestamp, PupilR, PupilL, PosRX, PosRY, PosRZ, PosLX, PosLY, PosLZ):
    # print("IN GAZE RECEIVED")
    # print ("Gaze: ", x, " ", y, " " , timestamp)
    g[0] = Rx
    g[1] = Ry
    g[2] = Lx
    g[3] = Ly
    tstamp[0] = timestamp
    RightEyePos[0] = PosRX
    RightEyePos[1] = PosRY
    RightEyePos[2] = PosRZ
    LeftEyePos[0] = PosLX
    LeftEyePos[1] = PosLY
    LeftEyePos[2] = PosLZ
    PupilDiam[0] = PupilR
    PupilDiam[1] = PupilL

'''
#win32api.SetCursorPos(((d['Gaze']['x'],(d['Gaze']['y']))
if distance > 0:
    x_mm = (x/width_pixel-0.5) * width_mm
    y_mm = (y/height_pixel-0.5) * height_mm
    len = math.sqrt(x_mm * x_mm + y_mm * y_mm)
    angle = math.degrees( math.atan(len/distance) )
    angle_x = math.degrees( math.atan(x_mm/distance) )
    angle_y = math.degrees( math.atan(y_mm/distance) )
    #print ("angle: ", angle, " degrees, angle x: ", angle_x, " degrees, angle y: ", angle_y, " degrees")
else:
    angle_x= -1
    angle_y= -1
    x_mm = -1
    y_mm = -1

ang_x[0] = angle_x
ang_y[0] = angle_y
xcoord_mm[0] = x_mm
ycoord_mm[0] = y_mm
'''


def imageReceived(img):
    # print ("got an image")
    # panel.configure(image = img)
    # panel.image = img
    #	cv2.imshow('frame',img)
    img.show()


def positionReceived(lx, ly, lz, rx, ry, rz, timestamp):
    # print("IN POSITION RECEIVED")
    global distance
    # print ("Position: ", lx, " ", ly, " ", lz, " ", rx, " ", ry, " ", rz, " " , timestamp)
    distance = max(lz, rz)
    dis[0] = distance
    # print("dist[0]", dist[0])
    # print ("distance: ", distance, "mm")
    tstamp[0] = timestamp


def enableTheLogImage():
    tobii.enableLogImage(
        "C:\SmoothGOVorversuch\images_" + str(datetime.now()).replace(' ', '_').replace(':', '-').replace('.',
                                                                                                          '-') + "/")


# tobii.enableLogImage("images/")

def disableTheLogImage():
    tobii.disableLogImage()


# tobii.enableLogImage("images/")

def killTrackerConnection():
    # print("diconnect tracker")
    tobii.disconnect()


def IsSMIConected():
    # CHECKING IF THERE IS A TOBI DEVICE CONNECTED WILL RETURN FALSE IF THERE IS NOT!!
    if tobii.connect():
        return True
    else:
        return False


tobii.addGazeCallback(gazeReceived)
# tobii.addImageCallback(imageReceived)
tobii.addPositionCallback(positionReceived)
'''try:
	tobii.connect()
except ImportError:
    print("not connected")'''

# print ("hello")

# root.mainloop()
